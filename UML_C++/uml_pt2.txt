@startuml

interface INewPoints {
+vector<Vector2d> {abstract} OrderedVertex()
+void {abstract} CalcPoints()
+vector<Vector2d> {abstract} ReturnPoints() const
+int {abstract} NumNewPoints() const
}

interface ISegment {
+double {abstract} Distance() const
+Vector2d {abstract} Direction() const
}

interface ICutPolygon {
+void {abstract} Cut()
+int& {abstract} NumIntersections()
+vector<Vector2d> {abstract} IntersectionPoints() const
+void {abstract} ChooseFunction()
+void {abstract} NewPolygonsTwoInt()
+void {abstract} NewPolygonsMoreInt()
+vector<vector<Vector2d>> {abstract} CuttedPolygons()
+vector<vector<int>> {abstract} NewPolygonsVertex()
}

interface IIntersector {
+void {abstract} SetStraight(const Vector2d& origin1, const Vector2d& end1)
+void {abstract} SetEdge(const Vector2d& origin2, const Vector2d& end2)
+bool {abstract} Intersection()
+const Vector2d& {abstract} IntersectionCoordinates()
+const Vector2d& {abstract} ParametricCoordinates() const
}

interface IPolygon {
+double {abstract} ComputeArea()
+vector<Vector2d> {abstract} ComputeBoundingBox()
}

interface IReferenceElement {
+vector<Vector2d> {abstract} newBoundingBox()
+vector<Vector2d> {abstract} CreateRefElement()
+vector<Vector2d> {abstract} Translate(vector<Vector2d> element, Vector2d translation)
+vector<vector<Vector2d>> {abstract} CreateMesh(vector<Vector2d> domain)
}

class NewPoints {
+static double tolerance
-vector<Vector2d> _newPoints
-vector<Vector2d> _vertex
-vector<Vector2d> _intersectionPoints
-Vector2d _s1, _s2
-void Reset()
+NewPoints(const vector<Vector2d> vertex, 
const vector<Vector2d> intersectionPoints, const Vector2d s1, const Vector2d s2)
+vector<Vector2d> OrderedVertex()
+void CalcPoints()
+vector<Vector2d> ReturnPoints() const
+int NumNewPoints() const
}

class Segment {
-Vector2d _s1, _s2
-void Reset()
+Segment(const Vector2d s1, Vector2d s2)
+double Distance() const
+Vector2d Direction() const
}

class CutPolygon {
+static double tolerance
-vector<Vector2d > _vertex, _ordVertex
-Vector2d _s1, _s2;
-vector<bool> _edgeWithInters
-int _numIntersections
-Vector2d _intersectionCoordinates
-vector<Vector2d> _intersectionPoints
-vector<vector<Vector2d>> _cuttedPolygons
-vector<vector<int>> _newPolygonsVertex
-vector<double> _distances
-vector<Vector2d> _puntiCorrispondenti
-void Reset()
+CutPolygon(vector<Vector2d> vertex, Vector2d& s1, Vector2d& s2)
+void Cut()
+int& NumIntersections()
+vector<Vector2d> IntersectionPoints() const
+void ChooseFunction()
+void NewPolygonsTwoInt()
+void NewPolygonsMoreInt()
+vector<vector<Vector2d>> CuttedPolygons()
+vector<vector<int>> NewPolygonsVertex()
}

class Intersector {
+static double toleranceParallelism
+static double toleranceIntersection
-Vector2d _origin1
-Vector2d _origin2
-Vector2d _resultParametricCoordinates
-Vector2d _rightHandSide
-Matrix2d _matrixTangentVector
-Vector2d _intersectionCoordinates
+Intersector()
+void SetStraight(const Vector2d& origin1, const Vector2d& end1)
+void SetEdge(const Vector2d& origin2, const Vector2d& end2)
+bool Intersection()
+const Vector2d& IntersectionCoordinates()
+const Vector2d& ParametricCoordinates() const
}

class Polygon {
-vector<Vector2d> _vertex
-vector<Vector2d> _boundingBox
-double _area
-void Reset()
+Polygon(vector<Vector2d> vertex)
+double ComputeArea()
+vector<Vector2d> ComputeBoundingBox()
}

class ReferenceElement {
-vector<Vector2d> _polygon, _boundingBox, _newBB, _transElement, _refElement
-vector<vector<Vector2d>> _mesh, _ref
-double _baseBB, _heightBB, _areaTot
-void Reset()
+ReferenceElement(vector<Vector2d> polygon, vector<Vector2d> boundingBox)
+vector<Vector2d> newBoundingBox()
+vector<Vector2d> CreateRefElement()
+vector<Vector2d> Translate(vector<Vector2d> element, Vector2d translation)
+vector<vector<Vector2d>> CreateMesh(vector<Vector2d> domain)
+double AreaTot()
}

ICutPolygon..>ISegment
ICutPolygon..>INewPoints
CutPolygon..|>ICutPolygon
ICutPolygon..>IIntersector

IReferenceElement..>ICutPolygon
IReferenceElement..>INewPoints
IReferenceElement..>ISegment
IReferenceElement..>IPolygon

INewPoints <|.. NewPoints
ISegment <|.. Segment 
IIntersector <|.. Intersector
IPolygon <|.. Polygon
IReferenceElement <|.right. ReferenceElement

@enduml