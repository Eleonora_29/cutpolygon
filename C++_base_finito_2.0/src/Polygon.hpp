#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Intersector.hpp>
#include <Segment.hpp>
#include <Point.hpp>
#include <Eigen>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class Polygon : public IPolygon {

  public:
      void Reset();

      Polygon() {};


  };
}

#endif // POLYGON_H
