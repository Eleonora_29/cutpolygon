#include "Point.hpp"
#include <math.h>

namespace PolygonCutLibrary {

double Point::tolerance = 1.0E-7;

void Point::Reset()
{
    _newPoints.clear();
}

double Point::Distance(const Vector2d &A, const Vector2d &B) const
  {
      return sqrt(pow((A(0) - B(0)), 2) + pow((A(1) - B(1)), 2));
  }

vector<Vector2d> Point::OrderedVertex(const vector<Vector2d> vertex)
{
    vector<Vector2d> orderedVertex;
    int primoIndex;
    double minX = vertex[0][0];
    double minY = vertex[0][1];
    int flagX = 0;
    int flagY = 0;
    bool daOrdinare = false;
    for (unsigned int v=1; v<vertex.size(); v++)
    {
        if (vertex[v][0] <= minX)
            flagX = v;
        if (vertex[v][1] <= minY)
            flagY = v;
        if (flagX != 0 && flagY != 0)
        {
            minX = vertex[v][0];
            minY = vertex[v][1];
            primoIndex = v;
            flagX=0;
            flagY=0;
            daOrdinare = true;
        }
        else
        {
            flagX=0;
            flagY=0;
        }
    }
    if (daOrdinare == true)
    {
        for (unsigned int a=primoIndex; a<vertex.size(); a++)
            orderedVertex.push_back(vertex[a]);
        for (int b=0; b<primoIndex; b++)
            orderedVertex.push_back(vertex[b]);
    }
    else
        orderedVertex = vertex;
    return orderedVertex;

}

void Point::CalcPoints(const vector<Vector2d> vertex, const int numVertex,
                       const vector<Vector2d> intersectionPoints, const int numIntersections,
                       const Vector2d s1, const Vector2d s2)
{
    Reset();
    vector<Vector2d> orderedVertex = OrderedVertex(vertex); //metto in ordine i vertici che ricevo in input, in modo che il primo sia quello in basso a sinistra
    for (int i=0; i<numVertex; i++)
      _newPoints.push_back(orderedVertex[i]); // prima inserisco i vertici del poligono

    int flag=0, flag1=0, flag2=0;

    for (int j=0; j<numIntersections; j++) //scorro i punti di intersezione
    {
        for (int i=0; i<numVertex; i++) //scorro i vertici
        {
            //controllo se il punto di intersezione coincide con il vertice a meno di una tolleranza
            if (abs(intersectionPoints[j](0) - orderedVertex[i](0)) < tolerance && abs(intersectionPoints[j](1) - orderedVertex[i](1)) < tolerance)
                flag = 1;
        }
        if (flag == 0)
            _newPoints.push_back(intersectionPoints[j]); //aggiungo il punto a newPoints solo se non è un vertice
        flag = 0;
    }

    for (unsigned int k=0; k<_newPoints.size(); k++)
    {
        //controllo se i due estremi del segmento coincidono con vertici o intersezioni a meno di una tolleranza
        if (abs(s1(0) - _newPoints[k](0)) < tolerance && abs(s1(1) - _newPoints[k](1)) < tolerance)
            flag1 = 1;
        if (abs(s2(0) - _newPoints[k](0)) < tolerance && abs(s2(1) - _newPoints[k](1)) < tolerance)
            flag2 = 1;
    }
    // se non coincidono con niente, li aggiungo
    if (flag1 == 0)
        _newPoints.push_back(s1);
    if (flag2 == 0)
        _newPoints.push_back(s2);
}

}
