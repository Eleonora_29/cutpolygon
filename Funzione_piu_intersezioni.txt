Funzione “NewPolygonsMoreInt”

Per trovare il primo poligono:

parto dal vertice 0
mi segno se è a destra o sinistra del segmento
vado avanti ad aggiungere vertici finché non trovo un'intersezione
aggiungo l'intersezione
procedo sulla retta finché non trovo un'altra intersezione, e ciò che c'è in mezzo lo aggiungo
aggiungo anche l'intersezione
devo andare ancora avanti sulla retta?

	se quell'intersezione non è un vertice, cerco il vertice che stia dalla stessa parte del segmento del punto 0, lo aggiungo
	procedo sulla retta fino all'intersezione successiva e vedo se c'è vertice da aggiungere dalla stessa parte dello 0
	vado avanti finché non ho finito la retta

	se è un vertice controllo il vertice successivo
	se è dalla stessa parte dello 0 lo aggiungo e aggiungo i vertici successivi
	se no vado avanti sulla retta fino alla prossima intersezione e ripeto il controllo "devo andare ancora avanti sulla retta?"


Per trovare i poligoni successivi:

parto dall'intersezione 0
vado al vertice successivo finché non trovo un altro lato con intersezione
aggiungo l'intersezione
controllo se è un vertice (vertRetta = true)
sto entrando nel poligono o sto uscendo?

	se non sto entrando nel poligono 
	torno indietro sulla retta fino alla prima intersezione messa

	se sto entrando nel poligono
	vado avanti sulla retta di uno, e aggiungo il punto che trovo
	cerco il prossimo lato con intersezione (finché non lo trovo aggiungo i vertici successivi)
	vado avanti sulla retta dall’ultima intersezione trovata fino alla prima aggiunta in questo poligono, e aggiungo tutto quello che trovo in mezzo

controllo quante volte ogni intersezione compare nei poligoni già tagliati

	se la seconda intersezione aggiunta nell’ultimo poligono tagliato, è stata messa meno di 2 volte, parto da lì per il prossimo poligono

	altrimenti controllo se tutte le intersezioni sono state prese almeno due volte
		se sì, ho finito ed esco
		se la seconda intersezione presa era un vertice, parto comunque da lì perché può essere aggiunto in più di due poligoni
		altrimenti parto dall’intersezione successiva

j++
rientro nel ciclo



Legenda:

_edgewithInters: è un booleano che contiene true o false ad indicare se il lato corrispondente ha intersezione o meno

whatEdgeInters: contiene gli indici dei lati che contengono intersezioni

_intersectionPoints: contiene tutti  i punti di intersezione nell’ordine in cui vengono trovati dal programma (dal lato 0 in poi, in senso antiorario)

_cuttedPolygons: contiene tutti i nuovi poligoni tramite le coordinate dei punti

_newPolygonsVertex:  contiene tutti i nuovi poligoni tramite i numeri assegnati ai vertici

pol: è un vettore di appoggio per riempire _cuttedPolygons, e contiene un poligono tagliato per volta

polNumerati: è un vettore di appoggio per riempire _newPolygonsVertex, e contiene un poligono tagliato per volta

_distances: contiene le misure delle distanze tra la prima intersezione e tutti gli altri punti sulla retta, in ordine crescente

_puntiCorrispondenti: contiene i punti a cui si riferiscono le distanze, e quindi le coordinate dei punti successivi alla prima intersezione, in ordine

ordineRetta:  contiene gli indici dei punti che trovo in ordine sulla retta, esclusa la prima intersezione

rangeI: contiene l’indice della prima intersezione che inserisco nel poligono che sto tagliando, l’indice corrisponde alla sua posizione sulla retta (sempre escludendo la prima intersezione)

rangeF: stessa cosa di rangeI, ma per la seconda intersezione trovata

(rangeI e rangeF mi servono per sapere quali punti ci sono in mezzo sulla retta, tra le intersezioni che considero)

