#ifndef POL_H
#define POL_H

#include <iostream>
#include <Eigen>
#include <vector>

using namespace Eigen;
using namespace std;

//qui implemento tutte le interfacce con le funzioni virtuali

namespace PolLibrary {

  class IPoint {
    public:
      virtual double Distance(const Vector2d& A, const Vector2d& B) const = 0;
      virtual void CalcPoints(const vector<Vector2d> vertex, const int numVertex,
                              const vector<Vector2d> intersectionPoints , const int numIntersections,
                              const Vector2d s1, const Vector2d s2) = 0;
      virtual vector<Vector2d> NewPoints() const = 0;
      virtual int NumNewPoints() const = 0;
  };

  class ISegment {
    public:
      virtual double CoeffAngolare(Vector2d s1, Vector2d s2) const = 0;
      virtual Vector2d Direzione(Vector2d s1, Vector2d s2) const = 0;
  };

  class IPolygon {
    public:
      virtual void Cut() = 0;
      virtual int& NumIntersection() = 0;
      virtual vector<Vector2d> IntersectionPoints() const = 0;
      virtual void ChooseFunction() = 0;
      virtual void NewPolygonsTwoInt() = 0;
      virtual void NewPolygonsMoreInt() = 0;
      virtual vector<vector<Vector2d>> CuttedPolygons() = 0;
      virtual vector<vector<int>> NewPolygonsVertex() = 0;
  };

  class IIntersector {
    public:
      virtual void SetStraight(const Vector2d& origin1, const Vector2d& end1) = 0;
      virtual void SetEdge(const Vector2d& origin2, const Vector2d& end2) = 0;
      virtual bool Intersection() = 0;
      virtual const Vector2d& IntersectionCoordinates() = 0;
      virtual const Vector2d& ParametricCoordinates() const = 0;
  };

}

#endif // POL_H
