#ifndef SEGMENT_H
#define SEGMENT_H

#include <iostream>
#include <Intersector.hpp>
#include <Eigen>
#include <vector>

#include <Pol.hpp>

using namespace Eigen;
using namespace std;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class Segment: public ISegment {
    private:
      Vector2d _s1, _s2;

      void Reset() {};

    public:
      Segment(const Vector2d& s1, const Vector2d& s2) {_s1 = s1;
                                                       _s2 = s2;}

      double CoeffAngolare(Vector2d s1, Vector2d s2) const;
      Vector2d Direzione(Vector2d s1, Vector2d s2) const; //restituisce il vettore che dà la direzione del segmento
  };
}

#endif // SEGMENT_H
