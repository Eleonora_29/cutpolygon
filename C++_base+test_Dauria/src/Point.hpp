#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <Eigen>
#include <vector>
#include <Segment.hpp>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class Point: public IPoint {

    public:
      static double tolerance;

    private:
      vector<Vector2d> _newPoints;

      void Reset();

    public:
      Point() {}; //costruttore copia
      double Distance (const Vector2d& A, const Vector2d& B) const;
      void CalcPoints (const vector<Vector2d> vertex, const int numVertex,
                       const vector<Vector2d> intersectionPoints , const int numIntersections,
                       const Vector2d s1, const Vector2d s2);
      vector<Vector2d> NewPoints () const {return _newPoints;} //contiene prima i vertici e poi le intersezioni
      int NumNewPoints () const {return _newPoints.size();}
  };
}

#endif // POINT_H
