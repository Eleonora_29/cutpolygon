#include "Segment.hpp"

namespace PolygonCutLibrary {

double Segment::CoeffAngolare(Vector2d s1, Vector2d s2) const
{
    double coeff;
    coeff = (s2(1)-s1(1))/(s2(0)-s1(0));
    return coeff;
}

Vector2d Segment::Direzione(Vector2d s1, Vector2d s2) const
{
    Vector2d direz;
    direz = s2 - s1;
    return direz;
}

}
