#ifndef __TEST_SEGMENT_H
#define __TEST_SEGMENT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace SegmentTesting {

  TEST(TestPolygonCut, TestSegment)
  {
    Vector2d s1, s2;
    s1 << 0.0, 3.0;
    s2 << 5.0, 1.0;

    try
    {
         PolygonCutLibrary::Segment(s1, s2);
    }
    catch (const exception& exception)
    {
         FAIL();
    }

    PolygonCutLibrary::Segment segment(s1,s2);

    try
    {
        EXPECT_EQ(segment.CoeffAngolare(s1,s2), -0.4);
    }
    catch (const exception& exception)
    {
        FAIL();
    }

  }
}

#endif // __TEST_SEGMENT_H
