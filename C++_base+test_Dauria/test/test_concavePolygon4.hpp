#ifndef __TEST_CONCAVEPOLYGON4_H
#define __TEST_CONCAVEPOLYGON4_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace ConcavePolygon4Testing {

  TEST(TestPolygonCut, TestConcavePolygon4)
  {
      //assumo che il primo punto dato sia in basso a sinistra
      Vector2d v1, v2, v3, v4, v5, v6, v7, v8, v9, v10;
      v1 << -3.0, -2.0;
      v2 << 2.0, -2.0;
      v3 << 0.0, -1.0;
      v4 << 3.0, 1.0;
      v5 << 0.0, 2.0;
      v6 << 3.0, 2.0;
      v7 << 3.0, 3.0;
      v8 << -1.0, 3.0;
      v9 << -3.0, 1.0;
      v10 << 0.0, 0.0;


      Vector2d s1, s2; //li ho scelti io perché i suoi erano fuori dal poligono
      s1 << -1, -1;
      s2 << 1, 1;

      vector<Vector2d> concavePolygon4Vertex;
      concavePolygon4Vertex.push_back(v1);
      concavePolygon4Vertex.push_back(v2);
      concavePolygon4Vertex.push_back(v3);
      concavePolygon4Vertex.push_back(v4);
      concavePolygon4Vertex.push_back(v5);
      concavePolygon4Vertex.push_back(v6);
      concavePolygon4Vertex.push_back(v7);
      concavePolygon4Vertex.push_back(v8);
      concavePolygon4Vertex.push_back(v9);
      concavePolygon4Vertex.push_back(v10);

      try
      {
         PolygonCutLibrary::Point();
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: Point point;

      try
      {
        PolygonCutLibrary::Polygon(10,concavePolygon4Vertex,s1,s2,point);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::Polygon concavePolygon4(10, concavePolygon4Vertex, s1, s2, point);

      try
      {
        concavePolygon4.Cut();
        EXPECT_EQ(5, concavePolygon4.NumIntersection());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters1, inters2, inters3, inters4, inters5, a, b, c, d, e;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << -2.0, -2.0;
      inters2 << 1.5, 1.5;
      inters3 << 2.0, 2.0;
      inters4 << 3.0, 3.0;
      inters5 << 0.0, 0.0;


      try
      {
        // i punti di intersezione che calcola
        a = concavePolygon4.IntersectionPoints()[0];
        b = concavePolygon4.IntersectionPoints()[1];
        c = concavePolygon4.IntersectionPoints()[2];
        d = concavePolygon4.IntersectionPoints()[3];
        e = concavePolygon4.IntersectionPoints()[4];
        intersectionPoints = concavePolygon4.IntersectionPoints();
        //controllo che coincidano
        EXPECT_EQ(a, inters1);
        EXPECT_EQ(b, inters2);
        EXPECT_EQ(c, inters3);
        EXPECT_EQ(d, inters4);
        EXPECT_EQ(e, inters5);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> newPoints;
      try
      {
        point.CalcPoints(concavePolygon4Vertex, 10, intersectionPoints, 5, s1, s2);
        EXPECT_EQ(15, point.NumNewPoints());
        newPoints = point.NewPoints();
        // controllo che i primi 10 punti corrispondano ai vertici
        EXPECT_EQ(v1, newPoints[0]);
        EXPECT_EQ(v2, newPoints[1]);
        EXPECT_EQ(v3, newPoints[2]);
        EXPECT_EQ(v4, newPoints[3]);
        EXPECT_EQ(v5, newPoints[4]);
        EXPECT_EQ(v6, newPoints[5]);
        EXPECT_EQ(v7, newPoints[6]);
        EXPECT_EQ(v8, newPoints[7]);
        EXPECT_EQ(v9, newPoints[8]);
        EXPECT_EQ(v10, newPoints[9]);
        // controllo che 3 siano le intersezioni non coincidenti con i vertici
        EXPECT_EQ(newPoints[10], a);
        EXPECT_EQ(newPoints[11], b);
        EXPECT_EQ(newPoints[12], c);
        // controllo che gli ultimi due siano gli estremi del segmento
        EXPECT_EQ(newPoints[13], s1);
        EXPECT_EQ(newPoints[14], s2);
        concavePolygon4.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2, cuttedPolygon3, cuttedPolygon4;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2, newVertex3, newVertex4;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(v10);
      newVertex1.push_back(0);
      newVertex1.push_back(10);
      newVertex1.push_back(11);
      newVertex1.push_back(9);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v2);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(v4);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s2);
      cuttedPolygon2.push_back(e);
      cuttedPolygon2.push_back(s1);
      newVertex2.push_back(10);
      newVertex2.push_back(1);
      newVertex2.push_back(2);
      newVertex2.push_back(3);
      newVertex2.push_back(13);
      newVertex2.push_back(12);
      newVertex2.push_back(9);
      newVertex2.push_back(11);

      cuttedPolygon3.push_back(b);
      cuttedPolygon3.push_back(v5);
      cuttedPolygon3.push_back(c);
      cuttedPolygon3.push_back(d);
      cuttedPolygon3.push_back(v8);
      cuttedPolygon3.push_back(v9);
      cuttedPolygon3.push_back(v10);
      cuttedPolygon3.push_back(s2);
      newVertex3.push_back(13);
      newVertex3.push_back(4);
      newVertex3.push_back(14);
      newVertex3.push_back(6);
      newVertex3.push_back(7);
      newVertex3.push_back(8);
      newVertex3.push_back(9);
      newVertex3.push_back(12);

      cuttedPolygon4.push_back(c);
      cuttedPolygon4.push_back(v6);
      cuttedPolygon4.push_back(v7);
      newVertex4.push_back(14);
      newVertex4.push_back(5);
      newVertex4.push_back(6);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allCuttedPol.push_back(cuttedPolygon3);
      allCuttedPol.push_back(cuttedPolygon4);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);
      allNewVertex.push_back(newVertex3);
      allNewVertex.push_back(newVertex4);

      try
      {
      EXPECT_EQ(allCuttedPol, concavePolygon4.CuttedPolygons());
      EXPECT_EQ(allNewVertex, concavePolygon4.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_CONCAVEPOLYGON4_H
