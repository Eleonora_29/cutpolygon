#ifndef __TEST_INTERSECTOR_H
#define __TEST_INTERSECTOR_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace IntersectorTesting {

  TEST(TestPolygonCut, TestIntersector)
  {

      Vector2d p1, p2;
      p1 << 1.0, 1.0;
      p2 << 5.0, 3.0;

      Vector2d s1, s2;
      s1 << 3.0, 0;
      s2 << 3.0, 5.0;

    try
    {
          PolygonCutLibrary::Intersector();
    }
    catch (const exception& exception)
    {
          FAIL();
    }

    PolygonCutLibrary::Intersector intersector;

    try
    {
        intersector.SetEdge(p1, p2);
        intersector.SetStraight(s1, s2);
        EXPECT_TRUE(intersector.Intersection()); //interseca
    }
    catch (const exception& exception)
    {
          FAIL();
    }

    Vector2d w, z;
    w << 0.4, 0.5; //coordinate parametriche che mi aspetto
    z << 3.0, 2.0; //punto di intersezione che mi aspetto

    try
    {
        EXPECT_EQ(w, intersector.ParametricCoordinates()); // controllo che calcoli correttamente le coordinate parametriche
        EXPECT_EQ(z, intersector.IntersectionCoordinates()); //controllo che calcoli correttamente l'intersezione
    }
    catch (const exception& exception)
    {
          FAIL();
    }
  }

}

#endif // __TEST_INTERSECTOR_H
