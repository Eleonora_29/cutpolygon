#ifndef __TEST_POINT_H
#define __TEST_POINT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace PointTesting {

  TEST(TestPolygonCut, TestPoint)
  {
      Vector2d p1, p2;
      p1 << 7.0, 3.0;
      p2 << 9.0, 2.0;

      try
      {
           PolygonCutLibrary::Point();
      }
      catch (const exception& exception)
      {
           FAIL();
      }

      PolygonCutLibrary::Point point;
      vector<Vector2d> vertex, intersPoint;
      Vector2d v1, v2, i, s1, s2;
      v1 << 1.0, 2.0;
      v2 << 4.0, 2.0;
      vertex.push_back(v1);
      vertex.push_back(v2);
      i << 3.0, 2.0;
      intersPoint.push_back(i);
      s1 << 3.0, 2.0;
      s2 << 3.0, 0.0;

      vector<Vector2d> newPoints; // quello che mi aspetto
      newPoints.push_back(v1);
      newPoints.push_back(v2);
      newPoints.push_back(i);
      newPoints.push_back(s2);

      try
      {
          EXPECT_TRUE((point.Distance(p1,p2) - 2.23607) < 1e-6);
          point.CalcPoints(vertex, 2, intersPoint, 1, s1, s2);
          EXPECT_EQ(point.NewPoints(), newPoints);
          EXPECT_EQ(point.NumNewPoints(), 4);
      }
      catch (const exception& exception)
      {
          FAIL();
      }

  }
}

#endif // __TEST_POINT_H
