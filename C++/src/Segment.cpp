#include "Segment.hpp"

namespace PolygonCutLibrary {

  Segment::Segment(const Point& s1,
                   const Point& s2)
  {
    _x1 = s1._x;
    _x2 = s2._x;
    _y1 = s1._y;
    _y2 = s2._y;
  }
  Segment::~Segment()
  { 
  }

  double Segment::coeffAngolare() const
  {
    double coeff;
    coeff = (_y2 - _y1)/(_x2 - _x1);
    return coeff;
  }

}
