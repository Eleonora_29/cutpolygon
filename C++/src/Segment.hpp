#ifndef SEGMENT_H
#define SEGMENT_H

#include <iostream>
#include <Point.hpp>

using namespace std;

namespace PolygonCutLibrary {

  class Segment {
    public:
      double _x1, _x2;
      double _y1, _y2;
      Segment(const Point& s1,
              const Point& s2);
      ~Segment();

      double coeffAngolare() const;

  };
}

#endif // SEGMENT_H
