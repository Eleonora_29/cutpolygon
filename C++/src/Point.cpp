#include "Point.hpp"
#include <math.h>

namespace PolygonCutLibrary {

  Point::Point(const double &x,
               const double &y)
  {
       _x=x;
       _y=y;
  }

  Point::~Point()
  {

  }

  double Point::Distance(const Point &X, const Point &Y)
  {
      return sqrt(pow((X._x - Y._x), 2) + pow((X._y - Y._y), 2));
  }

}
