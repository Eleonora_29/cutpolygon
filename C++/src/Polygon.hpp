#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <vector>
#include <Point.hpp>

using namespace std;

namespace PolygonCutLibrary {

  class Polygon {
    public:
      vector<Point *> _vertex;

      Polygon(int numVertex, vector<Point *> vertex);
      ~Polygon();
  };
}

#endif // POLYGON_H
