from unittest import TestCase
import src.Polygon as Polyg
import numpy as np


class TestPolygon(TestCase):
    def test_Cut_Rettangolo(self):
        vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
        s1 = np.array([[2], [1.2]])
        s2 = np.array([[4], [3]])
        edgeWithInters, intersection_point, num_intersection = Polyg.Polygon.Cut(Polyg, s1, s2, 4, vertex)
        intersection_point = str(intersection_point)
        num_intersection = str(num_intersection)
        edgeWithInters = str(edgeWithInters)
        result = edgeWithInters + "\n" + intersection_point + "\n" + num_intersection
        self.assertEqual(result, "[True, False, True, False]\n[[1.77777778 4.11111111]\n [1.         3.1       ]]\n2")

    def test_Cut_Pentagono(self):
        vertex = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
        s1 = np.array([[1.4], [2.75]])
        s2 = np.array([[3.6], [2.2]])
        edgeWithInters, intersection_point, num_intersection = Polyg.Polygon.Cut(Polyg, s1, s2, 5, vertex)
        intersection_point = str(intersection_point)
        num_intersection = str(num_intersection)
        edgeWithInters = str(edgeWithInters)
        result = edgeWithInters +"\n"+ intersection_point +"\n"+ num_intersection
        self.assertEqual(result,  "[True, False, False, True, False]\n[[4.  1.2]\n [2.1 2.8]]\n2")

    def test_Cut_Concavo1(self):
        vertex = np.array([[1.5, 5.6, 5.5, 4, 3.2, 1], [1, 1.5, 4.8, 6.2, 4.2, 4]])
        s1 = np.array([[2], [3.7]])
        s2 = np.array([[4.1], [5.9]])
        edgeWithInters, intersection_point, num_intersection = Polyg.Polygon.Cut(Polyg, s1, s2, 6, vertex)
        intersection_point = str(intersection_point)
        num_intersection = str(num_intersection)
        edgeWithInters = str(edgeWithInters)
        result = edgeWithInters +"\n"+ intersection_point +"\n"+ num_intersection
        self.assertEqual(result,  "[False, False, True, True, True, True]\n[[4.20432692 3.72131148 2.40859729 1.19121622]\n [6.00929487 5.50327869 4.1280543  2.8527027 ]]\n4")

    def test_NewPolygonsTwoInt_Rettangolo(self):
        vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
        s1 = np.array([[2], [1.2]])
        s2 = np.array([[4], [3]])
        num_vertex = 4
        edgeWithInters, intersection_points, num_intersection = Polyg.Polygon.Cut(Polyg, s1, s2,  num_vertex, vertex)
        cuttedPolygons, newPolygonsVertex = Polyg.Polygon.NewPolygonsTwoInt(Polyg, vertex, num_vertex, edgeWithInters, intersection_points, s1, s2, num_intersection)
        cuttedPolygons = str(cuttedPolygons)
        newPolygonsVertex = str(newPolygonsVertex)
        result = cuttedPolygons + "\n" + newPolygonsVertex
        self.assertEqual(result, "[[1.         1.77777778 2.         4.         4.11111111 1.\n  1.77777778 5.         5.         4.11111111 4.         2.        ]\n [1.         1.         1.2        3.         3.1        3.1\n  1.         1.         3.1        3.1        3.         1.2       ]]\n[0. 4. 5. 6. 7. 3. 4. 1. 2. 7. 6. 5.]")

    def test_NewPolygonsTwoInt_Pentagono(self):
        vertex = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
        s1 = np.array([[1.4], [2.75]])
        s2 = np.array([[3.6], [2.2]])
        num_vertex = 5
        edgeWithInters, intersection_points, num_intersection = Polyg.Polygon.Cut(Polyg, s1, s2,  num_vertex, vertex)
        cuttedPolygons, newPolygonsVertex = Polyg.Polygon.NewPolygonsTwoInt(Polyg, vertex, num_vertex, edgeWithInters, intersection_points, s1, s2, num_intersection)
        cuttedPolygons = str(cuttedPolygons)
        newPolygonsVertex = str(newPolygonsVertex)
        result = cuttedPolygons + "\n" + newPolygonsVertex
        self.assertEqual(result,"[[2.5  4.   1.4  3.6  1.2  1.   4.   3.4  1.6  1.2  1.4  3.6 ]\n [1.   2.1  1.4  2.2  2.8  2.1  2.1  4.2  4.2  2.8  2.75 2.2 ]]\n[0. 1. 5. 6. 7. 4. 1. 2. 3. 7. 6. 5.]")

    def test_NewPolygonsMoreInt(self):
        pass

'''
Test vuoto da fare
'''

