import numpy as np


class Intersector:
    def __init__(self):
        self.matrix_tangent_vector = []

    def SetStraight(self, origin_l1: np.array((2, 1), float), end_l1: np.array((2, 1), float)):
        self.matrix_tangent_vector = np.zeros((2, 2), float)
        self.matrix_tangent_vector[0][0] = end_l1[0][0] - origin_l1[0][0]  # Sulle x
        self.matrix_tangent_vector[1][0] = end_l1[1][0] - origin_l1[1][0]  # Sulle y
        return self.matrix_tangent_vector

    def SetEdge(self, matrix_tangent_vector, origin_l2: np.array((2, 1), float), end_l2: np.array((2, 1), float)):
        self.matrix_tangent_vector = matrix_tangent_vector
        self.matrix_tangent_vector[0][1] = origin_l2[0][0] - end_l2[0][0]  # Sulle x
        self.matrix_tangent_vector[1][1] = origin_l2[1][0] - end_l2[1][0]  # Sulle y
        return self.matrix_tangent_vector

    def Intersection(self, matrix_tangent_vector, origin_l1: np.array((2,1), float), origin_l2: np.array((2, 1), float)):
        self.matrix_tangent_vector = matrix_tangent_vector
        toleranceParallelism = 1e-7
        toleranceIntersection = 1e-7
        right_hand_side = np.zeros((2, 1), float) #Dichiarazione b
        right_hand_side[0][0] = origin_l2[0][0] - origin_l1[0][0] #Sulle x
        right_hand_side[1][0] = origin_l2[1][0] - origin_l1[1][0] #Sulle y
        det = np.linalg.det(self.matrix_tangent_vector)
        intersection: bool = False
        check = toleranceParallelism * toleranceParallelism * np.linalg.norm(self.matrix_tangent_vector[:, 0]) * np.linalg.norm(self.matrix_tangent_vector[:, 1])
        if det*det >= check:
            solve_matrix = np.zeros((2,2), float)
            solve_matrix = np.linalg.inv(self.matrix_tangent_vector) #Calcolo l'inversa della matrice
            resultParametricCoordinates = (np.dot(solve_matrix, right_hand_side)) #Prodotto matriciale
            if (resultParametricCoordinates[1] - 1.0 < toleranceIntersection and resultParametricCoordinates[1] > -toleranceIntersection): #Capire con Ele che cosa devo controllare
                intersection = True
            else:
                intersection = False
        return resultParametricCoordinates, intersection

    def IntersectionCoordinates(self, matrix_tangent_vector,  origin_l1: np.array((2,1), float), resultParametricCoordinates: np.array((2,1), float)):
        self.matrix_tangent_vector = matrix_tangent_vector
        intersection_coordinates = np.zeros((2,1), float) #Dichiarazione della matrice
        intersection_coordinates[0][0] = origin_l1[0][0] + self.matrix_tangent_vector[0][0] *  resultParametricCoordinates[0]  # Sulle x
        intersection_coordinates[1][0] = origin_l1[1][0] + self.matrix_tangent_vector[1][0] *  resultParametricCoordinates[0]  # Sulle y
        return intersection_coordinates




'''
Classe Intersector -> FUNZIONA
1. Metodo cotruttore: costruisce matrix_tangent_vector
2. Metodo SetStraight: Riempie la prima colonna di matrix_tangent_vector
3. Metodo SetEdge: Riempie la seconda colonna di matrix_tangent_vector
4. Metodo Intersection: Verifica se l'intersezione è presente o meno e ritorna le coordinate parametriche
5. Metodo IntersectionCoordinates: trova le coordinate delle intersezioni
'''
