from unittest import TestCase
import src.Vector2d as V2d


class TestVector2d(TestCase):
    def test_InMatrix(self):
        vector = V2d.Vector2d(1, 2)
        self.assertEqual(str(V2d.Vector2d.InMatrix(vector)), "[[1.]\n [2.]]")

    def test_X(self):
        vector = V2d.Vector2d(1, 2)
        self.assertEqual(V2d.Vector2d.X(vector), 1)

    def test_Y(self):
        vector = V2d.Vector2d(1, 2)
        self.assertEqual(V2d.Vector2d.Y(vector), 2)


'''
Classe di TestVector2d -> FUNZIONA
1. Test su InMatrix
2. Test su X
3. Test su Y
'''
