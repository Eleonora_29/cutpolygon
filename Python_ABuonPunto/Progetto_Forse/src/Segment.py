import numpy as np
import math as math


class Segment:
    def __init__(self, s1, s2):
        self.s1 = s1
        self.s2 = s2

    def tomatrix(self):
        segment = np.concatenate((self.s1, self.s2), axis=1)
        return segment

    def distance(self):
        return math.sqrt(math.pow((self.s1[0][0] - self.s2[0][0]), 2) + math.pow((self.s1[1][0] - self.s2[1][0]), 2))

    def angularcoefficient(self):
        tol = 1e-6
        m_y = self.s2[1][0] - self.s1[1][0]
        m_x = self.s2[0][0] - self.s1[0][0]
        if (-tol <= m_x <= tol) and (m_y <= -tol or m_y >= tol):
            return 'Non Define'
        elif (-tol <= m_y <= tol) and (m_x <= -tol or m_x >= tol):
            return 0
        elif (-tol <= m_x <= tol) and (-tol <= m_y <= tol):
            return 'The two past points coincide'
        else:
            return m_y / m_x


'''
Classe Segmento ->FUNZIONA
1. Metodo costruttore: prende in input s1 e s2 e li inserisce in segment(s1, s2)
2. Metodo tomatrix: trasforma in una unica matrice
3. Metodo angularcoefficient: restiuisce il coefficiente della retta che passa per s1 e s2
-Tengo conto della tolleranza

Ecco come costruire un oggetto segmento
s1 = np.array([[1],[2]])
s2 = np.array([[3],[4]])
segment = Seg.Segment(s1, s2)
'''
