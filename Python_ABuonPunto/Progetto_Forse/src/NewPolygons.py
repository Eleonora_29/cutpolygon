import numpy as np
import src.Intersector as Int
import src.Segment as Seg


class NewPolygons:
    def __init__(self, vertices, s1, s2):
        self.vertices = vertices
        self.edge_with_intersection = []
        self.intersection_points = np.array([[0], [0]], float)
        self.s1 = s1
        self.s2 = s2

    def cut(self):
        i: int = 0
        num_intersections = 1
        num_vertex = np.size(self.vertices, axis=1)
        for i in range(0, num_vertex, 1):
            matrix_tangent_vector = Int.Intersector()
            Int.Intersector.setstraight(matrix_tangent_vector, self.s1, self.s2)
            l0 = np.zeros((2, 1), float)
            l0[0][0] = self.vertices[0][0]
            l0[1][0] = self.vertices[1][0]
            li = np.zeros((2, 1), float)
            li[0][0] = self.vertices[0][i]
            li[1][0] = self.vertices[1][i]
            if i != (num_vertex - 1):
                li_1 = np.zeros((2, 1), float)
                li_1[0][0] = self.vertices[0][i + 1]
                li_1[1][0] = self.vertices[1][i + 1]
                Int.Intersector.setedge(matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.setedge(matrix_tangent_vector, li, l0)
            parametric_coord, inters = Int.Intersector.intersection(matrix_tangent_vector, self.s1, li)
            if inters is True:
                intersectionCoordinates = Int.Intersector.intersectioncoordinates(matrix_tangent_vector, self.s1,
                                                                                  parametric_coord)
                if (num_intersections == 0 or (
                        intersectionCoordinates[0][0] != self.intersection_points[0][num_intersections - 1] or
                        intersectionCoordinates[1][0] != self.intersection_points[1][num_intersections - 1])):
                    self.edge_with_intersection.append(True)
                    num_intersections += 1
                    flag = np.array([[0], [0]], float)
                    flag = intersectionCoordinates
                    self.intersection_points = np.append(self.intersection_points, flag, axis=1)
                else:
                    self.edge_with_intersection.append(False)

            else:
                self.edge_with_intersection.append(False)
        self.intersection_points = np.transpose(self.intersection_points)
        self.intersection_points = np.delete(self.intersection_points, 0, 0)
        self.intersection_points = np.transpose(self.intersection_points)
        num_intersections -= 1
        return num_intersections

    def choosefunction(self, num_intersections):
        if num_intersections == 2:
            NewPolygons.newpolygonstwoint(self)
        else:
            NewPolygons.NewPolygonsMoreInt(self)

    def newpolygonstwoint(self):
        pol1 = np.array([[0], [0]], float)
        pol2 = np.array([[0], [0]], float)
        polNum1 = [0]
        polNum2 = []
        flag = np.zeros((2, 1), float)
        flag1 = np.zeros((2, 1), float)
        flag[0][0] = self.vertices[0][0]
        flag[1][0] = self.vertices[1][0]
        pol1 = np.append(pol1, flag, axis=1)
        pol1 = np.transpose(pol1)
        pol1 = np.delete(pol1, 0, 0)
        pol1 = np.transpose(pol1)
        punti_corrispondenti = np.array([[0], [0]], float)
        cuttedPolygons = np.array([[0], [0]], float)
        newPolygonsVertex = []
        temp2 = np.array([[0], [0]], float)
        distances = []
        i: int = 0
        num_vertex = np.size(self.vertices, axis=1)
        num_intersection = np.size(self.intersection_points, axis=1)
        n: int = num_vertex
        p1: int = 1
        p2: int = 0
        v1 = 1
        x: int = num_vertex
        while v1<x:
            if self.edge_with_intersection[v1 - 1] is True:
                x = v1-1
                v2 = v1
                if self.intersection_points[0][i] != pol1[0][p1 - 1] or self.intersection_points[1][i] != pol1[1][p1 - 1]:
                    if self.intersection_points[0][i] != self.vertices[0][v1] or self.intersection_points[1][i] != self.vertices[1][v1]:
                        flag[0][0] = self.intersection_points[0][i]
                        flag[1][0] = self.intersection_points[1][i]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, n)
                        polNum2 = np.append(polNum2, n)
                        n += 1

                    else:
                        flag[0][0] = self.vertices[0][v1]
                        flag[1][0] = self.vertices[1][v1]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, v1)
                        polNum2 = np.append(polNum2, v1)
                        v2 += 1

                    p1 += 1
                    p2 += 1

                flag[0][0] = self.intersection_points[0][i]
                flag[1][0] = self.intersection_points[1][i]
                segment = Seg.Segment(flag, self.s1)
                e = Seg.Segment.distance(segment)
                distances = np.append(distances, e)
                punti_corrispondenti = np.append(punti_corrispondenti, self.s1, axis=1)
                segment = Seg.Segment(flag, self.s2)
                f = Seg.Segment.distance(segment)
                distances = np.append(distances, f)
                punti_corrispondenti = np.append(punti_corrispondenti, self.s2, axis=1)

                z = i + 1
                for z in range(z, num_intersection):
                    flag[0][0] = self.intersection_points[0][i]
                    flag[1][0] = self.intersection_points[1][i]
                    flag1[0][0] = self.intersection_points[0][z]
                    flag1[1][0] = self.intersection_points[1][z]
                    segment = Seg.Segment(flag, flag1)
                    d = Seg.Segment.distance(segment)
                    distances = np.append(distances, d)
                    punti_corrispondenti = np.append(punti_corrispondenti, flag1, axis=1)

                distance_size = np.size(distances)
                for z in range(0, distance_size-1):
                    min = z
                    w = z + 1
                    for w in range(w, distance_size):
                        if distances[w] < distances[min]:
                            min = w
                    temp1 = distances[min]
                    temp2[0][0] = punti_corrispondenti[0][min+1]
                    temp2[1][0] = punti_corrispondenti[1][min+1]
                    distances[min] = distances[z]
                    punti_corrispondenti[0][min+1] = punti_corrispondenti[0][z+1]
                    punti_corrispondenti[1][min+1] = punti_corrispondenti[1][z+1]
                    distances[z] = temp1
                    punti_corrispondenti[0][z+1] = temp2[0][0]
                    punti_corrispondenti[1][z+1] = temp2[1][0]

                for a in range(0, distance_size):
                    if punti_corrispondenti[0][a+1] != pol1[0][p1-1] or punti_corrispondenti[1][a+1] != pol1[1][p1-1]:
                        flag[0][0] = punti_corrispondenti[0][a + 1]
                        flag[1][0] = punti_corrispondenti[1][a + 1]
                        pol1 = np.append(pol1, flag, axis=1)
                        p1 += 1
                        polNum1 = np.append(polNum1, n)
                        n += 1

                for b in range(v1, num_vertex):
                    if self.edge_with_intersection[b] is True:
                        for c in range(b+1, num_vertex):
                            flag[0][0] = self.vertices[0][c]
                            flag[1][0] = self.vertices[1][c]
                            pol1 = np.append(pol1, flag, axis=1)
                            polNum1 = np.append(polNum1, c)

                        for c in range(v2, b+1):
                            if self.vertices[0][c] != pol2[0][p2] or self.vertices[1][c] != pol2[1][p2]:
                                flag[0][0] = self.vertices[0][c]
                                flag[1][0] = self.vertices[1][c]
                                pol2 = np.append(pol2, flag, axis=1)
                                polNum2 = np.append(polNum2, c)

                        flag[0][0] = self.intersection_points[0][1]
                        flag[1][0] = self.intersection_points[1][1]
                        pol2 = np.append(pol2, flag, axis=1)
                        m = n - 1
                        polNum2 = np.append(polNum2, m)
                        m -= 1

                        if f < e:
                            pol2 = np.append(pol2, self.s1, axis=1)
                            pol2 = np.append(pol2, self.s2, axis=1)
                        else:
                            pol2 = np.append(pol2, self.s2, axis=1)
                            pol2 = np.append(pol2, self.s1, axis=1)

                        polNum2 = np.append(polNum2, m)
                        polNum2 = np.append(polNum2, m - 1)

            else:
                if self.vertices[0][v1] != pol1[0][p1 - 1] or self.vertices[1][v1] != pol1[1][p1 - 1]:
                    flag[0][0] = self.vertices[0][v1]
                    flag[1][0] = self.vertices[1][v1]
                    pol1 = np.append(pol1, flag, axis=1)
                    polNum1 = np.append(polNum1, v1)
            v1 += 1
        pol2 = np.transpose(pol2)
        pol2 = np.delete(pol2, 0, 0)
        pol2 = np.transpose(pol2)

        pol = np.concatenate((pol1, pol2), axis=1)
        cuttedPolygons = np.append(cuttedPolygons, pol, axis=1)
        cuttedPolygons = np.transpose(cuttedPolygons)
        cuttedPolygons = np.delete(cuttedPolygons, 0, 0)
        cuttedPolygons = np.transpose(cuttedPolygons)
        newPolygonsVertex = np.append(newPolygonsVertex, polNum1)
        newPolygonsVertex = np.append(newPolygonsVertex, polNum2)

        return cuttedPolygons, newPolygonsVertex

    def NewPolygonsMoreInt(self):
        pass


'''
Rett -> OK
vertices = np.array([[1.0, 5.0, 5.0, 1.0], [1.0, 1.0, 3.1, 3.1]])
s1 = np.array([[2.0], [1.2]])
s2 = np.array([[4.0], [3.0]])
pol = NewPolygons(vertices, s1, s2)
edgeWithInters, intersection_points, num_intersection = NewPolygons.cut(pol)
NewPolygons.choosefunction(pol, num_intersection)
cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonstwoint(pol)
print(cuttedPolygons, newPolygonsVertex)

Pol-> OK
vertices = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
s1 = np.array([[1.4], [2.75]])
s2 = np.array([[3.6], [2.2]])
pol = NewPolygons(vertices, s1, s2)
edgeWithInters, intersection_point, num_intersection = NewPolygons.cut(pol)
NewPolygons.choosefunction(pol, num_intersection)
cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonstwoint(pol)
print(cuttedPolygons, newPolygonsVertex)

Convex -> OK
vertices = np.array([[2.4, 5.8, 3.3, 1.7], [1.1, 2.6, 6.8, 6.]])
print(vertices)
s1 = np.array([[2.8], [2.0]])
s2 = np.array([[4.3], [3.9]])
pol = NewPolygons(vertices, s1, s2)
num_intersection = NewPolygons.cut(pol)
NewPolygons.choosefunction(pol, num_intersection)
cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonstwoint(pol)
print(cuttedPolygons, newPolygonsVertex)

Concavo -> OK
vertices =  np.array([[3.5, 6.7, 5.3, 3.7, 2.2, 1.], [1., 4.2, 6.8, 6., 8.5, 5.6]])
s1 = np.array([[2], [6.5]])
s2 = np.array([[5.1], [3.9]])
pol = NewPolygons(vertices, s1, s2)
edgeWithInters, intersection_point, num_intersection = NewPolygons.cut(pol)
print(edgeWithInters, "\n", intersection_point )
NewPolygons.choosefunction(pol, num_intersection)
cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonstwoint(pol)
print(cuttedPolygons, newPolygonsVertex)

vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
newpol = NewPolygons(vertex)
s1 = np.array([[2], [1.2]])
s2 = np.array([[4], [3]])
edgeWithInters, intersection_point, num_intersection = NewPolygons.cut(newpol, s1, s2)
print("edgeWithInters\n", edgeWithInters)
print("intersection_point\n", intersection_point)
print("num_intersection\n", num_intersection)

'''

