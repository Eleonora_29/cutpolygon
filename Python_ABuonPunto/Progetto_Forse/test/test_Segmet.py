from unittest import TestCase
import src.Segment as Seg
import numpy as np


class TestSegment(TestCase):
    def test_tomatrix(self):
        try:
            s1 = np.array([[1], [2]])
            s2 = np.array([[5], [9]])
            segment = Seg.Segment(s1, s2)
            self.assertEqual(str(Seg.Segment.tomatrix(segment)), "[[1 5]\n [2 9]]")
        except:
            self.fail()

    def test_angularcoefficient(self):
        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[0], [2]])
            segment = Seg.Segment(s1, s2)
            self.assertTrue(abs(Seg.Segment.angularcoefficient(segment) - 1) < 1e-6)
        except:
            self.fail()

        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[2], [2]])
            segment = Seg.Segment(s1, s2)
            self.assertEqual(Seg.Segment.angularcoefficient(segment), 'Non Define')

        except:
            self.fail()

        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[0], [4]])
            segment = Seg.Segment(s1, s2)
            self.assertTrue(abs(Seg.Segment.angularcoefficient(segment) - 0) < 1e-6)
        except:
            self.fail()

        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[2], [4]])
            segment = Seg.Segment(s1, s2)
            self.assertEqual(Seg.Segment.angularcoefficient(segment), 'The two past points coincide')
        except:
            self.fail()