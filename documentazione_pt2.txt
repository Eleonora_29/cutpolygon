Documentazione parte 2:

Requirement:
Ricoprire un dominio rettangolare tramite delle "piastrelle" contenenti un poligono concavo, racchiuso da una bounding box.
Le parti in eccesso vengono tagliate. Si viene così a formare una mesh di poligoni.
In particolare bisogna controllare che la somma delle aree di ciascun elemento inserito, corrisponda all'area dell'intero dominio.

Classe polygon:
Il costruttore contiene una lista di vertici, corrispondenti al poligono concavo da includere nella bounding box
ComputeArea: calcola l'area del poligono passato al costruttore, tramite il prodotto vettoriale tra i lati che lo compongono
Restituisce il valore dell'area del poligono.
ComputeBoundingBox: forma la minima bounding box rettangolare contenente il poligono. La restituisce tramite i suoi 4 vertici
(Vector2d) in senso antiorario.

Classe ReferenceElement:
Il costruttore riceve il poligono concavo e la sua bounding box, come due liste di Vector2d
NewBoundingBox: questo metodo calcola la lunghezza della base e dell'altezza della bounding box e inoltre crea la nuova bounding
box del poligono concavo, inserendo, oltre ai 4 vertici, anche i punti aggiuntivi che servono a creare la mesh. I punti in questione
vengono calcolati proiettando i vertici che stanno sul bordo della bounding box, anche sul lato opposto.
CreateRefElement: crea il "Reference Element" inserendo prima tutti i punti del poligono concavo inscritto, e poi tutti i punti della
nuova bounding box, ovvero quelli necessari per creare la mesh
Translate: prende in imput una lista di punti, come vettore di Vector2d, e un Vector2d che fornisce il parametro di traslazione. Trasla
tutti i punti ricevuti secondo la direzione suggerita dal parametro, e restituisce la nuova lista di punti traslati
CreateMesh: prende in imput un vettore di Vector2d contenente i 4 vertici del dominio da ricoprire e costruisce la mesh come un vettore
di elementi. Ciascun elemento è un vettore di Vector2d, che contiene per primi i vertici del poligono inscritto, e poi i punti della
nuova bounding box. Per fare questo utilizza i Reference Element e li trasla nel dominio (tramite il metodo "Translate"). Se è il caso,
procede al taglio delle parti in eccesso, tramite la classe "Intersector". In quest'ultimo caso, nella mesh verrà inserito il poligono
tagliato, con la bounding box corrispondente. La mesh è costruita dal basso verso l'alto, partendo dal basso a sinistra e procedendo per
righe. Restituisce la mesh così creata.
AreaTot: calcola l'area totale della mesh, sommando quella dei vari elementi in essa inseriti, tramite il metodo "ComputeArea" della
classe Polygon