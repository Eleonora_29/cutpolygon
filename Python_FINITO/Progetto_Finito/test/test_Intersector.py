from unittest import TestCase
import numpy as np
import src.Intersector as Int


class TestIntersection(TestCase):
    def test_intersection(self):
        matrix_tangent_vector = Int.Intersection()
        try:
            origin_l1 = np.array([[3], [0]])
            end_l1 = np.array([[3.], [5.]])
            self.assertEqual(str(Int.Intersection.set_straight(matrix_tangent_vector, origin_l1, end_l1)), "None")
        except:
            self.fail()

        try:
            origin_l2 = np.array([[1], [1]])
            end_l2 = np.array([[5.], [3.]])
            self.assertEqual(str(Int.Intersection.set_edge(matrix_tangent_vector, origin_l2, end_l2)), "None")
        except:
            self.fail()

        try:
            resultParametricCoordinates, intersection = Int.Intersection.find_intersection(matrix_tangent_vector, origin_l1, origin_l2)
            result = str(resultParametricCoordinates) + "\n " + str(intersection)
            self.assertEqual(result, "[[0.4]\n [0.5]]\n True")
        except:
            self.fail()

        try:
            self.assertEqual(str(Int.Intersection.find_coordinates(matrix_tangent_vector, origin_l1, resultParametricCoordinates)), "[[3.]\n [2.]]")
        except:
            self.fail()
