from unittest import TestCase
import numpy as np
import src.NewPolygons as Pol


class TestPolygon(TestCase):
    def test_rectangle(self):
        try:
            vertices = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
            s1 = np.array([[2], [1.2]])
            s2 = np.array([[4], [3]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            num_intersection = str(num_intersection)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(1.0, 1.0), (1.7778, 1.0), (2.0, 1.2), (4.0, 3.0), (4.1111, 3.1), (1.0, 3.1)], "
                             "[(1.7778, 1.0), (5.0, 1.0), (5.0, 3.1), (4.1111, 3.1), (4.0, 3.0), (2.0, 1.2)])"
                             "\nThe numerical order is:\n([0, 4, 5, 6, 7, 3], [4, 1, 2, 7, 6, 5])")
        except:
            self.fail()

    def test_concave1(self):
        try:
            vertices = np.array([[1.5, 5.6, 5.5, 4., 3.2, 1.], [1., 1.5, 4.8, 6.2, 4.2, 4.]])
            s1 = np.array([[2], [3.7]])
            s2 = np.array([[4.1], [5.9]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            num_intersection = str(num_intersection)
            self.assertEqual(num_intersection, "4")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "[[(1.5, 1.0), (5.6, 1.5), (5.5, 4.8), (4.2043, 6.0093), (4.1, 5.9), (3.7213, 5.5033), "
                             "(3.2, 4.2), (2.4086, 4.1281), (2.0, 3.7), (1.1912, 2.8527)], [(4.2043, 6.0093), "
                             "(4.0, 6.2), (3.7213, 5.5033), (4.1, 5.9)], [(2.4086, 4.1281), (1.0, 4.0), "
                             "(1.1912, 2.8527), (2.0, 3.7)]]"
                             "\nThe numerical order is:\n[[0, 1, 2, 6, 7, 8, 4, 9, 10, 11], "
                             "[6, 3, 8, 7], [9, 5, 11, 10]]")
        except:
            self.fail()
