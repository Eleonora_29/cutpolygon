from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestPentagon(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
            s1 = np.array([[1.4], [2.75]])
            s2 = np.array([[3.6], [2.2]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "2")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(2.5, 1.0), (4.0, 2.1), (3.6, 2.2), (1.4, 2.75), (1.2, 2.8), (1.0, 2.1)], [(4.0, 2.1), "
                             "(3.4, 4.2), (1.6, 4.2), (1.2, 2.8), (1.4, 2.75), (3.6, 2.2)])"
                             "\nThe numerical order is:\n([0, 1, 5, 6, 7, 4],"
                             " [1, 2, 3, 7, 6, 5])")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(2.5, 1.0), (4.0, 2.1), (3.4, 4.2), (1.6, 4.2), (1.0, 2.1), (1.4, 2.75), (3.6, 2.2), "
                             "(1.2, 2.8)]")
        except:
            self.fail()
