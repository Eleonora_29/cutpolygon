from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestConcavePolygon1(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[2., 0., 3., 0., 3., 3., -1., -3., 0., -3.],
                                 [-2., -1., 1., 2., 2., 3., 3., 1., 0., -2.]])
            s1 = np.array([[0.], [-2.]])
            s2 = np.array([[0.], [3.]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "5")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "[[(-3.0, -2.0), (0.0, -2.0), (0.0, -1.0), (0.0, 0.0)], [(0.0, -2.0), (2.0, -2.0), "
                             "(0.0, -1.0)], [(0.0, -1.0), (3.0, 1.0), (0.0, 2.0), (0.0, 0.0)], [(0.0, 2.0), "
                             "(3.0, 2.0), (3.0, 3.0), (0.0, 3.0)], [(0.0, 3.0), (-1.0, 3.0), (-3.0, 1.0), (0.0, 0.0),"
                             " (0.0, 2.0)]]"
                             "\nThe numerical order is:\n[[0, 10, 2, 9], [10, 1, 2], [2, 3, 4, 9], [4, 5, 6, 11], "
                             "[11, 7, 8, 9, 4]]")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(-3.0, -2.0), (2.0, -2.0), (0.0, -1.0), (3.0, 1.0), (0.0, 2.0), (3.0, 2.0), (3.0, 3.0),"
                             " (-1.0, 3.0), (-3.0, 1.0), (0.0, 0.0), (0.0, -2.0), (0.0, 3.0)]")
        except:
            self.fail()
