import numpy as np


class NewPoints:
    def __init__(self, vertices, intersection_points, segment):
        self.vertices = vertices
        self.intersection_points = intersection_points
        self.segment = segment

    def ordered_vertex(self):
        ordered_vertex = np.array([[0.], [0.]], float)
        primoIndex: int = 0
        minX: float = self.vertices[0][0]
        minY: float = self.vertices[1][0]
        flagX: int = 0
        flagY: int = 0
        to_order: bool = False
        temp = np.array([[0.], [0.]], float)
        for v in range(1, np.size(self.vertices, axis=1)):
            if self.vertices[0][v] <= minX:
                flagX = v
            if self.vertices[1][v] <= minY:
                flagY = v
            if flagX != 0 and flagY != 0:
                minX = self.vertices[0][v]
                minY = self.vertices[1][v]
                primoIndex = v
                flagX = 0
                flagY = 0
                to_order = True
            else:
                flagX = 0
                flagY = 0
        if to_order is True:
            for a in range(primoIndex, np.size(self.vertices, axis=1)):
                temp[0][0] = self.vertices[0][a]
                temp[1][0] = self.vertices[1][a]
                ordered_vertex = np.append(ordered_vertex, temp, axis=1)
            for b in range(0, primoIndex):
                temp[0][0] = self.vertices[0][b]
                temp[1][0] = self.vertices[1][b]
                ordered_vertex = np.append(ordered_vertex, temp, axis=1)
        else:
            ordered_vertex = self.vertices
        if np.size(ordered_vertex, axis=1) > np.size(self.vertices, axis=1):
            ordered_vertex = np.transpose(ordered_vertex)
            ordered_vertex = np.delete(ordered_vertex, 0, 0)
            ordered_vertex = np.transpose(ordered_vertex)
        return ordered_vertex

    def new_points(self):
        ordered = NewPoints(self.vertices, self.intersection_points, self.segment)
        self.vertices = NewPoints.ordered_vertex(ordered)
        tol = 1e-7
        intersection_segment = np.array([[0.], [0.]], float)
        num_intersections = np.size(self.intersection_points, axis=1)
        temp = np.array([[0.], [0.]], float)
        for i in range(0, 2):
            temp[0][0] = self.segment[0][i]
            temp[1][0] = self.segment[1][i]
            intersection_segment = np.append(intersection_segment, temp, axis=1)
        intersection_segment = np.transpose(intersection_segment)
        intersection_segment = np.delete(intersection_segment, 0, 0)
        intersection_segment = np.transpose(intersection_segment)

        num_vertex = np.size(self.vertices, axis=1)
        position = 0
        for j in range(0, num_intersections):
            flag = 0
            for i in range(0, 2):
                if (abs(self.intersection_points[0][j] - intersection_segment[0][i]) <= tol) \
                        and (abs(self.intersection_points[1][j] - intersection_segment[1][i]) <= tol):
                    flag += 1
            if flag == 0:
                temp[0][0] = self.intersection_points[0][j]
                temp[1][0] = self.intersection_points[1][j]
                intersection_segment = np.append(intersection_segment, temp, axis=1)
                position += 1

        new_points = np.array([[0.], [0.]], float)
        for i in range(0, num_vertex):
            temp = np.array([[0.], [0.]], float)
            temp[0][0] = self.vertices[0][i]
            temp[1][0] = self.vertices[1][i]
            new_points = np.append(new_points, temp, axis=1)
        new_points = np.transpose(new_points)
        new_points = np.delete(new_points, 0, 0)
        new_points = np.transpose(new_points)

        position = 0
        num_int_seg = np.size(intersection_segment, axis=1)
        for j in range(0, num_int_seg):
            flag = 0
            for i in range(0, num_vertex):
                if (abs(intersection_segment[0][j] - self.vertices[0][i]) <= tol) \
                        and (abs(intersection_segment[1][j] - self.vertices[1][i]) <= tol):
                    flag += 1
            if flag == 0:
                temp[0][0] = intersection_segment[0][j]
                temp[1][0] = intersection_segment[1][j]
                new_points = np.append(new_points, temp, axis=1)
                position += 1

        new_points = np.round(new_points, 4)
        new_points_0 = new_points[0, :]
        new_points_1 = new_points[1, :]
        new_points = list(zip(new_points_0, new_points_1))
        return new_points

    @staticmethod
    def result(new_points):
        result = "The set of vertices, endpoints of the segment and intersection points is:\n" + str(new_points)
        return result
