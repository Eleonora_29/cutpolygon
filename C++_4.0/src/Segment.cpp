#include "Segment.hpp"

namespace PolygonCutLibrary {

double Segment::CoeffAngolare(Vector2d s1, Vector2d s2) const
{
    double coeff;
    coeff = (s2(1)-s1(1))/(s2(0)-s1(0));
    return coeff;
}

}
