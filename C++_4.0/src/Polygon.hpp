#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Intersector.hpp>
#include <Segment.hpp>
#include <Point.hpp>
#include <Eigen>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class Polygon : public IPolygon {

    private:
      vector<Vector2d > _vertex;
      Vector2d _s1, _s2;
      int _numVertex;
      vector<bool> _edgeWithInters;
      int _numIntersections = 0;
      Vector2d _intersectionCoordinates;
      vector<Vector2d> _intersectionPoints;
      vector<vector<Vector2d>> _cuttedPolygons;
      vector<vector<int>> _newPolygonsVertex;
      vector<double> _distances;
      vector<Vector2d> _puntiCorrispondenti;
      const IPoint& _point;

  public:
      void Reset();

      Polygon(int numVertex, vector<Vector2d> vertex, Vector2d& s1, Vector2d& s2, const IPoint& point);

      void Cut();
      int& NumIntersection() {return _numIntersections;}
      vector<Vector2d> IntersectionPoints() const { return _intersectionPoints;}

      void ChooseFunction(); //scelgo se usare la funzione per i poligoni che hanno due intersezioni o l'altra
      void NewPolygonsTwoInt(); //per i convessi e per i concavi con due intersezioni
      void NewPolygonsMoreInt(); //per gli altri concavi
      vector<vector<Vector2d>> CuttedPolygons() {return _cuttedPolygons;}
      vector<vector<int>> NewPolygonsVertex() {return _newPolygonsVertex;}
  };
}

#endif // POLYGON_H
