#ifndef __TEST_RECTANGLE_H
#define __TEST_RECTANGLE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace RectangleTesting {

  TEST (TestPolygonCut, RectangleTest)
  {
      Vector2d v1, v2, v3, v4;
      v1 << 1.0, 1.0;
      v2 << 5.0, 1.0;
      v3 << 5.0, 3.1;
      v4 << 1.0, 3.1;

      Vector2d s1, s2;
      s1 << 2.0, 1.2;
      s2 << 4.0, 3.0;

      //inserisco i vertici in un vettore di punti

      vector<Vector2d> rectangleVertex;
      rectangleVertex.push_back(v1);
      rectangleVertex.push_back(v2);
      rectangleVertex.push_back(v3);
      rectangleVertex.push_back(v4);

      try
      {
         PolygonCutLibrary::Point();
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: Point point;

      try
      {
          PolygonCutLibrary::Polygon(4, rectangleVertex, s1, s2, point);
      }
      catch (const exception& exception)
      {
          FAIL();
      }

      PolygonCutLibrary::Polygon rectangle(4, rectangleVertex, s1, s2, point); //creo un poligono

      try
      {
          rectangle.Cut();
          EXPECT_EQ(2, rectangle.NumIntersection());
      }
      catch (const exception& exception)
      {
          FAIL();
      }

      Vector2d inters1, inters2, a, b;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 1.77777777778, 1.0;
      inters2 << 4.11111111111, 3.1;

      try
      {
        // i punti di intersezione che calcola
        a = rectangle.IntersectionPoints()[0];
        b = rectangle.IntersectionPoints()[1];
        intersectionPoints = rectangle.IntersectionPoints();
        // la loro differenza deve essere minore di una tolleranza
        EXPECT_TRUE((a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE((a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE((b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE((b(1) - inters2(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
            FAIL();
      }

      vector<Vector2d> newPoints;
      try
      {
        point.CalcPoints(rectangleVertex, 4, intersectionPoints, 2, s1, s2);
        EXPECT_EQ(8, point.NumNewPoints()); // 4 vertici + 2 intersezioni + 2 estremi
        newPoints = point.NewPoints();
        // controllo che i primi 4 punti corrispondano ai vertici
        EXPECT_EQ(v1, newPoints[0]);
        EXPECT_EQ(v2, newPoints[1]);
        EXPECT_EQ(v3, newPoints[2]);
        EXPECT_EQ(v4, newPoints[3]);
        //controllo che dopo ci siano le intersezioni
        EXPECT_EQ(newPoints[4], a);
        EXPECT_EQ(newPoints[5], b);
        //gli ultimi due sono gli estremi
        EXPECT_EQ(s1, newPoints[6]);
        EXPECT_EQ(s2, newPoints[7]);
        rectangle.ChooseFunction();
       }
       catch (const exception& exception)
       {
         FAIL();
       }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v4);
      newVertex1.push_back(0);
      newVertex1.push_back(4);
      newVertex1.push_back(5);
      newVertex1.push_back(6);
      newVertex1.push_back(7);
      newVertex1.push_back(3);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v2);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s2);
      cuttedPolygon2.push_back(s1);
      newVertex2.push_back(4);
      newVertex2.push_back(1);
      newVertex2.push_back(2);
      newVertex2.push_back(7);
      newVertex2.push_back(6);
      newVertex2.push_back(5);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);

      try
      {
        EXPECT_EQ(allCuttedPol, rectangle.CuttedPolygons());
        EXPECT_EQ(allNewVertex, rectangle.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_RECTANGLE_H
