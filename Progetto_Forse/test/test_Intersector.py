from unittest import TestCase
import src.Intersector as Int
import numpy as np


class TestIntersector(TestCase):
    def test_intersector(self):
        matrix_tangent_vector = Int.Intersector()
        try:
            origin_l1 = np.array([[3], [0]])
            end_l1 = np.array([[3.], [5.]])
            self.assertEqual(str(Int.Intersector.setstraight(matrix_tangent_vector, origin_l1, end_l1)), "None")
        except:
            self.fail()

        try:
            origin_l2 = np.array([[1], [1]])
            end_l2 = np.array([[5.], [3.]])
            self.assertEqual(str(Int.Intersector.setedge(matrix_tangent_vector, origin_l2, end_l2)), "None")
        except:
            self.fail()

        try:
            resultParametricCoordinates, intersection = Int.Intersector.intersection(matrix_tangent_vector, origin_l1, origin_l2)
            result = str(resultParametricCoordinates) + "\n " + str(intersection)
            self.assertEqual(result, "[[0.4]\n [0.5]]\n True")
        except:
            self.fail()

        try:
            self.assertEqual(str(Int.Intersector.intersectioncoordinates(matrix_tangent_vector, origin_l1, resultParametricCoordinates)), "[[3.]\n [2.]]")
        except:
            self.fail()
'''
Classe di TestIntersecor -> FUNZIONA
1. Test su SetStraight: testa l'esattezza della prima colonna di matrix_tangent_vector
2. Test su SetEdge: testa l'esattezza della seconda colonna di matrix_tangent_vector
2. Test su Intersection: test che l'intersezione venga trovata correttamente e che ritorni le coordinate corrette
3. Test su IntersectionCoordinates: testa l'esattezza delle coordinate di intersezioni
'''