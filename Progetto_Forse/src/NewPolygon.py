import numpy as np
import src.Intersector as Int
class NewPolygon:
    def __init__(self, vertices):
        self.vertices = vertices
        self.edge_with_intersection = []

    def cut(self, s1, s2):
        i: int = 0
        intersection_points = np.array([[0], [0]], float)
        num_intersections = 1
        num_vertex = np.size(self.vertices, axis=1)
        for i in range(0, num_vertex, 1):
            matrix_tangent_vector = Int.Intersector()
            Int.Intersector.setstraight(matrix_tangent_vector, s1, s2)
            l0 = np.zeros((2, 1), float)
            l0[0][0] = self.vertices[0][0]
            l0[1][0] = self.vertices[1][0]
            li = np.zeros((2, 1), float)
            li[0][0] = self.vertices[0][i]
            li[1][0] = self.vertices[1][i]
            if i != (num_vertex - 1):
                li_1 = np.zeros((2, 1), float)
                li_1[0][0] = self.vertices[0][i + 1]
                li_1[1][0] = self.vertices[1][i + 1]
                Int.Intersector.setedge(matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.setedge(matrix_tangent_vector, li, l0)
            parametric_coord, inters = Int.Intersector.intersection(matrix_tangent_vector, s1, li)
            if (inters == True):
                intersectionCoordinates = Int.Intersector.intersectioncoordinates(matrix_tangent_vector, s1,
                                                                                  parametric_coord)
                if (num_intersections == 0 or (
                        intersectionCoordinates[0][0] != intersection_points[0][num_intersections - 1] or
                        intersectionCoordinates[1][0] != intersection_points[1][num_intersections - 1])):
                    self.edge_with_intersection.append(True)
                    num_intersections += 1
                    flag = np.array([[0], [0]], float)
                    flag = intersectionCoordinates
                    intersection_points = np.append(intersection_points, flag, axis=1)
                else:
                    self.edge_with_intersection.append(False)

            else:
                self.edge_with_intersection.append(False)
        intersection_points = np.transpose(intersection_points)
        intersection_points = np.delete(intersection_points, 0, 0)
        intersection_points = np.transpose(intersection_points)
        return self.edge_with_intersection, intersection_points, num_intersections - 1

'''
    def cut(self, s1, s2):
        i: int = 0
        num_intersections = 1
        intersection_points = np.array([[0], [0]], float)
        num_vertex = np.size(self.vertices, axis=1)
        matrix_tangent_vector = Int.Intersector()
        for i in range(0, num_vertex, 1):
            Int.Intersector.setstraight(matrix_tangent_vector, s1, s2)
            l0 = np.zeros((2, 1), float)
            l0[0][0] = self.vertices[0][0]
            l0[1][0] = self.vertices[1][0]
            li = np.zeros((2, 1), float)
            li[0][0] = self.vertices[0][i]
            li[1][0] = self.vertices[1][i]
            if i != (num_vertex - 1):
                li_1 = np.zeros((2, 1), float)
                li_1[0][0] = self.vertices[0][i + 1]
                li_1[1][0] = self.vertices[1][i + 1]
                Int.Intersector.setedge(matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.setedge(matrix_tangent_vector, li, l0)
            parametric_coord, inters = Int.Intersector.intersection(matrix_tangent_vector, s1, li)
            if inters == True:
                intersection_coordinates = Int.Intersector.intersectioncoordinates(matrix_tangent_vector, s1, parametric_coord)
                if (num_intersections == 0 or (
                        intersection_coordinates[0][0] != intersection_points[0][num_intersections - 1] or
                        intersection_coordinates[1][0] != intersection_points[1][num_intersections - 1])):
                    self.edge_with_intersection.append(True)
                    num_intersections += 1
                    flag = np.array([[0], [0]], float)
                    flag = intersection_coordinates
                    intersection_points = np.append(intersection_points, flag, axis=1)
                else:
                    self.edge_with_intersection.append(False)

            else:
                self.edge_with_intersection.append(False)
        intersection_points = np.transpose(intersection_points)
        intersection_points = np.delete(intersection_points, 0, 0)
        intersection_points = np.transpose(intersection_points)
        num_intersections -= 1
        return self.edge_with_intersection, intersection_points, num_intersections

'''
vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
newpol = NewPolygon(vertex)
s1 = np.array([[2], [1.2]])
s2 = np.array([[4], [3]])
edgeWithInters, intersection_point, num_intersection = NewPolygon.cut(newpol, s1, s2)
print("edgeWithInters\n", edgeWithInters)
print("intersection_point\n", intersection_point)
print("num_intersection\n", num_intersection)
