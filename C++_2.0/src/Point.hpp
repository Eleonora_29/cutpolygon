#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <Eigen>
#include <vector>

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Point {
    public:
      Vector2d _point;
      double x, y;

      Point(const Vector2d& point);
      ~Point();

      static double Distance (const Vector2d& A, const Vector2d& B);
      vector<Vector2d> NewPoints (const vector<Vector2d> vertex, const int numVertex,
                                  const vector<Vector2d> intersectionPoints , const int numIntersections);
  };
}

#endif // POINT_H
