#ifndef INTERSECTOR_H
#define INTERSECTOR_H

#include <iostream>
#include <vector>
#include <Eigen>
#include <Point.hpp>
#include "Segment.hpp"

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Intersector {

    //tutta la classe intersector è testata e funziona

    protected:
        Vector2d _origin1;
        Vector2d _origin2;
        double toleranceParallelism;
        double toleranceIntersection;

    public:
        Intersector();
        ~Intersector();

        void SetStraight(const Vector2d& origin1, const Vector2d& end1);
        void SetEdge(const Vector2d& origin2, const Vector2d& end2);
        bool Intersection();
        const Vector2d& IntersectionCoordinates(); //la chiamo solo se c'è intersezione
        const Vector2d& ParametricCoordinates() { return resultParametricCoordinates; } // [s1, s2], non sono ancora i punti di intersezione

        Vector2d resultParametricCoordinates;
        Vector2d rightHandSide;
        Matrix2d matrixTangentVector;
        Vector2d intersectionCoordinates;

  };
}

#endif // INTERSECTOR_H
