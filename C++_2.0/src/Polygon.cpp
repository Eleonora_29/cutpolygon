#include "Polygon.hpp"
#include <math.h>


namespace PolygonCutLibrary {

Polygon::Polygon(int numVertex, vector <Vector2d> vertex, Vector2d& s1, Vector2d& s2)
{
    _numVertex = numVertex;
    _s1 = s1;
    _s2 = s2;
    _vertex=vertex;
}

Polygon::~Polygon()
{

}

void Polygon::Cut()
{
    for (int i=0; i<_numVertex; i++)
    {
        Intersector intersector; //mi serve per calcolare l'intersezione tra ogni lato e il segmento
        if (i!=(_numVertex-1)) //se non sono arrivata all'ultimo vertice, il lato è da i a i+1, mentre l'ultimo lato è da i a 0
            intersector.SetEdge(_vertex[i], _vertex[i+1]);
        else
            intersector.SetEdge(_vertex[i], _vertex[0]);

        intersector.SetStraight(_s1, _s2);

        bool inters = intersector.Intersection();
        if (inters == true) //se c'è intersezione
        {
            intersectionCoordinates = intersector.IntersectionCoordinates(); //calcolo le coordinate del punto di intersezione
            edgeWithInters.push_back(true); //mi segno che quel lato ha un'intersezione
            numIntersections ++;
            intersectionPoints.push_back(intersectionCoordinates); //lo aggiungo alla lista dei punti di intersezione
        }
        else
            edgeWithInters.push_back(false); //segno anche se il lato non ha intersezione
    }

}

void Polygon::newPolygons()
{
    //per ora ho scritto solo come trovare il poligono 1. Se funziona, penso anche agli altri
    vector<Vector2d> pol; //vettore dei vertici del poligono 1

    pol[0]=_vertex[0]; //parto dal vertice 0 e lo inserisco nel poligono

    int i=0; //fa scorrere le intersezioni
    int v=1; //fa scorrere i vertici, parte da 1 perché il primo l'ho già inserito sopra

    for (int k=1; k<(_numVertex + numIntersections); k++) //fa scorrere i punti del nuovo poligono, parte da 1 perché il posto 0 è già occupato
    {
            if (edgeWithInters[k-1]==true) //se c'è intersezione sul lato che sto considerando
            {
                if (intersectionPoints[i] != pol[k-1]) //se l'intersezione non è sul vertice già inserito
                {
                   pol[k] = intersectionPoints[i]; //aggiungo il punto di intersezione
                   k++; //ogni volta che occupo un posto, incremento k
                }
                pol[k] = intersectionPoints[i+1]; //aggiungo anche l'intersezione successiva (per i convessi dovrebbero essere solo 2)
                k++;

                //ora cerco di capire qual è il lato che contiene la seconda intersezione, in modo da inserire il vertice successivo alla seconda intersezione
                for (int j=k; j<_numVertex; j++) // devo partire dal lato successivo a quello in cui c'è la prima intersezione
                {
                    if (edgeWithInters[j] == true) //quando lo trovo aggiungo il vertice successivo
                        pol[k] = _vertex[j+1];
                }
            }
            else //altrimenti aggiungo solo il vertice successivo
            {
                if (_vertex[v] != pol[k-1]) // se il vertice non coincide con l'intersezione precedente (se c'è)
                {
                    pol[k] = _vertex[v];
                    k++;
                }
                v++;
            }
    }
    // problema: k va fino al numero di vertici + intersezioni, però alcuni possono coincidere, quindi bisognerebbe dirgli di fermarsi ad un certo punto
}


}
