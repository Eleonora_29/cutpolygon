#include "Point.hpp"
#include <math.h>

namespace PolygonCutLibrary {

  Point::Point(const Vector2d& point)
  {
      _point = point;
      x = point(0);
      y = point(1);
  }

  Point::~Point()
  {

  }

  double Point::Distance(const Vector2d &A, const Vector2d &B)
  {
      return sqrt(pow((A(0) - B(0)), 2) + pow((A(1) - B(1)), 2));
  }

  vector<Vector2d> Point::NewPoints(const vector<Vector2d> vertex, const int numVertex,
                                     const vector<Vector2d> intersectionPoints, const int numIntersections)
  {
      vector<Vector2d> newPoints;

      for (int i=0; i<numVertex; i++)
        newPoints[i] = vertex[i]; // prima inserisco i vertici del poligono

      int flag=0;

      for (int j=0; j<numIntersections; j++) //scorro i punti di intersezione
      {
          for (int i=0; i<numVertex; i++) //scorro i vertici
          {
              if (intersectionPoints[j] == vertex[i]) //se il punto di intersezione è un vertice
                  flag = 1;
          }
          if (flag == 0)
              newPoints[numVertex + j] = intersectionPoints[j]; //aggiungo il punto a newPoints solo se non è un vertice
          flag = 0;
      }

      return newPoints; //contiene prima i vertici e poi le intersezioni
  }

}
