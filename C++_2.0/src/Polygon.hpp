#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <vector>
#include <Intersector.hpp>
#include <Segment.hpp>
#include <Point.hpp>
#include <Eigen>

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Polygon {
    private:
      vector<Vector2d > _vertex;
      Vector2d _s1, _s2;
      int _numVertex;
      vector<bool> edgeWithInters;
      int numIntersections = 0;
      Vector2d intersectionCoordinates;
      vector<Vector2d> intersectionPoints;
      vector<vector<Vector2d>> cuttedPolygons;

  public:
      Polygon(int numVertex, vector<Vector2d> vertex, Vector2d& s1, Vector2d& s2);
      ~Polygon();

      //questa prima parte è testata e funziona
      void Cut();
      int& NumIntersection() {return numIntersections;}
      vector<Vector2d> IntersectionPoints() {return intersectionPoints;}

      //questa funzione non è ancora testata, potrebbe non funzionare
      void newPolygons();
  };
}

#endif // POLYGON_H
