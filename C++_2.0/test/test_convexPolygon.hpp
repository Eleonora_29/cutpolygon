#ifndef __TEST_CONVEXPOLYGON_H
#define __TEST_CONVEXPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace PolygonCutLibrary;
using namespace testing;
using namespace std;

namespace ConvexPolygonTesting {

  TEST(TestPolygonCut, TestConvexPolygon)
  {
      Vector2d v1, v2, v3, v4, v5;
      v1 << 2.5, 1.0;
      v2 << 4.0, 2.1;
      v3 << 3.4, 4.2;
      v4 << 1.6, 4.2;
      v5 << 1.0, 2.1;

      Vector2d s1, s2;
      s1 << 1.4, 2.75;
      s2 << 3.6, 2.2;


      vector<Vector2d> convexPolygonvVertex;
      convexPolygonvVertex.push_back(v1);
      convexPolygonvVertex.push_back(v2);
      convexPolygonvVertex.push_back(v3);
      convexPolygonvVertex.push_back(v4);
      convexPolygonvVertex.push_back(v5);

      try
      {
        PolygonCutLibrary::Polygon(5,convexPolygonvVertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Polygon convexPolygon(5, convexPolygonvVertex, s1, s2);

      convexPolygon.Cut();
      EXPECT_EQ(3, convexPolygon.NumIntersection());


      // in questo poligono ci sono due intersezioni sui vertici, bisogna fare attenzione a non contare quei punti due volte in newpoints


  }
}

#endif // __TEST_CONVEXPOLYGON_H
