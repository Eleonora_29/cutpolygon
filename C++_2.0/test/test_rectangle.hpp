#ifndef __TEST_RECTANGLE_H
#define __TEST_RECTANGLE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace PolygonCutLibrary;
using namespace testing;
using namespace std;

namespace RectangleTesting {

//questo test non è finito ma funziona, tranne l'ultima riga

  TEST (TestPolygonCut, RectangleTest)
  {
      Vector2d v1, v2, v3, v4;
      v1 << 1.0, 1.0;
      v2 << 5.0, 1.0;
      v3 << 5.0, 3.1;
      v4 << 1.0, 3.1;

      Vector2d s1, s2;
      s1 << 2.0, 1.2;
      s2 << 4.0, 3.0;

      //inserisco i vertici in un vettore di punti

      vector<Vector2d> rectangleVertex;
      rectangleVertex.push_back(v1);
      rectangleVertex.push_back(v2);
      rectangleVertex.push_back(v3);
      rectangleVertex.push_back(v4);

      try
      {
            PolygonCutLibrary::Polygon(4, rectangleVertex, s1, s2); //creo un poligono
      }
      catch (const exception& exception)
      {
            FAIL();
      }

      Polygon rectangle(4, rectangleVertex, s1, s2);

      rectangle.Cut();
      EXPECT_EQ(2, rectangle.NumIntersection());
      Vector2d a,b;
      a << 1.77777777778, 1.0;
      b << 4.11111111111, 3.1;
      vector<Vector2d> inters;
      inters.push_back(a);
      inters.push_back(b);
      EXPECT_EQ(inters, rectangle.IntersectionPoints());


    // bisogna controllare che restituisca in imput tutti i punti
    // bisogna controllare che costruisca nuovi poligoni

  }
}

#endif // __TEST_RECTANGLE_H
