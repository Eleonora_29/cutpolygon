#include "Intersector.hpp"
#include <math.h>

namespace PolygonCutLibrary {

Intersector::Intersector()
{

}

Intersector::~Intersector()
{

}

void Intersector::SetStraight(const Vector2d &origin1, const Vector2d &end1)
{
    _origin1 = origin1;
    matrixTangentVector.col(0) = end1 - origin1; //t1
}

void Intersector::SetEdge(const Vector2d &origin2, const Vector2d &end2)
{
    _origin2 = origin2;
    matrixTangentVector.col(1) = origin2 - end2; //t2
}

const Vector2d& Intersector::ComputeIntersection()
{
    rightHandSide = _origin2 - _origin1;
    Matrix2d solverMatrix;
    bool intersection = false;

    solverMatrix << matrixTangentVector(1,1), -matrixTangentVector(0,1), -matrixTangentVector(1,0), matrixTangentVector(0,0); //inversa di matrixTangentVector
    resultParametricCoordinates = solverMatrix * rightHandSide; // vedi appunti di D'Auria

    if (resultParametricCoordinates(0) < 1 && resultParametricCoordinates(0) > 0 ) //controllo che l'intersezione sia interna al lato
        intersection = true;
    else
        intersection = false;

    intersectionCoordinates = _origin1 + matrixTangentVector.col(0)*resultParametricCoordinates(0); // vedi appunti di D'Auria

    if (intersection == true)
        numIntersections ++;

    intersectionPoints[numIntersections - 1] = intersectionCoordinates; //salvo il punto di intersezione in un vettore di punti

    return intersectionCoordinates;
}



}
