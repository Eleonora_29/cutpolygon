#include "Segment.hpp"

namespace PolygonCutLibrary {

  Segment::Segment(const Vector2d& s1,
                   const Vector2d& s2)
  {
    _s1 = s1;
    _s2 = s2;
  }
  Segment::~Segment()
  { 
  }

}
