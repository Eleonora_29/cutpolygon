#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <vector>
//#include <Point.hpp>
#include <Eigen>

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Polygon {
    public:
      vector<Vector2d *> _vertex;

      Polygon(int numVertex, vector<Vector2d *> vertex);
      ~Polygon();
  };
}

#endif // POLYGON_H
