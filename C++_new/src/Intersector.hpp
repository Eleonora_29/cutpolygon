#ifndef INTERSECTOR_H
#define INTERSECTOR_H

#include <iostream>
#include <vector>
#include <Eigen>
//#include "Point.hpp"

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Intersector {

    protected:
        Vector2d _origin1;
        Vector2d _origin2;

    public:
        Intersector();
        ~Intersector();

        void SetStraight(const Vector2d& origin1, const Vector2d& end1);
        void SetEdge(const Vector2d& origin2, const Vector2d& end2);
        const Vector2d& ComputeIntersection();
        const Vector2d& ParametricCoordinates() { return resultParametricCoordinates; } // [s1, s2], non sono ancora i punti di intersezione
        const double& FirstParametricCoordinate() { return resultParametricCoordinates(0); }
        const double& SecondParametricCoordinate() { return resultParametricCoordinates(1); }
        const vector <Vector2d> IntersectionPoints() {return intersectionPoints;} //lista dei punti di intersezione

        Vector2d resultParametricCoordinates;
        Vector2d rightHandSide;
        Matrix2d matrixTangentVector;
        Vector2d intersectionCoordinates;
        int numIntersections = 0; //per contare le intersezioni che trovo
        vector<Vector2d> intersectionPoints;

  };
}

#endif // INTERSECTOR_H
