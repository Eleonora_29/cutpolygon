#ifndef __TEST_EMPTYCLASS_H
#define __TEST_EMPTYCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace PolygonCutLibrary;
using namespace testing;
using namespace std;

namespace RectangleTesting {

  TEST(TestPolygonCut, TestRectangle)
  {

      Vector2d v1, v2, v3, v4;
      v1 << 1.0, 1.0;
      v2 << 5.0, 1.0;
      v3 << 5.0, 3.1;
      v4 << 1.0, 3.1;

      Vector2d s1, s2;
      s1 << 2.0, 1.2;
      s2 << 4.0, 3.0;

    //inserisco i vertici in un vettore di punti

    vector<Vector2d *> rectangle;
    rectangle[0]= &v1;
    rectangle[1]= &v2;
    rectangle[2]= &v3;
    rectangle[3]= &v4;

    try
    {
          PolygonCutLibrary::Polygon(4,rectangle);
    }
    catch (const exception& exception)
    {
          FAIL();
    }

    //controllo se c'è intersezione con i 4 lati del rettangolo

    Intersector intersector1;
    Intersector intersector2;
    Intersector intersector3;
    Intersector intersector4;

    intersector1.SetEdge(v2, v1);
    intersector1.SetStraight(s1, s2);
    EXPECT_TRUE(intersector1.ComputeIntersection()); //interseca
    EXPECT_FLOAT_EQ(1.78, intersector1.FirstParametricCoordinate()); //è approssimato
    EXPECT_FLOAT_EQ(1.002, intersector1.SecondParametricCoordinate());

    intersector2.SetEdge(v3, v2); //non interseca
    intersector2.SetStraight(s1, s2);
    EXPECT_TRUE(!intersector2.ComputeIntersection());

    intersector3.SetEdge(v4, v3);
    intersector3.SetStraight(s1, s2);
    EXPECT_TRUE(intersector3.ComputeIntersection());
    EXPECT_FLOAT_EQ(4.11, intersector3.FirstParametricCoordinate()); //è approssimato
    EXPECT_FLOAT_EQ(3.1, intersector3.SecondParametricCoordinate());

    intersector4.SetEdge(v1, v4);
    intersector4.SetStraight(s1, s2);
    EXPECT_TRUE(!intersector4.ComputeIntersection());

    vector <Vector2d> intersectionPoints; //metto dentro i due punti di intersezione
    intersectionPoints[0] << 1.78, 1.002;
    intersectionPoints[1] << 4.11, 3.1;

    EXPECT_TRUE()

    vector<Point *> newPoints;
    EXPECT_FLOAT_EQ((1.0, 1.0), Point::NewPoints(rectangle, 4, ));


    // bisogna controllare che restituisca in imput tutti i punti
    // bisogna controllare che costruisca nuovi poligoni

  }
}

#endif // __TEST_RECTANGLE_H
