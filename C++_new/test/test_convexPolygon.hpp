#ifndef __TEST_EMPTYCLASS_H
#define __TEST_EMPTYCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace PolygonCutLibrary;
using namespace testing;
using namespace std;

namespace EmptyTesting {

  TEST(TestPolygonCut, TestConvexPolygon)
  {
      Vector2d v1, v2, v3, v4, v5;
      v1 << 2.5, 1.0;
      v2 << 4.0, 2.1;
      v3 << 3.4, 4.2;
      v4 << 1.6, 4.2;
      v5 << 1.0, 2.1;

      Vector2d s1, s2;
      s1 << 1.4, 2.75;
      s2 << 3.6, 2.2;

      PolygonCutLibrary::Point v1 = PolygonCutLibrary::Point(2.5, 1.0);
      PolygonCutLibrary::Point v2 = PolygonCutLibrary::Point(4.0, 2.1);
      PolygonCutLibrary::Point v3 = PolygonCutLibrary::Point(3.4, 4.2);
      PolygonCutLibrary::Point v4 = PolygonCutLibrary::Point(1.6, 4.2);
      PolygonCutLibrary::Point v5 = PolygonCutLibrary::Point(1.0, 2.1);

      PolygonCutLibrary::Point s1 = PolygonCutLibrary::Point(1.4, 2.75);
      PolygonCutLibrary::Point s2 = PolygonCutLibrary::Point(3.6, 2.2);

      vector<Vector2d *> convexPolygon;
      convexPolygon[0]= &v1;
      convexPolygon[1]= &v2;
      convexPolygon[2]= &v3;
      convexPolygon[3]= &v4;
      convexPolygon[4]= &v5;

    try
    {
        PolygonCutLibrary::Polygon(5,convexPolygon);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

      Intersector intersector1;
      Intersector intersector2;
      Intersector intersector3;
      Intersector intersector4;
      Intersector intersector5;

      intersector1.SetEdge(v1, v2);
      intersector1.SetStraight(s1, -0.25);
      EXPECT_TRUE(intersector1.ComputeIntersection());
      EXPECT_FLOAT_EQ(4.0, intersector1.FirstParametricCoordinate());
      EXPECT_FLOAT_EQ(2.1, intersector1.SecondParametricCoordinate());

      intersector2.SetEdge(v2, v3);
      intersector2.SetStraight(s1, -0.25);
      EXPECT_TRUE(intersector2.ComputeIntersection());
      EXPECT_FLOAT_EQ(4.0, intersector2.FirstParametricCoordinate());
      EXPECT_FLOAT_EQ(2.1, intersector2.SecondParametricCoordinate());

      intersector3.SetEdge(v3, v4);
      intersector3.SetStraight(s1, -0.25);
      EXPECT_TRUE(!intersector3.ComputeIntersection());

      intersector4.SetEdge(v4, v5);
      intersector4.SetStraight(s1, -0.25);
      EXPECT_TRUE(intersector4.ComputeIntersection());
      EXPECT_FLOAT_EQ(1.2, intersector4.FirstParametricCoordinate());
      EXPECT_FLOAT_EQ(2.8, intersector4.SecondParametricCoordinate());

      intersector5.SetEdge(v5, v1);
      intersector5.SetStraight(s1, -0.25);
      EXPECT_TRUE(!intersector5.ComputeIntersection());

      // in questo poligono ci sono due intersezioni sui vertici, bisogna fare attenzione a non contare quei punti due volte in newpoints

      vector<Point *> newPoints;

  }
}

#endif // __TEST_EMPTYCLASS_H
