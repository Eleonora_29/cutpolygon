#ifndef __TEST_NEWPOINTS_H
#define __TEST_NEWPOINTS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace PointTesting {

  TEST(TestPolygonCut, TestNewPoints)
  {
      Vector2d p1, p2;
      p1 << 7.0, 3.0;
      p2 << 9.0, 2.0;

      vector<Vector2d> vertex, orderedVertex, intersPoint;
      Vector2d v1, v2, i, s1, s2;
      v1 << 4.0, 2.0;
      v2 << 1.0, 2.0;
      //disordinati
      vertex.push_back(v1);
      vertex.push_back(v2);
      //ordinati
      orderedVertex.push_back(v2);
      orderedVertex.push_back(v1);

      i << 3.0, 2.0;
      intersPoint.push_back(i);
      s1 << 3.0, 2.0;
      s2 << 3.0, 0.0;

      try
      {
           PolygonCutLibrary::NewPoints(vertex, intersPoint, s1, s2);
      }
      catch (const exception& exception)
      {
           FAIL();
      }

      PolygonCutLibrary::NewPoints newP(vertex, intersPoint, s1, s2);

      vector<Vector2d> newPoints; // quello che mi aspetto
      newPoints.push_back(v2);
      newPoints.push_back(v1);
      newPoints.push_back(i);
      newPoints.push_back(s2);

      try
      {
          EXPECT_EQ(newP.OrderedVertex(), orderedVertex);
          newP.CalcPoints();
          EXPECT_EQ(newP.ReturnPoints(), newPoints);
          EXPECT_EQ(newP.NumNewPoints(), 4);
      }
      catch (const exception& exception)
      {
          FAIL();
      }

  }
}

#endif // __TEST_NEWPOINTS_H
