#include "ReferenceElement.hpp"
#include <math.h>

namespace PolygonCutLibrary {

void ReferenceElement::Reset()
{
    _newBB.clear();
    _refElement.clear();
    _transElement.clear();
    _mesh.clear();
    _baseBB = 0;
    _heightBB = 0;
}

ReferenceElement::ReferenceElement(vector<Vector2d> polygon, vector<Vector2d> boundingBox)
{
    _polygon = polygon;
    _boundingBox = boundingBox;
}

vector<Vector2d> ReferenceElement::newBoundingBox()
{
    Reset();
    Segment base(_boundingBox[0], _boundingBox[1]);
    _baseBB = base.Distance();
    Segment height(_boundingBox[1], _boundingBox[2]);
    _heightBB = height.Distance();

    double minX, minY, maxX, maxY;
    Vector2d p1, p2, p3, p4;
    minX = _boundingBox[0][0];
    minY = _boundingBox[0][1];
    maxX = _boundingBox[2][0];
    maxY = _boundingBox[2][1];
    for (unsigned int i=0; i<_polygon.size(); i++)
    {
        if (_polygon[i][0] == minX)
            p2 << maxX, _polygon[i][1];
        if (_polygon[i][0] == maxX)
            p4 << minX, _polygon[i][1];
        if (_polygon[i][1] == minY)
            p3 << _polygon[i][0], maxY;
        if (_polygon[i][1] == maxY)
            p1 << _polygon[i][0], minY;
    }
    _newBB.push_back(_boundingBox[0]);
    _newBB.push_back(p1);
    _newBB.push_back(_boundingBox[1]);
    _newBB.push_back(p2);
    _newBB.push_back(_boundingBox[2]);
    _newBB.push_back(p3);
    _newBB.push_back(_boundingBox[3]);
    _newBB.push_back(p4);

    return _newBB;
}

vector<Vector2d> ReferenceElement::CreateRefElement()
{
    //lo creo inserendo prima tutti i punti del poligono e poi i punti di newBB
    for (unsigned int i=0; i<_polygon.size(); i++)
        _refElement.push_back(_polygon[i]);
    for (unsigned int j=0; j<_newBB.size(); j++)
        _refElement.push_back(_newBB[j]);
    return _refElement;
}

vector<Vector2d> ReferenceElement::Translate(vector<Vector2d> element, Vector2d translation)
{
    for (unsigned int j=0; j<element.size(); j++)
        _transElement.push_back(element[j]+translation);

    return _transElement;
}

vector<vector<Vector2d> > ReferenceElement::CreateMesh(vector<Vector2d> domain)
{
    double l1, l2;
    Segment segm1(domain[0], domain[1]);
    l1 = segm1.Distance();
    Segment segm2(domain[1], domain[2]);
    l2 = segm2.Distance();

    double filledBase = 0; //spazio occupato sull'orizzontale
    double filledHeight = 0; //verticale
    Vector2d t, s;
    t << _baseBB, 0; // serve per traslare in orizzontale
    s << 0, _heightBB; //trasla in verticale
    int polAggiunti = 0;
    vector<Vector2d> pol, ultimoAggiunto;

    for (int i=0; filledBase < l1; i++)
    {
        if (filledBase <= (l1 - _baseBB)) //vuol dire che ce ne sta ancora uno in orizzontale
        {
            if (polAggiunti == 0)
            {
                _mesh.push_back(_refElement);
                polAggiunti++;
                filledBase = _baseBB;
                ultimoAggiunto = _refElement;
            }
            else
            {
                pol.clear();
                pol = Translate(ultimoAggiunto, t);
                _mesh.push_back(pol);
                ultimoAggiunto.clear();
                ultimoAggiunto = pol;
                polAggiunti++;
                filledBase = filledBase + _baseBB;
            }
        }
        else
        {
            pol.clear();
            Vector2d s2;
            s2 = domain[1] + s;
            CutPolygon element(_refElement, domain[1], s2);
            element.Cut();
            vector<Vector2d> intersectionPoints;
            intersectionPoints = element.IntersectionPoints();
            NewPoints newP(_refElement, intersectionPoints, domain[1], s2);
            newP.CalcPoints();
            element.ChooseFunction();
            pol = element.CuttedPolygons()[0]; //mi interessa solo il primo poligono che si forma (di sinistra)
            _mesh.push_back(pol);
            polAggiunti++;
            filledBase = l1;
        }
    }
    pol.clear();
    ultimoAggiunto.clear();
    filledHeight = _heightBB;
    int riga = 0;
    int n = _mesh.size();
    for (int j=0; filledHeight < l2; j++)
    {
        int z = riga*n;
        if (filledHeight <= (l2 - _heightBB)) //vuol dire che ce ne sta ancora uno in verticale
        {
            for (int k=z; k<(z+n); k++) //traslo in alto tutta la prima striscia
            {
                pol = Translate(_mesh[k], s);
                _mesh.push_back(pol);
            }
            filledHeight = filledHeight + _heightBB;
            riga++;
            pol.clear();
        }
        else
        {
            for (int k=z; k<(z+n); k++)
            {
                pol.clear();
                Vector2d s1, s2;
                s1 << (k-z)*_baseBB, l2;
                s2 << ((k-z)*_baseBB + 1), l2;
                CutPolygon element(_refElement, s1, s2);
                element.Cut();
                vector<Vector2d> intersectionPoints;
                intersectionPoints = element.IntersectionPoints();
                NewPoints newP(_refElement, intersectionPoints, s1, s2);
                newP.CalcPoints();
                element.ChooseFunction();
                pol = element.CuttedPolygons()[0];
                _mesh.push_back(pol);
            }
            filledHeight = l2;
        }
    }
    return _mesh;
}

}
