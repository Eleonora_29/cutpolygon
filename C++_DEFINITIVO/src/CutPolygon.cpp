#include "CutPolygon.hpp"
#include <math.h>

namespace PolygonCutLibrary {

double CutPolygon::tolerance = 1.0E-7;

void CutPolygon::Reset()
{
    _edgeWithInters.clear();
    _numIntersections = 0;
    _intersectionCoordinates << 0,0;
    _intersectionPoints.clear();
    _cuttedPolygons.clear();
    _newPolygonsVertex.clear();
    _distances.clear();
    _puntiCorrispondenti.clear();
    _ordVertex.clear();
}

CutPolygon::CutPolygon(const vector<Vector2d> vertex, Vector2d &s1, Vector2d &s2)
{
    _vertex = vertex;
    _s1 = s1;
    _s2 = s2;
}

void CutPolygon::Cut()
{
    Reset();
    NewPoints newP(_vertex, _intersectionPoints, _s1, _s2);
    int numVertex = _vertex.size();
    _ordVertex = newP.OrderedVertex(); //metto in ordine i vertici, in modo che il primo sia quello in basso a sinistra

    for (int i=0; i<numVertex; i++)
    {
        Intersector intersector; //mi serve per calcolare l'intersezione tra ogni lato e il segmento
        if (i!=(numVertex-1)) //se non sono arrivata all'ultimo vertice, il lato è da i a i+1, mentre l'ultimo lato è da i a 0
            intersector.SetEdge(_ordVertex[i], _ordVertex[i+1]);
        else
            intersector.SetEdge(_ordVertex[i], _ordVertex[0]);

        intersector.SetStraight(_s1, _s2);

        bool inters = intersector.Intersection();
        if (inters == true) //se c'è intersezione
        {
            _intersectionCoordinates = intersector.IntersectionCoordinates(); //calcolo le coordinate del punto di intersezione
            if (_numIntersections==0 || abs(_intersectionCoordinates[0] - _intersectionPoints[_numIntersections-1][0]) >= tolerance || abs(_intersectionCoordinates[1] - _intersectionPoints[_numIntersections-1][1]))
            {
                _edgeWithInters.push_back(true); //mi segno che quel lato ha un'intersezione
                _numIntersections ++;
                _intersectionPoints.push_back(_intersectionCoordinates); //lo aggiungo alla lista dei punti di intersezione
            }
            else
                _edgeWithInters.push_back(false);
        }
        else
            _edgeWithInters.push_back(false); //segno anche se il lato non ha intersezione
    }
}

void CutPolygon::ChooseFunction()
{
    if (_numIntersections == 2)
        CutPolygon::NewPolygonsTwoInt();
    else
        CutPolygon::NewPolygonsMoreInt();
}

void CutPolygon::NewPolygonsTwoInt()
{
    vector<Vector2d> pol1, pol2;
    vector<int> polNumerati1, polNumerati2;
    pol1.push_back(_ordVertex[0]); //parto dal vetice 0
    polNumerati1.push_back(0);
    int i=0; //scorre le intersezioni
    int x=_vertex.size();
    int n=_vertex.size();
    int m; //è un indice, mi serve per il pol2
    int p1=1, p2=0; //tengono conto di dove sono arrivata a riempire pol1 e pol2
    int v2=0; //scorre i vertici del poligono originale per metterli in pol2
    float e,f;

    for (int v1=1; v1<x; v1++) //fa scorrere i vertici del poligono originale per metterli in pol1, parto da 1 perché 0 è occupato
    {
        if(_edgeWithInters[v1-1] == true) //se il lato contiene l'intersezione
        {
            x=v1-1; //quando trovo il lato con l'intersezione non voglio più entrare nel for grande, quindi faccio in modo che l<x
            v2 = v1; //memorizzo qual è il primo vertice dopo l'intersezione, mi servirà per pol2

            if (abs(_intersectionPoints[i][0] - pol1[p1-1][0]) >= tolerance || abs(_intersectionPoints[i][1] - pol1[p1-1][1]) >= tolerance) //se l'intersezione trovata non coincide con il punto già inserito, a meno di una tolleranza
            {
                if (abs(_intersectionPoints[i][0] - _ordVertex[v1][0]) >= tolerance || abs(_intersectionPoints[i][1] - _ordVertex[v1][1]) >= tolerance) //se l'intersezione non coincide con il vertice, a meno di una tolleranza
                {
                    pol1.push_back(_intersectionPoints[i]); //aggiungo il punto di intersezione al pol1
                    pol2.push_back(_intersectionPoints[i]); //metto l'intersezione come primo punto del secondo poligono
                    polNumerati1.push_back(n); //alle intersezioni dò numeri successivi rispetto a quelli dei vertici
                    polNumerati2.push_back(n);
                    n++;
                }
                else //se coincide, aggiungo quel vertice
                {
                    pol1.push_back(_ordVertex[v1]);
                    pol2.push_back(_ordVertex[v1]);
                    polNumerati1.push_back(v1);
                    polNumerati2.push_back(v1);
                    v2++; //v1 non lo incremento perché mi serve che tenga conto del lato con la prima intersezione
                }
                p1++; p2++;
            }
            //calcolo le distanze tra la prima intersezione e i vertici del segmento e memorizzo rispetto a quale punto la sto calcolando in "puntiCorrispondenti"
            Segment segment1(_intersectionPoints[i], _s1);
            e=segment1.Distance();
            _distances.push_back(e);
            _puntiCorrispondenti.push_back(_s1);
            Segment segment2(_intersectionPoints[i], _s2);
            f=segment2.Distance();
            _distances.push_back(f);
            _puntiCorrispondenti.push_back(_s2);

            // calcolo le distanze tra la prima intersezione e le altre e salvo rispetto a quale punto le sto calcolando
            for (int z=i+1; z<_numIntersections; z++)
            {
                Segment segment(_intersectionPoints[i], _intersectionPoints[z]);
                _distances.push_back(segment.Distance());
                _puntiCorrispondenti.push_back(_intersectionPoints[z]);
            }

            //metto in ordine crescente il vettore delle distanze (le distanze partono tutte dal primo punto di intersezione e arrivano agli estremi
            //del segmento e alle altre intersez)
            //ordino anche i punti a cui le distanze si riferiscono
            for(unsigned int z=0; z<_distances.size()-1; z++)
            {
                double min = z;
                for(unsigned int w=z+1; w<_distances.size(); w++)
                {
                    if(_distances[w] < _distances[min])
                        min = w;
                }
            double temp1=_distances[min];
            Vector2d temp2=_puntiCorrispondenti[min];
            _distances[min]=_distances[z];
            _puntiCorrispondenti[min]=_puntiCorrispondenti[z];
            _distances[z]=temp1;
            _puntiCorrispondenti[z]=temp2;
            }

            for (unsigned int a=0; a<_distances.size(); a++)
            {
                if (abs(_puntiCorrispondenti[a][0] - pol1[p1-1][0]) >= tolerance || abs(_puntiCorrispondenti[a][1] - pol1[p1-1][1]) >= tolerance) //controllo che i punti con cui ho calcolato la distanza, non siano già stati inseriti in pol1
                {
                    pol1.push_back(_puntiCorrispondenti[a]); //li inserisco in ordine, dal più vicino al più lontano
                    p1++;
                    polNumerati1.push_back(n); //numero i punti che sto inserendo
                    n++;
                }
            }

            for (unsigned int b=v1; b<_vertex.size(); b++)
            {
                if (_edgeWithInters[b]==true) //cerco in che lato è l'ultimo punto di intersezione
                {
                    for (unsigned int c=b+1; c<_vertex.size(); c++)
                    {
                        pol1.push_back(_ordVertex[c]); //in pol1 inserisco i vertici successivi al lato in cui c'è l'ultima intersezione
                        polNumerati1.push_back(c);
                    }
                    for (unsigned int c=v2; c<b+1; c++)
                    {
                        if (abs(_ordVertex[c][0] - pol2[p2-1][0]) >= tolerance || abs(_ordVertex[c][1] - pol2[p2-1][1]) >= tolerance) //se il vertice non coincide con l'intersezione inserita
                        {
                            pol2.push_back(_ordVertex[c]); //in pol2 inserisco i vertici precedenti al lato in cui c'è l'ultima intersezione
                            polNumerati2.push_back(c);
                        }
                    }
                    pol2.push_back(_intersectionPoints[1]); //inserisco la seconda intersezione
                    m=n-1; //m è l'indice delle intersezioni che parte dal fondo
                    polNumerati2.push_back(m);
                    m--;

                    //inserisco gli estremi del segmento in ordine di distanza inversa dalla prima intersezione
                    if (f<e)
                    {
                        pol2.push_back(_s1);
                        pol2.push_back(_s2);
                    }
                    else
                    {
                        pol2.push_back(_s2);
                        pol2.push_back(_s1);
                    }
                    polNumerati2.push_back(m);
                    polNumerati2.push_back(m-1);
                }
            }
        }

        else //altrimenti aggiungo solo il vertice successivo
        {
            if (abs(_ordVertex[v1][0] - pol1[p1-1][0]) >= tolerance || abs(_ordVertex[v1][1] - pol1[p1-1][1]) >= tolerance) // se il vertice non coincide con l'intersezione precedente (se c'è)
            {
                pol1.push_back(_ordVertex[v1]); //lo aggiungo e vado avanti con il for
                polNumerati1.push_back(v1);
            }
        }

    }
    _cuttedPolygons.push_back(pol1);
    _cuttedPolygons.push_back(pol2);
    _newPolygonsVertex.push_back(polNumerati1); //in realtà mi serve solo questo
    _newPolygonsVertex.push_back(polNumerati2);

}

void CutPolygon::NewPolygonsMoreInt()
{
    //scelgo di numerare le intersezioni dalla prima trovata, e proseguo seguendo la retta

    int j=0; //scorre i poligoni tagliati
    int numVertex = _vertex.size();

    _newPolygonsVertex.clear();
    _cuttedPolygons.clear();

    vector<int> whatEdgeInters;

    for (int a=0; a<numVertex; a++)
    {
        if (_edgeWithInters[a] == true)
            whatEdgeInters.push_back(a); //lo riempio con gli indici dei lati che contengono intersezioni
    }

    //li uso come vettori di appoggio, per riempire cuttedPolygon e newPolygonsVertex
    vector<Vector2d> pol;
    vector<int> polNumerati;

    //inizio a cercare il primo poligono

    pol.push_back(_ordVertex[0]); //parto dal vertice 0 e lo inserisco nel poligono
    polNumerati.push_back(0); //qui salvo gli indici corrispondenti ai vertici

    vector<int> ordineRetta;
    bool vertAppenaAggiunto = false;
    bool v0Destra, vNuovoDestra;
    int i=0; //fa scorrere le intersezioni
    int l=_vertex.size();
    int n=_vertex.size();
    bool intTrovata = false;
    Vector2d h,d;
    d = _s2 - _s1;
    int flag1=0, flag2=0;

    //cerco se il vertice 0 sta a destra o sinistra del segmento
    h = _ordVertex[0] - _s1; //w, per lo 0
    if (d(0)*h(1)-d(1)*h(0) > tolerance)
        v0Destra = false;
    else if (d(0)*h(1)-d(1)*h(0) < -tolerance)
        v0Destra = true;

    for (int v=1; v<l; v++) //fa scorrere i punti del nuovo poligono, parte da 1 perché il posto 0 è già occupato
    {
        if (_edgeWithInters[v-1]==true) //se c'è intersezione sul lato che sto considerando
        {
             l=v-1; //quando trovo il lato con l'intersezione non voglio più entrare nel for grande, quindi faccio in modo che v<l
             if (_intersectionPoints[i][0] - pol[v-1][0] >= tolerance || _intersectionPoints[i][1] - pol[v-1][1] >= tolerance) //se l'intersezione non è sul vertice già inserito
             {
                 pol.push_back(_intersectionPoints[i]); //aggiungo il punto di intersezione
                 polNumerati.push_back(n); //alle intersezioni dò i numeri successivi a quelli dei vertici
                 n++;
             }

             // calcolo le distanze tra la prima intersezione e le altre e salvo rispetto a quale punto le sto calcolando
             if (abs(_s1[0] - _intersectionPoints[i][0]) < tolerance && abs(_s1[1] - _intersectionPoints[i][1]) < tolerance)
                 flag1=1;
             if (abs(_s2[0] - _intersectionPoints[i][0]) < tolerance && abs(_s2[1] - _intersectionPoints[i][1]) < tolerance)
                 flag2=1;
             for (int z=i+1; z<_numIntersections; z++)
             {
                 Segment segm(_intersectionPoints[i], _intersectionPoints[z]);
                 _distances.push_back(segm.Distance());
                 _puntiCorrispondenti.push_back(_intersectionPoints[z]);
                 if (abs(_s1[0] - _intersectionPoints[z][0]) < tolerance && abs(_s1[1] - _intersectionPoints[z][1]) < tolerance)
                     flag1=1;
                 if (abs(_s2[0] - _intersectionPoints[z][0]) < tolerance && abs(_s2[1] - _intersectionPoints[z][1]) < tolerance)
                     flag2=1;
             }
             //calcolo le distanze con s1 e s2 solo se non coincidono con nessuna interesezione (altrimenti le distanze le ho già calcolate)
             if (flag1==0)
             {
                Segment segment1(_intersectionPoints[i], _s1);
                _distances.push_back(segment1.Distance()); //distanza tra la prima intersezione e s1
                _puntiCorrispondenti.push_back(_s1); //salvo s1, così so a cosa si riferisce la distanza
             }
             if (flag2==0)
             {
                Segment segment2(_intersectionPoints[i], _s2);
                _distances.push_back(segment2.Distance()); //distanza tra la prima intersezione e s2
                _puntiCorrispondenti.push_back(_s2); //salvo s2, così so a cosa si riferisce la distanza
             }

             //metto in ordine crescente il vettore delle distanze
             for(unsigned int z=0; z<_distances.size()-1; z++)
             {
                 double min = z;
                 for(unsigned int w=z+1; w<_distances.size(); w++)
                 {
                     if(_distances[w] < _distances[min])
                          min = w;
                 }
                 double temp1=_distances[min];
                 Vector2d temp2=_puntiCorrispondenti[min];
                 _distances[min]=_distances[z];
                 _puntiCorrispondenti[min]=_puntiCorrispondenti[z];
                 _distances[z]=temp1;
                 _puntiCorrispondenti[z]=temp2;
             }

             //salvo gli indici dei punti che trovo in ordine sulla retta, esclusa la prima intersezione
             int e = numVertex +1;
             int flag=0;
             for (unsigned int h=0; h<_distances.size(); h++)
             {
                 for (int f=0; f<numVertex; f++)
                 {
                     if (abs(_puntiCorrispondenti[h][0] - _ordVertex[f][0]) < tolerance && abs(_puntiCorrispondenti[h][1] - _ordVertex[f][1]) < tolerance)
                     {
                         flag=1;
                         ordineRetta.push_back(f);
                     }
                 }
                 if (flag==0)
                 {
                     ordineRetta.push_back(e);
                     e++;
                 }
                 flag=0;
             }

             for (unsigned int z=0; z<ordineRetta.size(); z++)
             {
                 intTrovata=true;
                 flag=0;
                 i++;
                 pol.push_back(_puntiCorrispondenti[z]); //proseguo sulla retta e aggiungo i punti successivi all'intersezione trovata
                 polNumerati.push_back(ordineRetta[z]);
                 if (vertAppenaAggiunto==false) //se non ho appena aggiunto un vertice
                 {
                    for (unsigned int b=0; b<_intersectionPoints.size(); b++)
                    {
                        if (abs(_puntiCorrispondenti[z][0] - _intersectionPoints[b][0]) < tolerance && abs(_puntiCorrispondenti[z][1] - _intersectionPoints[b][1]) < tolerance) //controllo se il punto trovato è un'intersezione
                        {
                           for (int a=1; a<numVertex; a++)
                           {
                               if (abs(_ordVertex[a][0] - _intersectionPoints[b][0]) < tolerance && abs(_ordVertex[a][1] - _intersectionPoints[b][1]) < tolerance) //controllo se è anche un vertice
                               {
                                   //se ho trovato un vertice controllo se il vertice successivo sta dalla stessa parte del segmento del vertice 0, e in quel caso lo aggiungo
                                   flag=1;
                                   Vector2d g;
                                   g= _ordVertex[a+1] - _s1;
                                   if (d(0)*g(1)-d(1)*g(0) > tolerance)
                                       vNuovoDestra = false;
                                   else if (d(0)*g(1)-d(1)*g(0) < -tolerance)
                                       vNuovoDestra = true;
                                   else //se è proprio sulla retta
                                       vNuovoDestra=v0Destra;
                                   if (v0Destra==vNuovoDestra) //se sono dalla stessa parte
                                   {
                                       if (a+1<numVertex)
                                       {
                                           pol.push_back(_ordVertex[a+1]);
                                           polNumerati.push_back(a+1);
                                       }
                                       else
                                           z=ordineRetta.size();
                                   }
                               }
                           }

                           if (flag==0) //se l'intersezione trovata non era un vertice, cerco qual è il primo vertice che sta dalla stessa parte dello 0
                           {
                               Vector2d g;
                               for (int q=v; q<numVertex; q++)
                               {
                                  g= _ordVertex[q] - _s1;
                                  if (d(0)*g(1)-d(1)*g(0) > tolerance)
                                      vNuovoDestra = false;
                                  else if (d(0)*g(1)-d(1)*g(0) < -tolerance)
                                      vNuovoDestra = true;
                                  else //se è proprio sulla retta
                                      vNuovoDestra=v0Destra;
                                  if (v0Destra == vNuovoDestra)
                                  {
                                      pol.push_back(_ordVertex[q]);
                                      polNumerati.push_back(q);
                                      v = q+1;
                                      q=numVertex;
                                      b=_intersectionPoints.size();
                                      vertAppenaAggiunto = true;
                                  }
                               }
                           }
                        }
                    }
                 }
                 else
                    vertAppenaAggiunto=false;
              }
        }
        else //altrimenti aggiungo solo il vertice successivo
        {
              if (abs(_ordVertex[v][0] - pol[v-1][0]) > tolerance || abs(_ordVertex[v][1] - pol[v-1][1]) > tolerance) // se il vertice non coincide con l'intersezione precedente (se c'è)
              {
                   pol.push_back(_ordVertex[v]);
                   polNumerati.push_back(v);
              }
        }
        if (intTrovata == true) //so che il poligono è finito, perché ho trovato la prima intersezione e tutti i punti successivi, e sono uscito dal for
        {
             _cuttedPolygons.push_back(pol);
             _newPolygonsVertex.push_back(polNumerati);
             j++; //faccio scorrere l'indice dei poligoni trovati
             pol.clear();
             polNumerati.clear();
        }
    }

    // da qui cerco gli altri poligoni (il primo l'ho già trovato)
    int secondaInt;
    bool polFinito = false;
    bool vertRetta = false;
    i=0;
    n=numVertex+1;
    int v=0;
    int rangeI=-1; //è il primo punto di intersezione che considero
    int rangeProv;
    int rangeF; // è il secondo punto di intersezione che considero
    Segment segment(_s1, _s2);
    Vector2d direz = segment.Direction();

    for (int y=0; i<_numIntersections; y++)
    {
        pol.push_back(_intersectionPoints[i]); //inizio da un'intersezione
        int z=_puntiCorrispondenti.size();
        for (int a=0; a<z; a++)
        {
           if (abs(_puntiCorrispondenti[a][0] - _intersectionPoints[i][0]) < tolerance && abs(_puntiCorrispondenti[a][1] - _intersectionPoints[i][1]) < tolerance) //se trovo in punti corrispondenti l'intersezione che sto inserendo
           {
                z=a-1; //quando trovo il punto non voglio più entrare in questo for
                polNumerati.push_back(ordineRetta[a]); //così è il numero corrispondente all'intersezione
                rangeI=a; //salvo l'indice della prima intersezione, corrispondente alla sua posizione sulla retta
           }
        }
        if (rangeI==-1) //vuol dire che l'intersezione è la prima (intersezione 0), cioè fuori dal vettore puntiCorrispondenti
        {
            polNumerati.push_back(numVertex);
        }
        v=l+1;
        i++;
        pol.push_back(_ordVertex[v]); //aggiungo il vertice successivo al lato con l'intersezione
        polNumerati.push_back(v);
        v++;
        for (int k=l+1; k<numVertex; k++)
        {
            if (_edgeWithInters[k]==true)
            {
                pol.push_back(_intersectionPoints[i]); //quando trovo un altro lato con intersezione, aggiungo l'intersezione
                z = _puntiCorrispondenti.size();
                for (int a=0; a<z; a++)
                {
                   if (abs(_puntiCorrispondenti[a][0] - _intersectionPoints[i][0]) < tolerance && abs(_puntiCorrispondenti[a][1] - _intersectionPoints[i][1]) < tolerance) //quando trovo in punti corrispondenti l'intersezione che sto inserendo
                   {
                        z=a-1; //quando trovo il punto non voglio più entrare in questo for
                        polNumerati.push_back(ordineRetta[a]); //così è il numero corrispondente all'intersezione
                        rangeF = a;
                   }
                }
                for (int f=0; f<numVertex; f++)
                {
                   if (abs(_ordVertex[f][0] - _intersectionPoints[i][0]) < tolerance && abs(_ordVertex[f][1] - _intersectionPoints[i][1]) < tolerance)
                   {
                       vertRetta=true; // mi segno se l'intersezione che sto aggiungendo è anche un vertice
                   }
                }
                i++;
                int a=z+1; //salva l'indice corrispondente alla posizione dell'intersezione sulla retta
                Vector2d lato = _ordVertex[v] - _ordVertex[v-1];
                //controllo se sto entrando nel poligono, ma non devo essere alla fine del poligono, nè su un'intersezione che è anche vertice
                if (direz(0)*lato(0) + direz(1)*lato(1) > 0 && k+1<numVertex && vertRetta==false)
                {
                    // se sto entrando nel poligono vado avanti sulla retta e cerco il prossimo lato con intersezione
                    for (int x=a+1; x<numVertex; x++)
                    {
                        if (_edgeWithInters[a]==true)
                        {
                            pol.push_back(_intersectionPoints[i]);
                            z=_puntiCorrispondenti.size();
                            for (int b=0; b<z; b++)
                            {
                               if (abs(_puntiCorrispondenti[b][0] - _intersectionPoints[i][0]) < tolerance && abs(_puntiCorrispondenti[b][1] - _intersectionPoints[i][1]) < tolerance) //quando trovo in punti corrispondenti l'intersezione che sto inserendo
                               {
                                    z=b-1; //quando trovo il punto non voglio più entrare in questo for
                                    polNumerati.push_back(ordineRetta[b]); //così è il numero corrispondente all'intersezione
                               }
                            }
                            for (int c=whatEdgeInters[i]+1; c<numVertex; c++)
                            {
                                //cerco se i lati successivi hanno intersezione
                                if (_edgeWithInters[c]==true)
                                {
                                    i++;
                                    pol.push_back(_intersectionPoints[i]); //aggiungo l'intersezione
                                    z = _puntiCorrispondenti.size();
                                    for (int a=0; a<z; a++)
                                    {
                                       if (abs(_puntiCorrispondenti[a][0] - _intersectionPoints[i][0]) < tolerance && abs(_puntiCorrispondenti[a][1] - _intersectionPoints[i][1]) < tolerance)
                                       {
                                            z=a-1;
                                            polNumerati.push_back(ordineRetta[a]); //aggiungo l'indice corrispondente all'intersezione
                                            rangeProv=a;
                                            c = numVertex;
                                       }
                                    }
                                    for (int h=rangeProv+1; h<rangeI; h++) //vado avanti sulla retta per inserire i punti che si trovano nel mezzo tra la prima e l'ultima inters messa
                                    {
                                        pol.push_back(_puntiCorrispondenti[h]);
                                        polNumerati.push_back(ordineRetta[h]);
                                        polFinito=true;
                                        x=numVertex;
                                    }
                                }
                                else // se non c'è intersezione, aggiungo il vertice successivo
                                {
                                    v=c+1;
                                    pol.push_back(_ordVertex[v]);
                                    polNumerati.push_back(v);
                                }
                            }
                        }
                        else // se non c'è intersezione aggiungo il vertice successivo
                        {
                            pol.push_back(_ordVertex[x+1]);
                            polNumerati.push_back(x+1);
                        }
                    }
                }
                else //se non sto entrando nel poligono
                {
                    if (rangeF>rangeI) // se il range finale è maggiore dell'iniziale
                    {
                        for (int f=rangeF-1; f>rangeI; f--) //torno indietro sulla retta e aggiungo tutti i punti fino ad arrivare alla prima intersezione messa
                        {
                            pol.push_back(_puntiCorrispondenti[f]);
                            polNumerati.push_back(ordineRetta[f]);
                            polFinito = true;
                        }
                        if (rangeF-1 == rangeI) // se in mezzo non c'è niente mi segno comunque che il poligono è finito
                            polFinito = true;
                    }
                    else //se il range iniziale è maggiore dell'iniziale procedo al contrario
                    {
                        for (int f=rangeF+1; f<rangeI; f++)
                        {
                            pol.push_back(_puntiCorrispondenti[f]);
                            polNumerati.push_back(ordineRetta[f]);
                            polFinito = true;
                        }
                    }

                }
            }
            else //se non c'è intersezione aggiungo il vertice successivo
            {
                pol.push_back(_ordVertex[v]);
                polNumerati.push_back(v);
                v++;
            }
            if (polFinito==true)
            {
                _cuttedPolygons.push_back(pol); //aggiungo il nuovo poligono alla lista di quelli tagliati
                _newPolygonsVertex.push_back(polNumerati); //aggiungo anche l'elenco degli indici dei vertici
                // svuoto i vettori di appoggio
                pol.clear();
                polNumerati.clear();
                j++;
                vector<int> quanteVolte;
                // mi segno quante volte ciascuna intersezione compare nei poligoni tagliati
                int flag=0;
                for (int a=0; a<_numIntersections; a++)
                {
                   for (unsigned int j=0; j<_cuttedPolygons.size(); j++)
                   {
                       for (unsigned int e=0; e<_cuttedPolygons[j].size(); e++)
                       {
                           if (abs(_cuttedPolygons[j][e][0] - _intersectionPoints[a][0]) < tolerance && abs(_cuttedPolygons[j][e][1] - _intersectionPoints[a][1]) < tolerance)
                           {
                               flag++;
                               e = _cuttedPolygons[j].size();
                           }
                       }
                   }
                   quanteVolte.push_back(flag);
                   flag=0;
                }
                for (int x=0; x<_numIntersections; x++)
                {
                    //cerco la seconda intersezione aggiunta, così so da dove partire per il prossimo poligono
                    if (abs(_intersectionPoints[x][0] - _puntiCorrispondenti[rangeF][0])<tolerance && abs(_intersectionPoints[x][1] - _puntiCorrispondenti[rangeF][1])<tolerance)
                        secondaInt=x;
                }
                // se la seconda intersezione che ho aggiunto nell'ultimo poligono, è stata inserita in meno di due poligoni, parto da lì per il prossimo poligono
                if (quanteVolte[secondaInt] < 2)
                    i=secondaInt;
                else //altrimenti controllo se c'è qualche intersezione che non è stata presa due volte
                {
                    bool finito = true;
                    for (unsigned int h=0; h<quanteVolte.size(); h++)
                    {
                        if (quanteVolte[h]<2) //se un'intersezione non è stata presa due volte
                            finito = false;
                    }
                    if (finito == true) //se tutte sono state prese due volte, ho finito ed esco dal ciclo grande
                        i=_numIntersections;
                    else if (vertRetta==true) //se la seconda intersezione presa era un vertice, parto comunque da lì perché può essere aggiunto in più di due poligoni
                        i=secondaInt;
                    else
                        i=secondaInt+1; // se non rientro negli altri casi vado all'intersezione successiva
                }
                l=whatEdgeInters[i]; //il lato da cui devo partire è quello che contiene l'inters da cui parto per il prossimo poligono
                if (vertRetta == true)
                    l++; //ma se l'intersezione da cui parto è un vertice, vado al lato successivo
                k = numVertex+1; //per uscire dal ciclo delle k
                polFinito=false;
                vertRetta=false;
            }
        }
    }

}
}
