#ifndef REFERENCEELEMENT_H
#define REFERENCEELEMENT_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Segment.hpp>
#include <NewPoints.hpp>
#include <Eigen>
#include <Polygon.hpp>
#include <CutPolygon.hpp>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class ReferenceElement : public IReferenceElement {

  private:
      vector<Vector2d> _polygon, _boundingBox, _newBB, _transElement, _refElement;
      vector<vector<Vector2d>> _mesh, _ref;
      double _baseBB, _heightBB, _areaTot;

      void Reset();

  public:

      ReferenceElement(vector<Vector2d> polygon, vector<Vector2d> boundingBox);

      vector<Vector2d> NewBoundingBox();
      vector<Vector2d> CreateRefElement();
      vector<Vector2d> Translate(vector<Vector2d> element, Vector2d translation);
      vector<vector<Vector2d>> CreateMesh(vector<Vector2d> domain);
      double AreaTot();

  };
}

#endif // REFERENCEELEMENT_H
