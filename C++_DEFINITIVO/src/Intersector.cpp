#include "Intersector.hpp"
#include <math.h>

namespace PolygonCutLibrary {

double Intersector::toleranceIntersection = 1.0E-7;
double Intersector::toleranceParallelism = 1.0E-7;

void Intersector::SetStraight(const Vector2d &origin1, const Vector2d &end1)
{
    _origin1 = origin1;
    _matrixTangentVector.col(0) = end1 - origin1; //t1
}

void Intersector::SetEdge(const Vector2d &origin2, const Vector2d &end2)
{
    _origin2 = origin2;
    _matrixTangentVector.col(1) = origin2 - end2; //t2
}

bool Intersector::Intersection()
{
    _rightHandSide = _origin2 - _origin1;
    double det = _matrixTangentVector.determinant(); //mi serve per calcolare la matrice inversa
    bool intersection = false;

    double check = toleranceParallelism * toleranceParallelism * _matrixTangentVector.col(0).squaredNorm() *  _matrixTangentVector.col(1).squaredNorm();
    if(det * det >= check)
    {
        Matrix2d solverMatrix;
        solverMatrix << _matrixTangentVector(1,1), -_matrixTangentVector(0,1), -_matrixTangentVector(1,0), _matrixTangentVector(0,0); //se la divido per il determinante, è l'inversa di matrixTangentVector
        _resultParametricCoordinates = (solverMatrix * _rightHandSide)/det; // _resultParametricCoord = (matrixTangentVector)^(-1) * _rightHandSide

        if (_resultParametricCoordinates(1)-1.0 < toleranceIntersection  && _resultParametricCoordinates(1) > -toleranceIntersection ) //controllo che l'intersezione sia interna al lato
            intersection = true;
        else
            intersection = false;
    }
    return intersection;
}

const Vector2d& Intersector::IntersectionCoordinates()
{
    _intersectionCoordinates = _origin1 + _matrixTangentVector.col(0)*_resultParametricCoordinates(0); // P = x01 + t1*s1

    return _intersectionCoordinates;
}

}
