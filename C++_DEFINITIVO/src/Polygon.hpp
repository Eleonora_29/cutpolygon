#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Eigen>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

 class Polygon: IPolygon {

  private:
      vector<Vector2d> _vertex;
      vector<Vector2d> _boundingBox;
      double _area;

      void Reset();

 public:

      Polygon(vector<Vector2d> vertex);

      double ComputeArea();
      vector<Vector2d> ComputeBoundingBox();

  };
}

#endif // POLYGON_H
