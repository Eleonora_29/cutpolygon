#ifndef __TEST_REFELEMENT_H
#define __TEST_REFELEMENT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"
#include "ReferenceElement.hpp"

using namespace testing;
using namespace std;

namespace PolygonTesting {

  TEST (TestPolygonCut, RefElementTest)
  {
      Vector2d v1, v2, v3, v4, v5, v6;
      v1 << 1.5, 1.0;
      v2 << 5.6, 1.5;
      v3 << 5.5, 4.8;
      v4 << 4.0, 6.2;
      v5 << 3.2, 4.2;
      v6 << 1.0, 4.0;

      vector<Vector2d> polygonVertex;
      polygonVertex.push_back(v1);
      polygonVertex.push_back(v2);
      polygonVertex.push_back(v3);
      polygonVertex.push_back(v4);
      polygonVertex.push_back(v5);
      polygonVertex.push_back(v6);

      Vector2d p1, p2, p3, p4;
      p1 << 1, 1;
      p2 << 5.6, 1;
      p3 << 5.6, 6.2;
      p4 << 1, 6.2;
      vector<Vector2d> boundingBox, newBB, refElement;
      boundingBox.push_back(p1);
      boundingBox.push_back(p2);
      boundingBox.push_back(p3);
      boundingBox.push_back(p4);
      Vector2d b1, b2, b3, b4;
      b1 << 4.0, 1.0;
      b2 << 5.6, 4.0;
      b3 << 1.5, 6.2;
      b4 << 1.0, 1.5;
      newBB.push_back(p1);
      newBB.push_back(b1);
      newBB.push_back(p2);
      newBB.push_back(b2);
      newBB.push_back(p3);
      newBB.push_back(b3);
      newBB.push_back(p4);
      newBB.push_back(b4);

      try
      {
         PolygonCutLibrary::ReferenceElement element(polygonVertex, boundingBox);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary::ReferenceElement element(polygonVertex, boundingBox);

      refElement = polygonVertex;
      refElement.insert( refElement.end(), newBB.begin(), newBB.end() );

      try
      {
         EXPECT_EQ(element.NewBoundingBox(), newBB);
         EXPECT_EQ(element.CreateRefElement(), refElement);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14;
      t1 << 1.5, 3.0;
      t2 << 5.6, 3.5;
      t3 << 5.5, 6.8;
      t4 << 4.0, 8.2;
      t5 << 3.2, 6.2;
      t6 << 1.0, 6.0;
      t7 << 1, 3;
      t8 << 4, 3;
      t9 << 5.6, 3;
      t10 << 5.6, 6;
      t11 << 5.6, 8.2;
      t12 << 1.5, 8.2;
      t13 << 1, 8.2;
      t14 << 1.0, 3.5;

      vector<Vector2d> shifted;
      shifted.push_back(t1);
      shifted.push_back(t2);
      shifted.push_back(t3);
      shifted.push_back(t4);
      shifted.push_back(t5);
      shifted.push_back(t6);
      shifted.push_back(t7);
      shifted.push_back(t8);
      shifted.push_back(t9);
      shifted.push_back(t10);
      shifted.push_back(t11);
      shifted.push_back(t12);
      shifted.push_back(t13);
      shifted.push_back(t14);

      Vector2d translation;
      translation << 0, 2;

      try
      {
          EXPECT_EQ(element.Translate(refElement, translation), shifted);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> domain;
      Vector2d d1, d2, d3, d4;
      d1<< 1, 1;
      d2<< 14, 1;
      d3<< 14, 13;
      d4<< 1, 13;
      domain.push_back(d1);
      domain.push_back(d2);
      domain.push_back(d3);
      domain.push_back(d4);
      try
      {
        element.CreateMesh(domain);
        EXPECT_TRUE(abs(element.AreaTot() - 156) < 1e-6);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_REFELEMENT_H
