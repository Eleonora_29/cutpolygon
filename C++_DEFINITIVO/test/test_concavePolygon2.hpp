#ifndef __TEST_CONCAVEPOLYGON2_H
#define __TEST_CONCAVEPOLYGON2_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace ConcavePolygon2Testing {

  TEST(TestPolygonCut, TestConcavePolygon2)
  {
      Vector2d v1, v2, v3, v4, v5, v6;
      v1 << 3.5, 1.0;
      v2 << 6.7, 4.2;
      v3 << 5.3, 6.8;
      v4 << 3.7, 6.0;
      v5 << 2.2, 8.5;
      v6 << 1.0, 5.6;

      Vector2d s1, s2;
      s1 << 2.0, 6.5;
      s2 << 5.1, 3.9;

      vector<Vector2d> concavePolygon2Vertex;
      concavePolygon2Vertex.push_back(v1);
      concavePolygon2Vertex.push_back(v2);
      concavePolygon2Vertex.push_back(v3);
      concavePolygon2Vertex.push_back(v4);
      concavePolygon2Vertex.push_back(v5);
      concavePolygon2Vertex.push_back(v6);

      try
      {
        PolygonCutLibrary::CutPolygon(concavePolygon2Vertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::CutPolygon concavePolygon2(concavePolygon2Vertex, s1, s2);

      try
      {
        concavePolygon2.Cut();
        EXPECT_EQ(2, concavePolygon2.NumIntersections());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters1, inters2, a, b;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 5.8070175, 3.3070175;
      inters2 << 1.5341040, 6.8907514;

      try
      {
        // i punti di intersezione che calcola
        a = concavePolygon2.IntersectionPoints()[0];
        b = concavePolygon2.IntersectionPoints()[1];
        intersectionPoints = concavePolygon2.IntersectionPoints();
        //controllo che coincidano a meno di una tolleranza
        EXPECT_TRUE(abs(a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE(abs(a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE(abs(b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE(abs(b(1) - inters2(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
         PolygonCutLibrary::NewPoints(concavePolygon2Vertex, intersectionPoints, s1, s2);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: NewPoints newP(concavePolygon2Vertex, intersectionPoints, s1, s2);

      vector<Vector2d> newPoints;
      try
      {
        newP.CalcPoints();
        EXPECT_EQ(10, newP.NumNewPoints());
        newPoints = newP.ReturnPoints();
        // controllo che i primi 6 punti corrispondano ai vertici
        EXPECT_EQ(v1, newPoints[0]);
        EXPECT_EQ(v2, newPoints[1]);
        EXPECT_EQ(v3, newPoints[2]);
        EXPECT_EQ(v4, newPoints[3]);
        EXPECT_EQ(v5, newPoints[4]);
        EXPECT_EQ(v6, newPoints[5]);
        // controllo che 2 siano le intersezioni
        EXPECT_EQ(newPoints[6], a);
        EXPECT_EQ(newPoints[7], b);
        // controllo che gli ultimi due siano gli estremi del segmento
        EXPECT_EQ(newPoints[8], s1);
        EXPECT_EQ(newPoints[9], s2);
        concavePolygon2.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v6);
      newVertex1.push_back(0);
      newVertex1.push_back(6);
      newVertex1.push_back(7);
      newVertex1.push_back(8);
      newVertex1.push_back(9);
      newVertex1.push_back(5);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v2);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(v4);
      cuttedPolygon2.push_back(v5);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s1);
      cuttedPolygon2.push_back(s2);
      newVertex2.push_back(6);
      newVertex2.push_back(1);
      newVertex2.push_back(2);
      newVertex2.push_back(3);
      newVertex2.push_back(4);
      newVertex2.push_back(9);
      newVertex2.push_back(8);
      newVertex2.push_back(7);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);

      try
      {
      EXPECT_EQ(allCuttedPol, concavePolygon2.CuttedPolygons());
      EXPECT_EQ(allNewVertex, concavePolygon2.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_CONCAVEPOLYGON2_H
