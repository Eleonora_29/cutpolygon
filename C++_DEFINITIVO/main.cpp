#include "test_concavePolygon5.hpp"
#include "test_concavePolygon4.hpp"
#include "test_newPoints.hpp"
#include "test_segment.hpp"
#include "test_intersector.hpp"
#include "test_cutPolygon.hpp"
#include "test_rectangle.hpp"
#include "test_pentagon.hpp"
#include "test_convexPolygon.hpp"
#include "test_concavePolygon1.hpp"
#include "test_concavePolygon2.hpp"
#include "test_concavePolygon3.hpp"

#include "test_polygon.hpp"
#include "test_refElement.hpp"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
