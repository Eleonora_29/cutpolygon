\documentclass[a4paper, 10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{titling}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{blindtext}

\title{\textbf{\centering{\Huge{DOCUMENTAZIONE PROGETTO}}}}
\author{Cafasso Eleonora s268671 \\  Massaro Camilla s269065 \\ Roviera Elisabetta s274715}
\date{Anno Accademico 2020-2021}

\begin{document}

\begin{titlingpage}
\maketitle
\begin{abstract}
È richiesta l'implementazione di un programma nei linguaggi ad oggetti C++ e Python che svolga le seguenti operazioni:
\begin{itemize}
    \item Ricevere in input i vertici di un poligono e gli estremi di un segmento di taglio;
    \item Restituire in output il set di punti formato dai vertici del poligono, dagli estremi del segmento e dai punti di intersezione;
    \item Restituire in output il set di poligoni venutisi a creare dopo il taglio del poligono;
    \item Ricoprire un dominio rettangolare tramite delle $piastrelle$ contenenti un poligono concavo racchiuso da una bounding box.
\end{itemize}
\end{abstract}
\end{titlingpage}

\tableofcontents
\newpage


\part{Analisi del Progetto}
In questa prima parte si descrive il problema da affrontare mettendo in luce tutte le richieste da soddisfare. Viene inoltre illustrato il procedimento di studio e di analisi preliminare, svolto per strutturare il progetto.
\section{Descrizione del problema}
In questa sezione sono presentati i requirements del progetto. \par È richiesta l'implementazione di un programma chiamato CutPolygon. Questo deve avere i seguenti requisiti.
\begin{enumerate}
    \item Prendere in input:
        \begin{itemize}
            \item Una collezione di punti 2D nello spazio;
            \item Un poligono formato da una collezione di indici ordinati in senso antiorario;
            \item Due punti 2D rappresentanti il segmento di taglio.
        \end{itemize}
    \item Restituire in output:
        \begin{itemize}
            \item La collezione dei punti dei vertici del poligono concatenati con gli estremi del segmento e i punti di intersezione;
            \item Il risultato degli N poligoni venutisi a creare dal processo di taglio, come una collezione di vertici numerati e ordinati in senso antiorario.
            \item Il risultato della formazione di una mesh di dominio rettangolare. 
        \end{itemize}
    \item Inoltre il programma deve contenere:
        \begin{itemize}
            \item L'uso della Programmazione Orientata agli Oggetti;
            \item Un Design Pattern;
            \item Un test specifico per ciascun metodo e ciascuna classe.
        \end{itemize}
\end{enumerate}

\section{Approccio risolutivo}
In questa sezione è presentato il processo di decisioni strutturali preliminari affrontato.
\begin{enumerate}
    \item Visualizzazione del problema.\par
        Dal punto di vista visivo, il problema è stato affrontato utilizzando i software Matlab e Geogebra. Si è così ottenuta una visione più chiara degli aspetti geometrici.
    \newpage
    \item Strutture dati.
        \begin{itemize}
            \item Punti.\par
                \underline{C++}. Non è implementata una specifica classe Vector2D. Si è preferito utilizzare le funzionalità della libreria Eigen per poter sfruttare a pieno le operazione consentite tra questi oggetti. Questo è risultato fondamentale soprattutto nella classe Intersector. La prima componente è l'ascissa, mentre la seconda l'ordinata. Si è preferito non implementare una classe apposita "Vector2D", ma, in maniera computazionalmente più agile, si accede alle diverse componenti in questo modo:
                    \[ Vector2d[0] = ascissa.\]
                    \[Vector2d[1] = ordinata.\]
                \underline{Python}. Si è scelto di implementare una classe Vector2D con attibuti le componenti di un punto. Successivamente, per sfruttare la libreria Numpy, si sono convertici i Vector2D in $numpy.array([[0.], [0.], float)$. Si è quindi definito un array formato da due righe e una colonna. Per accedere agli elementi si utilizza:
                    \[ Vector2d[0][0] = ascissa.\]
                    \[Vector2d[1][0] = ordinata.\]
            \item Poligoni.\par
                Si è deciso di considerare i Poligoni come una lista di Vector2d in modo da accedervi in maniera più agile, e da poter sfruttare tutte le operazioni di calcolo della libreria Eigen e Numpy.\par
                Nello specifico in C++ si parla di un vector di Vector2D, metre in Python di una list di Vector2D, successivamente convertito in un $numpy.ndarray$ in modo tale che sulla prima riga siano contenute le ascisse, mentre sulla seconda tutte le ordinate.
        \end{itemize}            
    \item Design Pattern.\par
        \underline{C++}. Si è deciso di implementare la Dependency Injection. Le logiche sono, infatti, totalmente separate, dunque non sono permesse creazioni collegate. Inoltre, vengono usati polimorfismo, ereditarietà e astrazione, e il programma risulta essere facilmente testabile e configurabile.\par
        \underline{Python}. Si è deciso di affrontare il problema sotto un altro punto di vista, per sfruttare a pieno le potenzialità dei diversi linguaggi di programmazione. È quindi implementato il Factory Method. Con questo Design Pattern si ha la separazione della logica e la facilità di estensione del progetto. Sfrutta l'ereditarietà e il polimorfismo, inoltre risulta essere facilmente testabile.
    \item Librerie utilizzate.\par
        Vista l'entità del progetto è stato necessario utilizzare delle librerie standard del C++ e del Python. In entrambi i casi abbiamo utilizzato math. Mentre, per la parte geometrica del progetto, sono stati utilizzati la libreria Eigen per il C++ e Numpy per il Python.
    \item Test.\par
        Per quanto riguarda i test, si sono utilizzati i google test per il C++ e la modalità di test standard offerta dal Python.
\end{enumerate}
\newpage


\part{UML}
\section{Diagramma UML}
In questa seconda parte si presenta il diagramma UML del progetto in entrambi i linguaggi. Inoltre si descrivono le classi con i rispettivi metodi.\par

\subsection{UML C++}
In questa sezione è presentato l'UML del programma in C++ e una dettagliata descrizione di tutte le classi e di tutti i metodi implementati.\par

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.25]{uml_pt2.png}
    \caption{C++}\par
    \label{fig:my_label}
\end{figure}

Gli attributi principali sono:
\begin{itemize}
    \item Vertices. Vector di Vector2D contente i vertici;
    \item Ord Vertex. Vector di Vector2D conentete i vertici ordinati;
    \item S1. Vector2D che rappresenta un estremo del segmento di taglio;
    \item S2. Vector2D che rappresenta un estremo del segmento di taglio;
    \item Intersection Points. Vector di Vector2D contenente i punti di intersezione;
    \item Intersection Coordinates. Vector2D contenete le coordinate del punto di intersezione;
    \item Edge With Intersection. Vector di bool conentente se un lato viene intersecato oppure no;
    \item Matrix Tangent Vector. Matrix2D riempita dai vettori di direzioni tangenti;
    \item Origin 1. Vector2D usato per settare la retta di intersezione;
    \item Origin 2. Vector2D usato per settare il lato;
    \item Result Parametric Coordinates. Vector2D per salvare le coordinate parametriche dell'intersezione;
    \item Right Hand Side. Vector2D per contentere la differenza tra Origin 1 e Origin 2;
    \item Cutted Polygon. Vector di Vector di Vector2D contenente le coordinate dei nuovi poligoni;
    \item New Polygons Vertex. Vector di Vector int contenente i vertici numerati dei nuovi poligoni;
    \item Distances. Vector di double contente le distanze sulla retta di taglio;
    \item Punti Corrispondente. Vector di Vector2D conentente i punti sulla retta di taglio successivi alla prima intersezione;
    \item New Points. Vector di Vector2D conentente i nuovi punti;
    \item Bounding Box. Vector di Vector2D che crea il $contenitore$ attorno al poligono;
    \item Area. Double contenente l'area;
    \item Polygon. Vector di Vector2D;
    \item New BB. Vector di Vector2D;
    \item Trans Element. Vector di Vector2D;
    \item Ref Element. Vector di Vector2D;
    \item Mesh. Vector di Vector di Vector2D;
    \item Ref. Vector di Vector di Vector2D;
    \item BaseBB. Double;
    \item HeigatBB. Double;
    \item Area Tot. DOuble.
\end{itemize}

Le classi implementate sono:
\begin{enumerate}
    \item New Points.\par
        Il costruttore riceve in input la lista di vertici, i punti di intersezione e gli estremi del segmento. Questa classe presenta quattro metodi.
        \begin{itemize}
            \item Ordered Vertx. Mette in ordine i vertici ricevuti dal costruttore in modo che la numerazione parta dal vertice in basso a sinistra e proceda in senso antiorario. Salva in prima posizione il vertice che presenta l'ascissa e l'ordinata minore, successivamente scorre i vertici dati in ordine antiorario. Quindi restituisce il vettore di vertici nella giusta sequenza;
            \item Calc Points. Chiama il metodo Ordered Vertex per mettere nella giusta sequenza il vettore di vertici. Riempie il vettore dei nuovi punti con i vertici del poligono, punti di intersezione e estremi del segmento. Verifica che nessun punto coincida con un altro a meno di una tolleranza e che quindi non sia già stato inserito in precedenza.
            La tolleranza presa in considerazione è: $tol = 10^{-7}$;
            \item Return Points. Restituisce il vettore di nuovi punti calcolato dal metodo Calc Points;
            \item Num New Points. Restituiscce il numero dei nuovi punti trovati.
        \end{itemize}
    \item Segment.\par
        Il costruttore riceve in input i due estremi del segmento e li inizializza come variabili private. Questa classe ha tre metodi.
        \begin{itemize}
            \item Distance. Calcola la distanza tra i due estremi del segmento creato. Restituisce il valore della distanza tra di essi.
            \item Direction. Restituisce un array che dà la direzione del segmento.
            \item To Matrix. Solamente in Python è stato implementato questo metodo che restiuisce gli estremi del segmento concatenati in un unico array di array.
        \end{itemize}
    \item Intersector.\par
        Il costruttore è vuoto. La classe è usata per intersecare un segmento con una retta. Presenta quattro metodi.
        \begin{itemize}
            \item Set Straight. Prende in input due punti della retta, i vertici del segmento, e mette la loro dirrenza nella prima colonna di\par $matrix\_tangent\_vector$. Nella prima riga c'è la differenza delle ascisse, mentre nella seconda quella delle ordinate;
            \item Set Edge. Prende in input due estremi del segmento, un lato del poligono, e mette la loro differenza nella seconda colonna di \par $matrix\_tangent\_vector$;
            \item Intersection. Calcola $right_hand_side$ facendo la differenza delle due origini dei segmenti. Inoltre calcola le coordinate parametriche dell'eventuale intersezione tramite il prodotto scalare di\par $matrix\_tangent\_vector$ e $right_hand_side$. Se il risultato ottenuto è compreso tra 0 e 1 a meno della tolleranza $tol = 10^{-7}$, l'intersezione è presente e il metodo restiuisce True, altrimenti False;
            \item Parametric Coordinates. Ritorna le coordinate parametriche;
            \item Intersection Coordinate. Calcola le coordinate di intersezione sommando il primo punto della retta al prodotto tra la prima colonna di $matrix\_tangent\_vector$  e la prima coordinata parametrica. Restituisce le coordinate trovate in un array.
        \end{itemize}
    \item Cut Polygon.\par
        Il costruttore riceve i vertici e gli estremi del segmento di taglio e li inizializza come variabili private. Questa classe presenta cinque metodi.
        \begin{itemize}
            \item Cut. Chiama il metodo Ordered Vertex per mettere nella giusta sequenza i vertici. Per ogni lato del poligono chiama un'istanza della classe Intersector così da calcolare le intersezioni tra il prolungamento del segmento di tagli e ciascun lato, dove presenti. Le intersezioni vengono salvate in ordine per come vengno trovate, quindi in senso antiorario, in $intersection_points$. Inoltre la variabile $edge_with_inters$ memorizza se ciascun lato contiene o meno un'intersezione, mentre $num_intersection$ contiene il numero di intersezioni trovate;
            \item Num Intersections. Ritorna il numero di intersezioni;
            \item Intersection Points. Ritorna i punti di intersezione.
            \item Choose Function. Se il poligono preso in considerazione ha due intersezioni, chiama il metodo $New Polygons Two Int$, altrimenti \par $New Polygon More Int$;
            \item New Polygon Two Int. Dallo studio geometrico del problema si intuisce che se un poligono presenta solamente due intersezioni, dal tagli questo sarà diviso in due meta. La struttura di questo metodo risulta essere complicata, quindi verrà illustrata tramite un elenco di azioni che il meotodo compie: \par
                \begin{itemize}
                    \item Inserisce nel primo poligono il vertice $v_0$;
                    \item Cerca il primo lato con intersezione. Finchè non lo trova aggiunge i vertici successivi al pirmo, controllando che non coincidano con le intersezioni già aggiunte;
                    \item Quando trova la prima intersezione cerca se conicide con un vertice. Se non coincide la aggiunge al primo poligono e anche come primo punto del secondo poligono;
                    \item Calcola le distanze tra la prima intersezione e le altre, e tra la prima intersezione e gli estremi del segmento, controllando che alcuni di questi punti non coincidano tra di loro;
                    \item Mette in ordine crescente il vettore delle distanze, salvando anche in ordine i punti rispetto ai quali le distanze sono state calcolate;
                    \item Se i punti rispetto ai quali sono state calcolate le distanze non sono già presenti nel primo poligono, li inserisce dal più vicino al più lontano rispetto alla prima intersezione trovata e li numera in successione;
                    \item Cerca in che lato è l'ultimo punto di intersezione e inserisce nel primo poligono i vertici successivi a quel lato;
                    \item Nel secondo poligono inserisce i vertcii precedenti al lato in c'è l'ultima interseione e poi inserisce la seconda intersezione;
                    \item Aggiunge in esso gli estremi del segmento in ordine di distanza inverso rispetto alla prima intersezione;
                    \item Concatena i due poligoni trovati in $cutted\_polygons$ e\par $new\_polygons\_vertex$, inserendo nel primo le coordinate di ciascun punto, mentre nel seondo gli indici rappresentativi dei punti costituenti i poligoni.
                \end{itemize}
        \item New Polyogon More Int. In questo caso non sappiamo a priori il numero di poligoni che si formeranno, per questa ragione creiamo il primo e poi riproponiamo iterativamente il successivo codice fino ad ottenere l'elenco completo di tutti i poligoni. Descrizione dettagliata del metodo.
            \begin{itemize}
                \item Si aggiunge il vertice $v_0$ al primo poligono segnando se è a destra o a sinistra del segmento;
                \item Si procede ad aggiungere vertici fino a quando non si incontra una intersezione;
                \item Si aggiunge l'intersezione e si procede sulla retta fino a quando non si trova l'intersezione successiva. Tutto quello che è contenuto tra la prima intersezione e quella successiva viene automaticamente aggiunta;
                \item Se la seconda intersezione  trovata non è un vertice, cerco il vertice che stia dalla stessa parte del segmento del punto del vertice $v_0$ e lo aggiungo;
                \item Si procede sulla retta fino all'intersezione successiva controllando nuovamente se è presente un nuovo vertice da aggiungere come fatto in precedenza;
                \item Si procede ripetutamente fino a quando non è stata analizzata tutta la retta;
                \item Se il programma si ferma su di un vertice allora controlla il vertice successiva e nel caso in cui sia dalla stessa parte del $v_0$ allora lo aggiunge e anche quelli successivi;
                \item Altrimenti si continua ad analizzare la retta e si ripete il check sulla retta;
                \item Per trovare i poligoni successivi si parte dalla prima intersezione precedentemente trovata;
                \item Si procede con l'aggiunta di vertici successivi fino a quando non si incontra una nuova intersezione;
                \item Si controlla se si tratta di un vertice e se è un'intersezione di entrata o di uscita dal poligono;
                \item Se sto uscendo dal poligono si torna indietro sulla retta fino alla prima intersezione messa;
                \item Se sto entrando nel poligono, si procede avanti sulla retta di un passo e si aggiunge il punto successivamente trovato;
                \item Quindi si cerca il prossimo lato con intersezione aggiugendo fino a quel punto vertici;
                \item Si procede sulla retta dal'ultima intersezione trovata fino alla prima aggiunta in questo poligono, facendo il check di aggiungere anche tutto quello presente nel mezzo della retta;
                \item Si controlla $Quante Volte$ ogni intersezione compare nei poligoni già tagliati. Se la seconda intersezione aggiunta nell'ultimo poligono tagliato è stata trovata almeno due volte, allora questo sarà il punt di inizio del prossimo poligono;
                \item Altrimenti si controlla se tutte le intersezioni sono state prese almeno due volte. Se si verifica questa condizione tutti i poligono sono stati tagliati correttamente;
                \item Se invece la seconda intersezione presa era un vertice, il programma riparte da quel punto perchè può essere aggiunto in più di due poligoni;
                \item Altrimenti controllo le intersezioni successive. Se tutte le intersezioni sono state inserite con successo, il taglio dei poligoni è completato.
            \end{itemize}
        
        \item Cutted Polygons. Restiuisce $cutted\_polygons$, ovvero i nuovi poligoni tramite le coordinate dei loro punti;
        \item New Polygon Vertex. Restituisce $new\_polygons\_vertex$, ovvero i nuovi poligono tramite gli indici rappresentativi di ciascuno punto;
    \end{itemize}
    
    MANCA DOCUMENTAZIONE DELLA PARTE DUEEEEEE
\end{enumerate}


\newpage
\subsection{UML PYTHON}
In questa sezione è presentato l'UML del programma in Python e una dettagliata descrizione di tutte le classi e di tutti i metodi implementati. Sono stati strutturati due UML diversi per rappresentare correttamente i diversi utilizzi del Design Pattern. I metodi non analizzati nel dettaglio sono stati implementati utilizzando lo stesso ragionamento e approccio del C++. \par

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.33]{uml_python.png}
    \caption{Python}
    \label{fig:my_label}
\end{figure}

Gli attributi principali sono:
\begin{itemize}
    \item Vertices. È un'istanza della classe Polygon che contiene i vertici del poligono;
    \item S1. È un'istanza della classe Vector2D che contiene il primo estremo del segmento;
    \item S2. È un'istanza della classe Vector2D che contiene il secondo estremo del segmento;
    \item Product. È una list che viene riempita dai metodi con la lista di nuovi punti e dei nuovi poligoni;
    \item Intersection Points. È un array di array bidimensionali, quindi si tratta di una matrice $intersection\_points\in R^{2,n}$ dato $n$ il numero di intersezioni;
    \item Segment. È un'istanza della classe Segment;
    \item Edge With Intersection. È una lista di booleani che dichiarano se l'intersezione è presente oppure no in un determinato lato;
    \item Matrix Tangent Vector. È un array di array, nello specifico\par $matrix\_tangent\_vector \in R^{2,2}$;
    \item X. Un float che corrisponde all'ascissa del punto;
    \item Y. Un float che corrisponde all'ordinata del punto.
\end{itemize}

Le classi implementate sono:
\begin{enumerate}
    \item Factory Method.\par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{FactoryMethod.png}
            \caption{Factory Method}
            \label{fig:my_label}
        \end{figure}
        Si tratta di una classe astratta che crea la classe Result. Implementa due metodi vuoti quali: 
        \begin{itemize}
            \item Produce New Points;
            \item Produce New Polygons.
        \end{itemize}
    \item Result. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{Result.png}
            \caption{Result}
            \label{fig:my_label}
        \end{figure}
        Si tratta di una classe astratta estesa dalle classi CutPolygon e NewPoints. L'obbiettivo di questa classe è di produrre il risultato finale. Implementa i seguenti metodi:
        \begin{itemize}
            \item Add New Points. Produce la lista dei nuovi punti;
            \item Add New Polygons. Produce i nuovi poligoni;
            \item Return Product. Restituisce una stringa con il risultato finale nella seguente forma: \par
                \textit{The set of vertices, endpoints of the segment, intersection points is:}
                \par\textit{The number of new points is:}
                \par\textit{The coordinates of the vertices of the new polygons are:}
                \par\textit{The numerical order is:}
        \end{itemize}
    \item New Points. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{NewPoints_Py.png}
            \caption{New Points}
            \label{fig:my_label}
        \end{figure}
        Ha l'obiettivo di produrre la list di nuovi punti. Implementa i seguenti metodi:
        \begin{itemize}
            \item Ordered Vertex. Riordina i vertici del poligono in senso antiorario a partire da quello in basso a sinistra, restiuisce l'array di array di vertici;
            \item Calc Points. Forma e restituisce la lista dei nuovi vertici, punti di intersezione e estremi del segmento, controllando di non inserire due volte lo stesso punto;
            \item Return Points. Produce e ritorna la lista completa dei nuovi punti.
        \end{itemize}
    \item Segment. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{Segment_Py.png}
            \caption{Segment}
            \label{fig:my_label}
        \end{figure}
        L'obiettivo è quello di calcolare la lunghezza di un segmento e di restituire la direzione. Implementa i seguenti metodi:
        \begin{itemize}
            \item To Matrix. Dati S1 e S2 restituisce la concatenazione dei due numpy array;
            \item Distance. Restituisce la lunghezza del segmento e quindi la distanza tra S1 e S2;
            \item Direction. Restituisce la direzione data dalla formula \par $Direction = S2 - S1$.
        \end{itemize}
    \item Polygon. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{Polygon_Py.png}
            \caption{Polygon}
            \label{fig:my_label}
        \end{figure}
        Ha l'obbiettivo di formare il poligono. Implementa il seguente metodo:
        \begin{itemize}
            \item Create Polygon. Presi in input i vertici come collezione di Vector2D restituisce un numpy array formato da due righe e $n$ colonne, dato $n$ il numero dei vertici.
        \end{itemize}
    \item Vector2D. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{Vector2D.png}
            \caption{Vector2D}
            \label{fig:my_label}
        \end{figure}
        \item Vector2D \par
        Il costruttore riceve in input due float coorrispondenti all'ascissa e all'ordinata. Ha l'obbiettivo di formare dei Vector2D e se necessario castarli in un numpy array bidimensionali colonna. Implementa i seguenti metodi:
        \begin{itemize}
            \item X. Restituisce l'ascissa;
            \item Y. Restituisce l'ordinata;
            \item Into Matrix. Forma un numpy array della forma $numpy.array([[0.], [0.], float)$.
        \end{itemize}
        Si è quindi sfruttato il supporto della libreria esterna numpy per compiere agilmente le operazioni tra array e array di array.
    \item Intersector. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{Intersector_Py.png}
            \caption{Intersector}
            \label{fig:my_label}
        \end{figure}
        Ha l'obbiettivo di calcolare i punti di intersezione tra i lati del poligono e il segmento di taglio. Implementa i seguenti metodi:
        \begin{itemize}
            \item Set Straight. Forma la prima colonna della matrice $matrix\_tangent\_vector$;
            \item Set Edge. Forma la seconda colonna della matrice $matrix\_tangent\_vector$;
            \item Intersection. Verifica se l'intersezione è presente oppure no e nel caso affermativo restiuisce le coordinate parametriche;
            \item Intersection Coordinate. Calcola le coordinate di intersezione.
        \end{itemize}
    \item Cut Polygon. \par
        \begin{figure}[h]
            \centering
            \includegraphics[scale=0.33]{CutPolygon_Py.png}
            \caption{Cut Polygon}
            \label{fig:my_label}
        \end{figure}
        Ha l'obiettivo di creare i nuovi poligoni formatisi dopo il taglio. Implementa i seguenti metodi:
        \begin{itemize}
            \item Cut. Taglia i poligoni;
            \item Choose Function. In base al numero di intersezioni, chiama il metodo $New Polygon Two Int$ o $New Polygons More Int$;
            \item New Polygon Two Int. Costruisce i poligoni tagliati con due intersezioni;
            \item New Polygon More Int. Costruisce i poligoni tagliati con più intersezioni;
            \item Return New Polygons. Restituisce la stringa con i nuovi poligoni.
        \end{itemize}
\end{enumerate}
\newpage



\newpage
\part{Test unitari}

Per testare il codice, sono stati utilizzati i google test per il C++, e la modalità di test standard offerta dal Python.
Sono stati eseguiti due tipi di test:
\begin{itemize}
    \item I test singoli, uno per ciascuna classe, per verificare il corretto funzionamento di ciascun metodo per ciascuna classe;
    \item I test completi per alcuni poligoni, convessi e concavi, per verificare che l'intero programma agisse nel modo desiderato.
\end{itemize}

In seguito descriveremo nello specifico i test generici sui poligoni.

\section{Test generici sui poligoni}\par
Per testare il corretto funzionamento del programma, abbiamo creato alcuni test che prendono in input i vertici di un particolare poligono (come vector di Vector2d), e gli estremi del segmento di taglio.\par
In seguito il test verifica che si crei nel modo corretto un'istanza "CutPolygon" e che i suoi metodi restituiscano quanto ci aspettiamo (il numero di intersezioni e i corretti punti di intersezione). \par
Successivamente il test controlla che si crei nel modo corretto un'istanza "NewPoints", e che questa permetta di calcolare tutti i nuovi punti che si vengono a creare.\par
In ultimo il test si accerta che si formino nel modo corretto i nuovi poligoni, verificando sia la corretta numerazione dei vertici, sia le loro coordinate.\par
Sono stati creati numerosi test per verificare la stabilità dell'algoritmo. In particolare, alcuni verificano il corretto funzionamento dell'algoritmo pensato per i tagli con due intersezioni, ed altri per i tagli con più intersezioni.\par
\newpage
\subsection{Rettangolo}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.20]{rettangolo.jpg}
    \caption{Poligono originale}\par
    \label{fig:my_label} \par
    \centering
    \includegraphics[scale=0.20]{rettangolo_tagliato.jpg}
    \caption{Poligoni dopo l'esecuzione del programma}
    \label{fig:my_label}
\end{figure}
newPoints =\par
[(1.0, 1.0), (5.0, 1.0), (5.0, 3.1), (1.0, 3.1), (1.7778, 1.0), (4.1111, 3.1), (2.0, 1.2), (4.0, 3.0) ]\par
cuttedPolygons =\par
( [0, 4, 5, 6, 7, 3], [4, 1, 2, 7, 6, 5] )
\newpage

\subsection{Pentagono}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.16]{pentagono.jpg}
    \caption{Poligono originale}\par
    \label{fig:my_label} \par
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.16]{pentagono_tagliato.jpg}
    \caption{Poligoni dopo l'esecuzione del programma}
    \label{fig:my_label}
\end{figure}
newPoints =\par
[ (2.5, 1.0), (4.0, 2.1), (3.4, 4.2), (1.6, 4.2), (1.0, 2.1), (1.2, 2.8), (1.4, 2.75), (3.6, 2.2) ]\par
cuttedPolygons =\par
( [0, 1, 5, 6, 7, 4], [1, 2, 3, 7, 6, 5] )
\newpage
\subsection{Poligono convesso}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.16]{convesso.jpg}
    \caption{Poligono originale}\par
    \label{fig:my_label} \par
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.10]{convesso_tagliato.jpg}
    \caption{Poligoni dopo l'esecuzione del programma}
    \label{fig:my_label}
\end{figure}
newPoints =\par
[ (2.4, 1.1), (5.8, 2.6), (3.3, 6.8), (1.7, 6.0), (4.7669, 4.3357), (2.3481, 1.4634), (2.8, 2.0), (4.4, 3.9) ]\par
cuttedPolygons =\par
( [0, 1, 4, 5, 6, 7], [4, 2, 3, 7, 6, 5] )
\newpage
\subsection{Poligono concavo 1}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.12]{concavo_1.jpg}
    \caption{Poligono originale}\par
    \label{fig:my_label} \par
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.12]{concavo_1_tagliato.jpg}
    \caption{Poligoni dopo l'esecuzione del programma}
    \label{fig:my_label}
\end{figure}
newPoints =\par
[(1.5, 1.0), (5.6, 1.5), (5.5, 4.8), (4.0, 6.2), (3.2, 4.2), (1.0, 4.0), (4.2043, 6.0093), (3.7213, 5.5033), (2.4086, 4.1281), (1.1912, 2.8527), (2.0, 3.7), (4.1, 5.9) ]\par
cuttedPolygons =\par
( [0, 1, 2, 6, 7, 8, 4, 9, 10, 11], [6, 3, 8, 7], [9, 5, 11, 10] )
\newpage
\subsection{Poligono concavo 2}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.11]{concavo_2.jpg}
    \caption{Poligono originale}\par
    \label{fig:my_label} \par
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.11]{concavo_2_tagliato.jpg}
    \caption{Poligoni dopo l'esecuzione del programma}
    \label{fig:my_label}
\end{figure}
newPoints =\par
[(3.5, 1.0), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.0, 5.6), (5.807, 3.307), (1.5341, 6.8908), (2.0, 6.5), (5.1, 3.9) ]\par
cuttedPolygons =\par
([0, 6, 7, 8, 9, 5], [6, 1, 2, 3, 4, 9, 8, 7])
\newpage
\subsection{Poligono concavo 3}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.17]{concavo_3.jpg}
    \caption{Poligono originale}\par
    \label{fig:my_label} \par
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.17]{concavo_3_tagliato.jpg}
    \caption{Poligoni dopo l'esecuzione del programma}
    \label{fig:my_label}
\end{figure}
newPoints =\par
[(1.0, 1.0), (4.5, 0.5), (5.0, 4.5), (3.5, 3.5), (3.0, 5.0), (2.0, 3.5), (0.5, 5.5), (4.9392, 4.0133), (4.2091, 3.9727), (3.3582, 3.9255), (2.2423, 3.8635), (1.748, 3.836), (0.6914, 3.7773), (1.1, 3.8), (2.9, 3.9) ]\par
cuttedPolygons =\par
([0, 1, 7, 8, 3, 9, 10, 11, 5, 12, 13, 14], [7, 2, 8], [9, 4, 11, 10], [12, 6, 14, 13])
\newpage
\end{document}