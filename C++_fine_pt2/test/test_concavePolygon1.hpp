#ifndef __TEST_CONCAVEPOLYGON1_H
#define __TEST_CONCAVEPOLYGON1_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace ConcavePolygon1Testing {

  TEST(TestPolygonCut, TestConcavePolygon1)
  {
      Vector2d v1, v2, v3, v4, v5, v6;
      v1 << 1.5, 1.0;
      v2 << 5.6, 1.5;
      v3 << 5.5, 4.8;
      v4 << 4.0, 6.2;
      v5 << 3.2, 4.2;
      v6 << 1.0, 4.0;

      Vector2d s1, s2;
      s1 << 2.0, 3.7;
      s2 << 4.1, 5.9;

      vector<Vector2d> concavePolygon1Vertex;
      concavePolygon1Vertex.push_back(v1);
      concavePolygon1Vertex.push_back(v2);
      concavePolygon1Vertex.push_back(v3);
      concavePolygon1Vertex.push_back(v4);
      concavePolygon1Vertex.push_back(v5);
      concavePolygon1Vertex.push_back(v6);

      try
      {
        PolygonCutLibrary::CutPolygon(concavePolygon1Vertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::CutPolygon concavePolygon1(concavePolygon1Vertex, s1, s2);

      try
      {
        concavePolygon1.Cut();
        EXPECT_EQ(4, concavePolygon1.NumIntersections());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters1, inters2, inters3, inters4, a, b, c, d;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 4.2043269, 6.0092949;
      inters2 << 3.7213115, 5.5032787;
      inters3 << 2.4085973, 4.1280543;
      inters4 << 1.1912162, 2.8527027;

      try
      {
        // i punti di intersezione che calcola
        a = concavePolygon1.IntersectionPoints()[0];
        b = concavePolygon1.IntersectionPoints()[1];
        c = concavePolygon1.IntersectionPoints()[2];
        d = concavePolygon1.IntersectionPoints()[3];
        intersectionPoints = concavePolygon1.IntersectionPoints();
        //controllo che coincidano a meno di una tolleranza
        EXPECT_TRUE(abs(a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE(abs(a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE(abs(b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE(abs(b(1) - inters2(1)) < 1e-6);
        EXPECT_TRUE(abs(c(0) - inters3(0)) < 1e-6);
        EXPECT_TRUE(abs(c(1) - inters3(1)) < 1e-6);
        EXPECT_TRUE(abs(d(0) - inters4(0)) < 1e-6);
        EXPECT_TRUE(abs(d(1) - inters4(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
         PolygonCutLibrary::NewPoints(concavePolygon1Vertex, intersectionPoints, s1, s2);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: NewPoints newP(concavePolygon1Vertex, intersectionPoints, s1, s2);

      try
      {
         EXPECT_EQ(newP.OrderedVertex(), concavePolygon1Vertex);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> newPoints;
      try
      {
        newP.CalcPoints();
        EXPECT_EQ(12, newP.NumNewPoints());
        newPoints = newP.ReturnPoints();
        // controllo che i primi 6 punti corrispondano ai vertici
        EXPECT_EQ(v1, newPoints[0]);
        EXPECT_EQ(v2, newPoints[1]);
        EXPECT_EQ(v3, newPoints[2]);
        EXPECT_EQ(v4, newPoints[3]);
        EXPECT_EQ(v5, newPoints[4]);
        EXPECT_EQ(v6, newPoints[5]);
        // controllo che 4 siano le intersezioni
        EXPECT_EQ(newPoints[6], a);
        EXPECT_EQ(newPoints[7], b);
        EXPECT_EQ(newPoints[8], c);
        EXPECT_EQ(newPoints[9], d);
        // controllo che gli ultimi due siano gli estremi del segmento
        EXPECT_EQ(newPoints[10], s1);
        EXPECT_EQ(newPoints[11], s2);
        concavePolygon1.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2, cuttedPolygon3;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2, newVertex3;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(v2);
      cuttedPolygon1.push_back(v3);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v5);
      cuttedPolygon1.push_back(c);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(d);
      newVertex1.push_back(0);
      newVertex1.push_back(1);
      newVertex1.push_back(2);
      newVertex1.push_back(6);
      newVertex1.push_back(7);
      newVertex1.push_back(8);
      newVertex1.push_back(4);
      newVertex1.push_back(9);
      newVertex1.push_back(10);
      newVertex1.push_back(11);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v4);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s2);
      newVertex2.push_back(6);
      newVertex2.push_back(3);
      newVertex2.push_back(8);
      newVertex2.push_back(7);

      cuttedPolygon3.push_back(c);
      cuttedPolygon3.push_back(v6);
      cuttedPolygon3.push_back(d);
      cuttedPolygon3.push_back(s1);
      newVertex3.push_back(9);
      newVertex3.push_back(5);
      newVertex3.push_back(11);
      newVertex3.push_back(10);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allCuttedPol.push_back(cuttedPolygon3);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);
      allNewVertex.push_back(newVertex3);

      try
      {
      EXPECT_EQ(allCuttedPol, concavePolygon1.CuttedPolygons());
      EXPECT_EQ(allNewVertex, concavePolygon1.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_CONCAVEPOLYGON1_H
