#ifndef INTERSECTOR_H
#define INTERSECTOR_H

#include <iostream>
#include <vector>
#include <Eigen>
#include <NewPoints.hpp>
#include "Segment.hpp"

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class Intersector: public IIntersector {

    public:
        static double toleranceParallelism;
        static double toleranceIntersection;

    private:
        Vector2d _origin1;
        Vector2d _origin2;
        Vector2d _resultParametricCoordinates;
        Vector2d _rightHandSide;
        Matrix2d _matrixTangentVector;
        Vector2d _intersectionCoordinates;

    public:
        Intersector() {};

        void SetStraight(const Vector2d& origin1, const Vector2d& end1);
        void SetEdge(const Vector2d& origin2, const Vector2d& end2);
        bool Intersection();
        const Vector2d& ParametricCoordinates() const { return _resultParametricCoordinates; } // [s1, s2], non sono ancora i punti di intersezione
        const Vector2d& IntersectionCoordinates(); //la chiamo solo se c'è intersezione
  };
}

#endif // INTERSECTOR_H
