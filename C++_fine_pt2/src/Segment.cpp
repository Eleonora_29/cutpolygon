#include "Segment.hpp"

namespace PolygonCutLibrary {

double Segment::Distance() const
{
    return sqrt(pow((_s1(0) - _s2(0)), 2) + pow((_s1(1) - _s2(1)), 2));
}

Vector2d Segment::Direction() const
{
    Vector2d direz;
    direz = _s2 - _s1;
    return direz;
}

}
