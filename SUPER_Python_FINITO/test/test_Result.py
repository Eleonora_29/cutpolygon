from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestRectangle(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(1, 1),
                    V2d.Vector2d(5, 1),
                    V2d.Vector2d(3, 4)]
        return vertices

    def test_Result(self):
        try:
            vertices = TestRectangle.FillPolygonVertices()
            s1 = V2d.Vector2d(2, 2)
            s2 = V2d.Vector2d(3.55, 2.46)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(1.0, 1.0), (5.0, 1.0), (3.0, 4.0), (2.0, 2.0), (3.55, 2.46), (3.9479, 2.5781),"
                             " (1.5845, 1.8767)]\n"
                             "The number of new points is:\n"
                             "7\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(1.0, 1.0), (5.0, 1.0), (3.9479, 2.5781), (3.55, 2.46), (2.0, 2.0), (1.5845, 1.8767)],"
                             " [(3.9479, 2.5781), (3.0, 4.0), (1.5845, 1.8767), (2.0, 2.0), (3.55, 2.46)])\n"
                             "The numerical order is:\n"
                             "([0, 1, 3, 4, 5, 6], [3, 2, 6, 5, 4])")
        except:
            self.fail()
