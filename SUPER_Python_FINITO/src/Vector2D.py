import numpy as np


class Vector2d:
    def __init__(self, x: float, y: float):
        self.__x = x  # Private
        self.__y = y

    def x(self):
        return self.__x

    def y(self):
        return self.__y

    def into_matrix(self):  # Converting to a numpy array
        vector = np.array([[0.], [0.]], float)
        vector[0][0] = self.__x
        vector[1][0] = self.__y
        return vector
