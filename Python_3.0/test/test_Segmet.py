from unittest import TestCase
import src.Segment as Seg
import src.Vector2d as V2d


class TestSegment(TestCase):
    def test_InMatrix(self):
        s1 = V2d.Vector2d(1, 2)
        s2 = V2d.Vector2d(5, 9)
        self.assertEqual(str(Seg.Segment.InMatrix(Seg, s1, s2)), "[[1. 5.]\n [2. 9.]]")

    def test_CoefAng(self): #Test su tutti i casi di coefficienti angolari
        s1 = V2d.Vector2d(2, 4)
        s2 = V2d.Vector2d(0, 2)
        self.assertTrue(abs(Seg.Segment.CoefAng(Seg, s1, s2) - 1) < 1e-6)

        s1 = V2d.Vector2d(2, 4)
        s2 = V2d.Vector2d(2, 2)
        self.assertEqual(Seg.Segment.CoefAng(Seg, s1, s2), 'Inf')

        s1 = V2d.Vector2d(2, 4)
        s2 = V2d.Vector2d(0, 4)
        self.assertTrue(abs(Seg.Segment.CoefAng(Seg, s1, s2) - 0) < 1e-6)

        s1 = V2d.Vector2d(2, 4)
        s2 = V2d.Vector2d(2, 4)
        self.assertEqual(Seg.Segment.CoefAng(Seg, s1, s2), 'I due punti passati coincidono')


'''
Classe di TestSegmento -> FUNZIONA
1. Test su InMatrix: prende due V2d e li mette in una matrice unica
2. Test sui Coefficienti angolari: prende due punti e calcola il coefficiente angolare della retta

-Tengo conto della tolleranza
'''