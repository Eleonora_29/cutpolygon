from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestConcavePolygon1(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(1.5, 1),
                    V2d.Vector2d(5.6, 1.5),
                    V2d.Vector2d(5.5, 4.8),
                    V2d.Vector2d(4, 6.2),
                    V2d.Vector2d(3.2, 4.2),
                    V2d.Vector2d(1, 4)]
        return vertices

    def test_Polygon(self):
        try:
            vertices = TestConcavePolygon1.FillPolygonVertices()
            s1 = V2d.Vector2d(2, 3.7)
            s2 = V2d.Vector2d(4.1, 5.9)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(1.5, 1.0), (5.6, 1.5), (5.5, 4.8), (4.0, 6.2), (3.2, 4.2), (1.0, 4.0), "
                             "(2.0, 3.7), (4.1, 5.9), (4.2043, 6.0093), (3.7213, 5.5033), (2.4086, 4.1281), "
                             "(1.1912, 2.8527)]\nThe number of new points is:\n12\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(1.5, 1.0), (5.6, 1.5), (5.5, 4.8), (4.2043, 6.0093), (4.1, 5.9), (3.7213, 5.5033), "
                             "(3.2, 4.2), (2.4086, 4.1281), (2.0, 3.7), (1.1912, 2.8527)], [(4.2043, 6.0093), "
                             "(4.0, 6.2), (3.7213, 5.5033), (4.1, 5.9)], [(2.4086, 4.1281), (1.0, 4.0), "
                             "(1.1912, 2.8527), (2.0, 3.7)])"
                             "\nThe numerical order is:\n([0, 1, 2, 6, 7, 8, 4, 9, 10, 11], "
                             "[6, 3, 8, 7], [9, 5, 11, 10])")
        except:
            self.fail()