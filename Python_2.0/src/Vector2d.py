class Vector2d:
    def __init__(self, x: float, y: float): #Costruttore
        self.x = x
        self.y = y

    def X(self): #Metodo per le X
        return self.x

    def Y(self): #Metodo per le Y
        return self.y

    def IsV2d(self, point): #Verifica se un elemento è oppure no un V2d
        if (str(type(point)) == "<class 'src.Vector2d.Vector2d'>"):
            return True
        else:
            return False


'''
Classe Vettore a due dimensioni -> FUNZIONA
1. Metodo costruttore: prende in input x e y e le assegna
2. Metodo X: resituisce il valore dell'ascissa
3. Metodo Y: restituisce il valore dell'ordinata
4. Metodo IsV2d: restituisce Vero o falso in base alla tipologia dell'elemento passato
'''