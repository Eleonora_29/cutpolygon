from unittest import TestCase
import src.Intersector_B as Int
import src.Vector2d as V2d
import numpy as np


class TestIntersector(TestCase):
    def test_SetStraight(self):
        matrix_tangent_vector = np.zeros((2, 2), float)
        Int.Intersector.__init__(Int, matrix_tangent_vector)
        origin_l1 = V2d.Vector2d(1, 2)
        end_l1 = V2d.Vector2d(2, 5)
        matrix_tangent_vector = Int.Intersector.SetStraight(Int, origin_l1, end_l1)
        result = str(matrix_tangent_vector)
        self.assertEqual(result, "[[1. 0.]\n [3. 0.]]")

    def test_Edge(self):
        matrix_tangent_vector = np.zeros((2, 2), float)
        Int.Intersector.__init__(Int, matrix_tangent_vector)
        origin_l1 = V2d.Vector2d(1, 2)
        end_l1 = V2d.Vector2d(2, 5)
        origin_l2 = V2d.Vector2d(3, 3)
        end_l2 = V2d.Vector2d(0, 4)
        matrix_tangent_vector = Int.Intersector.SetStraight(Int, origin_l1, end_l1)
        matrix_tangent_vector = Int.Intersector.SetEdge(Int, origin_l2, end_l2)
        result = str(matrix_tangent_vector)
        self.assertEqual(result, "[[ 1.  3.]\n [ 3. -1.]]")

    def test_Intersection(self):
        matrix_tangent_vector = np.zeros((2, 2), float)
        Int.Intersector.__init__(Int, matrix_tangent_vector)
        origin_l1 = V2d.Vector2d(3, 0)
        end_l1 = V2d.Vector2d(3, 5)
        origin_l2 = V2d.Vector2d(1, 1)
        end_l2 = V2d.Vector2d(5, 3)
        matrix_tangent_vector = Int.Intersector.SetStraight(Int, origin_l1, end_l1)
        matrix_tangent_vector = Int.Intersector.SetEdge(Int, origin_l2, end_l2)
        resultParametricCoordinates, intersection = Int.Intersector.Intersection(Int, origin_l1, origin_l2)
        resultParametricCoordinates = str(resultParametricCoordinates)
        intersection = str(intersection)
        result = resultParametricCoordinates + "\n " + intersection
        self.assertEqual(result, "[[0.4]\n [0.5]]\n True")

    def test_IntersectionCoordinates(self):
        matrix_tangent_vector = np.zeros((2, 2), float)
        Int.Intersector.__init__(Int, matrix_tangent_vector)
        origin_l1 = V2d.Vector2d(3, 0)
        end_l1 = V2d.Vector2d(3, 5)
        origin_l2 = V2d.Vector2d(1, 1)
        end_l2 = V2d.Vector2d(5, 3)
        matrix_tangent_vector = Int.Intersector.SetStraight(Int, origin_l1, end_l1)
        matrix_tangent_vector = Int.Intersector.SetEdge(Int, origin_l2, end_l2)
        resultParametricCoordinates, intersection = Int.Intersector.Intersection(Int, origin_l1, origin_l2)
        self.assertEqual(str(Int.Intersector.IntersectionCoordinates(Int, origin_l1, resultParametricCoordinates)), "[[3.]\n [2.]]")

'''
Classe di TestIntersecor -> FUNZIONA
1. Test su SetStraight: testa l'esattezza della matrix_tangent_vector
2. Test su Intersection: test che l'intersezione venga trovata correttamente e che ritorni le coordinate corrette
3. Test su IntersectionCoordinates: testa l'esattezza delle coordinate di intersezioni
'''