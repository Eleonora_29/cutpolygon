from unittest import TestCase
import src.Polygon as Polyg
import numpy as np


class TestPolygon(TestCase):
    def test_Cut(self):
        vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
        s1 = np.array([[2], [1.2]])
        s2 = np.array([[4], [3]])
        edgeWithInters, intersection_point, num_intersection = Polyg.Polygon.Cut(Polyg, s1, s2, 4, vertex)
        intersection_point = str(intersection_point)
        num_intersection = str(num_intersection)
        edgeWithInters = str(edgeWithInters)
        result = edgeWithInters +"\n"+ intersection_point +"\n"+ num_intersection
        self.assertEqual(result,  "[True, False, True, False]\n[[1.77777778 4.11111111]\n [1.         3.1       ]]\n2")

    def test_NewPolygonsTwoInt(self):
        pass

    def test_NewPolygonsMoreInt(self):
        pass

'''
Test vuoto da fare
'''

