from unittest import TestCase
import src.Intersector as Int
import numpy as np


class TestIntersector(TestCase):
    def test_SetStraight(self):
        origin_l1 = np.array([[1.],[2.]])
        end_l1 = np.array([[2.], [5.]])
        self.assertEqual(str(Int.Intersector.SetStraight(Int, origin_l1, end_l1)), "[[1. 0.]\n [3. 0.]]")

    def test_Edge(self):
        origin_l2 = np.array([[3.], [4.]])
        end_l2 = np.array([[0.], [5.]])
        matrix_tangent_vector = np.array([[1., 0.], [3., 0.]])
        self.assertEqual(str(Int.Intersector.SetEdge(Int, matrix_tangent_vector, origin_l2, end_l2)), "[[ 1.  3.]\n [ 3. -1.]]")


    def test_Intersection(self):
        origin_l1 = np.array([[3], [0]])
        origin_l2 = np.array([[1], [1]])
        matrix_tangent_vector = np.array([[0., -4.],[5., -2.]])
        resultParametricCoordinates, intersection = Int.Intersector.Intersection(Int,matrix_tangent_vector, origin_l1, origin_l2)
        resultParametricCoordinates = str(resultParametricCoordinates)
        intersection = str(intersection)
        result = resultParametricCoordinates + "\n " + intersection
        self.assertEqual(result, "[[0.4]\n [0.5]]\n True")

    def test_IntersectionCoordinates(self):
        origin_l1 = np.array([[3], [0]])
        resultParametricCoordinates = np.array([[0.4], [0.5]])
        matrix_tangent_vector = np.array([[0., -4.], [5., -2.]])
        self.assertEqual(str(Int.Intersector.IntersectionCoordinates(Int, matrix_tangent_vector, origin_l1, resultParametricCoordinates)), "[[3.]\n [2.]]")

'''
Classe di TestIntersecor -> FUNZIONA
1. Test su SetStraight: testa l'esattezza della prima colonna di matrix_tangent_vector
2. Test su SetEdge: testa l'esattezza della seconda colonna di matrix_tangent_vector
2. Test su Intersection: test che l'intersezione venga trovata correttamente e che ritorni le coordinate corrette
3. Test su IntersectionCoordinates: testa l'esattezza delle coordinate di intersezioni
'''