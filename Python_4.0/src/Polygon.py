import src.Intersector as Int
import numpy as np

class Polygon:
    def __init__(self):
        pass

    def Cut(self, s1, s2, num_vertex, vertex):
        i: int = 0
        num_intersections = 0
        edgeWithInters = []
        intersection_points = np.array([[0],[0]], float)
        j = 0
        for i in range(0, num_vertex, 1):
            matrix_tangent_vector = Int.Intersector.SetStraight(Int, s1, s2)
            if (i != (num_vertex - 1)):
                l0 = np.zeros((2, 1), float)
                l0[0][0] = vertex[0][0]
                l0[1][0] = vertex[1][0]
                li = np.zeros((2, 1), float)
                li[0][0] = vertex[0][i]
                li[1][0] = vertex[1][i]
                li_1 = np.zeros((2, 1), float)
                li_1[0][0] = vertex[0][i + 1]
                li_1[1][0] = vertex[1][i + 1]
                Int.Intersector.SetEdge(Int, matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.SetEdge(Int, matrix_tangent_vector, l0, li)
            resultParametricCoordinates, inters = Int.Intersector.Intersection(Int, matrix_tangent_vector, s1, li)
            if (inters == True):
                intersectionCoordinates = Int.Intersector.IntersectionCoordinates(Int, matrix_tangent_vector, s1, resultParametricCoordinates)
                if ((intersectionCoordinates[0][0] != intersection_points[0][num_intersections - 1]) and (intersectionCoordinates[1][0] != intersection_points[1][num_intersections - 1])):
                    edgeWithInters.append(True)
                    num_intersections += 1
                    flag = np.array([[0],[0]], float)
                    flag = intersectionCoordinates
                    j += 1
                    intersection_points = np.append(intersection_points, flag, axis = 1)
                else:
                    edgeWithInters.append(False)

            else:
                edgeWithInters.append(False)
        intersection_points = np.transpose(intersection_points)
        intersection_points = np.delete(intersection_points, 0, 0)
        intersection_points = np.transpose(intersection_points)
        return edgeWithInters, intersection_points, num_intersections

    def ChooseFunction(self, num_intersections):
        if(num_intersections == 2):
            Polygon.NewPolygonsTwoInt()
        else:
            Polygon.NewPolygonsMoreInt()

    def NewPolygonsTwoInt(self):
        pass

    def NewPolygonsMoreInt(self):
        pass
'''
vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
s1 = np.array([[2], [1.2]])
s2 = np.array([[4], [3]])

edgeWithInters, intersectionPoint, num_intersection = Polygon.Cut(Polygon, s1, s2, 4, vertex)
print(edgeWithInters,"\n", intersectionPoint, "\n", num_intersection)


'''