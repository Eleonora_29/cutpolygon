import numpy as np
import src.Intersector as Int
import src.Segment as Seg
import src.NewPoints as Np
import src.Vector2D as V2d
import src.Polygon as Pol
tol = 1e-7


class CutPolygon:
    def __init__(self, vertex, seg1, seg2):
        self.__vertices = Pol.Polygon.create_polygon(vertex)
        self.__s1 = seg1
        self.__s2 = seg2
        self.__edge_with_intersection = []
        self.__intersection_points = np.array([[0.], [0.]], float)

    def cut(self):
        new_points = Np.NewPoints(self.__vertices, self.__intersection_points, self.__s1, self.__s2)
        self.__vertices = Np.NewPoints.ordered_vertex(new_points)
        num_inter = 0
        num_vertex = np.size(self.__vertices, axis=1)
        li_1 = V2d.Vector2d(0, 0)
        for i in range(0, num_vertex, 1):
            matrix_tangent_vector = Int.Intersector()
            l0 = V2d.Vector2d(self.__vertices[0][0], self.__vertices[1][0])
            li = V2d.Vector2d(self.__vertices[0][i], self.__vertices[1][i])
            if i < num_vertex - 1:
                li_1 = V2d.Vector2d(self.__vertices[0][i + 1], self.__vertices[1][i + 1])
            if i != (num_vertex - 1):
                Int.Intersector.set_edge(matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.set_edge(matrix_tangent_vector, li, l0)
            Int.Intersector.set_straight(matrix_tangent_vector, self.__s1, self.__s2)
            parametric_coord, inters = Int.Intersector.intersection(matrix_tangent_vector, self.__s1, li)
            if inters is True:
                intersection_coordinates = Int.Intersector.intersection_coordinates(matrix_tangent_vector, self.__s1,
                                                                                    parametric_coord)
                if (num_inter == 0 or
                        ((abs(intersection_coordinates[0][0] - self.__intersection_points[0][num_inter]) >= tol) or
                         (abs(intersection_coordinates[1][0] - self.__intersection_points[1][num_inter]) >= tol))):
                    self.__edge_with_intersection.append(True)
                    num_inter += 1
                    self.__intersection_points = np.append(self.__intersection_points, intersection_coordinates, axis=1)
                else:
                    self.__edge_with_intersection.append(False)

            else:
                self.__edge_with_intersection.append(False)
        self.__intersection_points = np.transpose(self.__intersection_points)
        self.__intersection_points = np.delete(self.__intersection_points, 0, 0)
        self.__intersection_points = np.transpose(self.__intersection_points)
        return self.__intersection_points, num_inter

    def choose_function(self):
        num_intersection = np.size(self.__intersection_points, axis=1)
        if num_intersection == 2:
            cut_polygons, new_polygons_vertex = CutPolygon.new_polygons_two_int(self)
            return cut_polygons, new_polygons_vertex
        else:
            cut_polygons, new_polygons_vertex = CutPolygon.new_polygons_more_int(self)
            return cut_polygons, new_polygons_vertex

    def new_polygons_two_int(self):
        pol1 = np.array([[0], [0]], float)
        pol2 = np.array([[0], [0]], float)
        polNum1: list = [0]
        polNum2: list = []
        flag = np.array([[0], [0]], float)
        flag1 = np.array([[0], [0]], float)
        flag[0][0] = self.__vertices[0][0]
        flag[1][0] = self.__vertices[1][0]
        pol1 = np.append(pol1, flag, axis=1)
        pol1 = np.transpose(pol1)
        pol1 = np.delete(pol1, 0, 0)
        pol1 = np.transpose(pol1)
        corresponding_points = np.array([[0], [0]], float)
        temp2 = np.array([[0], [0]], float)
        distances = []
        i: int = 0
        num_vertex = np.size(self.__vertices, axis=1)
        num_intersection = np.size(self.__intersection_points, axis=1)
        n: int = num_vertex
        p1: int = 1
        p2: int = 0
        v1: int = 1
        x: int = num_vertex
        while v1 < x:
            if self.__edge_with_intersection[v1 - 1] is True:
                x = v1 - 1
                v2 = v1
                if (abs(self.__intersection_points[0][i] - pol1[0][p1 - 1]) >= tol or
                        (abs(self.__intersection_points[1][i] - pol1[1][p1 - 1] >= tol))):
                    if (abs(self.__intersection_points[0][i] - self.__vertices[0][v1]) >= tol or
                            (abs(self.__intersection_points[1][i] - self.__vertices[1][v1]) >= tol)):
                        flag[0][0] = self.__intersection_points[0][i]
                        flag[1][0] = self.__intersection_points[1][i]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, n)
                        polNum2 = np.append(polNum2, n)
                        n += 1

                    else:
                        flag[0][0] = self.__vertices[0][v1]
                        flag[1][0] = self.__vertices[1][v1]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, v1)
                        polNum2 = np.append(polNum2, v1)
                        v2 += 1

                    p1 += 1
                    p2 += 1
                flag_v2d = V2d.Vector2d(self.__intersection_points[0][i], self.__intersection_points[1][i])
                segment = Seg.Segment(flag_v2d, self.__s1)
                e = Seg.Segment.distance(segment)
                distances = np.append(distances, e)
                s1 = V2d.Vector2d.into_matrix(self.__s1)
                corresponding_points = np.append(corresponding_points, s1, axis=1)
                segment = Seg.Segment(flag_v2d, self.__s2)
                f = Seg.Segment.distance(segment)
                distances = np.append(distances, f)
                s2 = V2d.Vector2d.into_matrix(self.__s2)
                corresponding_points = np.append(corresponding_points, s2, axis=1)

                z = i + 1
                for z in range(z, num_intersection):
                    flag_v2d = V2d.Vector2d(self.__intersection_points[0][i], self.__intersection_points[1][i])
                    flag_v2d1 = V2d.Vector2d(self.__intersection_points[0][z], self.__intersection_points[1][z])
                    flag1[0][0] = self.__intersection_points[0][z]
                    flag1[1][0] = self.__intersection_points[1][z]
                    segment = Seg.Segment(flag_v2d, flag_v2d1)
                    d = Seg.Segment.distance(segment)
                    distances = np.append(distances, d)
                    corresponding_points = np.append(corresponding_points, flag1, axis=1)

                distance_size = np.size(distances)
                for z in range(0, distance_size - 1):
                    min = z
                    w = z + 1
                    for w in range(w, distance_size):
                        if distances[w] < distances[min]:
                            min = w
                    temp1 = distances[min]
                    temp2[0][0] = corresponding_points[0][min + 1]
                    temp2[1][0] = corresponding_points[1][min + 1]
                    distances[min] = distances[z]
                    corresponding_points[0][min + 1] = corresponding_points[0][z + 1]
                    corresponding_points[1][min + 1] = corresponding_points[1][z + 1]
                    distances[z] = temp1
                    corresponding_points[0][z + 1] = temp2[0][0]
                    corresponding_points[1][z + 1] = temp2[1][0]

                for a in range(0, distance_size):
                    if (abs(corresponding_points[0][a + 1] - pol1[0][p1 - 1]) >= tol or
                            (abs(corresponding_points[1][a + 1] - pol1[1][p1 - 1]) >= tol)):
                        flag[0][0] = corresponding_points[0][a + 1]
                        flag[1][0] = corresponding_points[1][a + 1]
                        pol1 = np.append(pol1, flag, axis=1)
                        p1 += 1
                        polNum1 = np.append(polNum1, n)
                        n += 1

                for b in range(v1, num_vertex):
                    if self.__edge_with_intersection[b] is True:
                        for c in range(b + 1, num_vertex):
                            flag[0][0] = self.__vertices[0][c]
                            flag[1][0] = self.__vertices[1][c]
                            pol1 = np.append(pol1, flag, axis=1)
                            polNum1 = np.append(polNum1, c)
                        c: int
                        for c in range(v2, b + 1):
                            if (abs(self.__vertices[0][c] - pol2[0][p2]) >= tol or
                                    (abs(self.__vertices[1][c] - pol2[1][p2]) >= tol)):
                                flag[0][0] = self.__vertices[0][c]
                                flag[1][0] = self.__vertices[1][c]
                                pol2 = np.append(pol2, flag, axis=1)
                                polNum2 = np.append(polNum2, c)

                        flag[0][0] = self.__intersection_points[0][1]
                        flag[1][0] = self.__intersection_points[1][1]
                        pol2 = np.append(pol2, flag, axis=1)
                        m: int = n - 1
                        polNum2 = np.append(polNum2, m)
                        m -= 1

                        if f < e:
                            s1 = V2d.Vector2d.into_matrix(self.__s1)
                            s2 = V2d.Vector2d.into_matrix(self.__s2)
                            pol2 = np.append(pol2, s1, axis=1)
                            pol2 = np.append(pol2, s2, axis=1)
                        else:
                            s1 = V2d.Vector2d.into_matrix(self.__s1)
                            s2 = V2d.Vector2d.into_matrix(self.__s2)
                            pol2 = np.append(pol2, s2, axis=1)
                            pol2 = np.append(pol2, s1, axis=1)

                        polNum2 = np.append(polNum2, m)
                        polNum2 = np.append(polNum2, m - 1)

            else:
                if (abs(self.__vertices[0][v1] - pol1[0][p1 - 1]) >= tol or
                        (abs(self.__vertices[1][v1] - pol1[1][p1 - 1]) >= tol)):
                    flag[0][0] = self.__vertices[0][v1]
                    flag[1][0] = self.__vertices[1][v1]
                    pol1 = np.append(pol1, flag, axis=1)
                    polNum1 = np.append(polNum1, v1)
            v1 += 1
        pol2 = np.transpose(pol2)
        pol2 = np.delete(pol2, 0, 0)
        pol2 = np.transpose(pol2)

        pol1 = np.round(pol1, 4)
        pol1_0 = pol1[0, :]
        pol1_1 = pol1[1, :]
        pol1 = list(zip(pol1_0, pol1_1))
        pol2 = np.round(pol2, 4)
        pol2_0 = pol2[0, :]
        pol2_1 = pol2[1, :]
        pol2 = list(zip(pol2_0, pol2_1))
        cut_polygons = pol1, pol2
        new_polygons_vertex = list(polNum1), list(map(int, polNum2))
        return cut_polygons, new_polygons_vertex

    def new_polygons_more_int(self):
        cut_polygons: list = []
        new_polygons_vertex: list = []
        j = 0
        distances = []
        corresponding_points = np.array([[0], [0]], float)
        whatEdgeInters: list = []
        num_vertex = np.size(self.__vertices, axis=1)
        num_intersection = np.size(self.__intersection_points, axis=1)
        for a in range(0, num_vertex):
            if self.__edge_with_intersection[a] is True:
                whatEdgeInters = np.append(whatEdgeInters, a)
        pol = np.array([[0], [0]], float)
        polNum: list = []
        temp = np.array([[0], [0]], float)
        temp2 = np.array([[0], [0]], float)
        g = np.array([[0], [0]], float)
        temp[0][0] = self.__vertices[0][0]
        temp[1][0] = self.__vertices[1][0]
        pol = np.append(pol, temp, axis=1)
        pol = np.transpose(pol)
        pol = np.delete(pol, 0, 0)
        pol = np.transpose(pol)
        polNum = np.append(polNum, 0)
        straight_order: list = []
        summit_just_added: bool = False
        v0_right: bool = True
        v_new_right: bool
        i: int = 0
        num_l: int = num_vertex
        n: int = num_vertex
        int_found: bool = False
        temp_d2 = np.array([[0], [0]], float)
        s1 = V2d.Vector2d.into_matrix(self.__s1)
        s2 = V2d.Vector2d.into_matrix(self.__s2)
        d = s2 - s1
        flag1: int = 0
        flag2: int = 0
        temp[0][0] = self.__vertices[0][0]
        temp[1][0] = self.__vertices[1][0]
        h = temp - s1
        edge = np.array([[0], [0]], float)
        if (d[0][0] * h[1][0] - d[1][0] * h[0][0]) > tol:
            v0_right = False
        elif (d[0][0] * h[1][0] - d[1][0] * h[0][0]) < -tol:
            v0_right = True
        v = 1
        while v < num_l:
            if self.__edge_with_intersection[v - 1] is True:
                num_l = v - 1
                if (abs(self.__intersection_points[0][i] - pol[0][v - 1]) >= tol) or \
                        (abs(self.__intersection_points[1][i] - pol[1][v - 1]) >= tol):
                    temp[0][0] = self.__intersection_points[0][i]
                    temp[1][0] = self.__intersection_points[1][i]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, n)
                    n += 1
                if (abs(s1[0][0] - self.__intersection_points[0][i]) <= tol) and \
                        (abs(s1[1][0] - self.__intersection_points[1][i]) <= tol):
                    flag1 = 1
                if (abs(s2[0][0] - self.__intersection_points[0][i]) <= tol) and \
                        (abs(s2[1][0] - self.__intersection_points[1][i]) <= tol):
                    flag2 = 1

                for z in range(i + 1, num_intersection):
                    temp_v2d = V2d.Vector2d(self.__intersection_points[0][i], self.__intersection_points[1][i])
                    temp2_v2d = V2d.Vector2d(self.__intersection_points[0][z], self.__intersection_points[1][z])
                    temp2[0][0] = self.__intersection_points[0][z]
                    temp2[1][0] = self.__intersection_points[1][z]
                    seg = Seg.Segment(temp_v2d, temp2_v2d)
                    e = Seg.Segment.distance(seg)
                    distances = np.append(distances, e)
                    corresponding_points = np.append(corresponding_points, temp2, axis=1)
                    if (abs(s1[0][0] - self.__intersection_points[0][z]) <= tol) and \
                            (abs(s1[1][0] - self.__intersection_points[1][z]) <= tol):
                        flag1 = 1
                    if (abs(s2[0][0] - self.__intersection_points[0][z]) <= tol) and \
                            (abs(s2[1][0] - self.__intersection_points[1][z]) <= tol):
                        flag2 = 1
                if flag1 == 0:
                    temp_v2d = V2d.Vector2d(self.__intersection_points[0][i], self.__intersection_points[1][i])
                    seg = Seg.Segment(temp_v2d, self.__s1)
                    e = Seg.Segment.distance(seg)
                    distances = np.append(distances, e)
                    corresponding_points = np.append(corresponding_points, s1, axis=1)
                if flag2 == 0:
                    temp_v2d = V2d.Vector2d(self.__intersection_points[0][i], self.__intersection_points[1][i])
                    seg = Seg.Segment(temp_v2d, self.__s2)
                    e = Seg.Segment.distance(seg)
                    distances = np.append(distances, e)
                    corresponding_points = np.append(corresponding_points, s2, axis=1)

                distance_size = np.size(distances)
                for z in range(0, distance_size - 1):
                    min: int = z
                    w = z + 1
                    for w in range(w, distance_size):
                        if distances[w] < distances[min]:
                            min = w
                    temp_d = distances[min]
                    temp_d2[0][0] = corresponding_points[0][min + 1]
                    temp_d2[1][0] = corresponding_points[1][min + 1]
                    distances[min] = distances[z]
                    corresponding_points[0][min + 1] = corresponding_points[0][z + 1]
                    corresponding_points[1][min + 1] = corresponding_points[1][z + 1]
                    distances[z] = temp_d
                    corresponding_points[0][z + 1] = temp_d2[0][0]
                    corresponding_points[1][z + 1] = temp_d2[1][0]
                e = num_vertex + 1
                flag = 0
                for h in range(0, np.size(distances)):
                    for f in range(0, num_vertex):
                        if (abs(corresponding_points[0][h + 1] - self.__vertices[0][f]) <= tol) and \
                                (abs(corresponding_points[1][h + 1] - self.__vertices[1][f]) <= tol):
                            flag = 1
                            straight_order = np.append(straight_order, f)
                    if flag == 0:
                        straight_order = np.append(straight_order, e)
                        e += 1
                    flag = 0
                z = 0
                while z < np.size(straight_order):
                    int_found = True
                    flag = 0
                    i += 1
                    temp[0][0] = corresponding_points[0][z + 1]
                    temp[1][0] = corresponding_points[1][z + 1]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, straight_order[z])
                    z += 1
                    if summit_just_added is False:
                        b = 0
                        while b < np.size(self.__intersection_points, axis=1):
                            if (abs(corresponding_points[0][z] - self.__intersection_points[0][b]) <= tol) and \
                                    (abs(corresponding_points[1][z] - self.__intersection_points[1][b]) <= tol):
                                for a in range(1, num_vertex):
                                    if (abs(self.__vertices[0][a] - self.__intersection_points[0][b]) <= tol) and \
                                            (abs(self.__vertices[1][a] - self.__intersection_points[1][b]) <= tol):
                                        flag = 1
                                        g = np.array([[0], [0]], float)
                                        if a < (num_vertex - 1):
                                            g[0][0] = self.__vertices[0][a + 1] - s1[0][0]
                                            g[1][0] = self.__vertices[1][a + 1] - s1[1][0]
                                        if (d[0][0] * g[1][0] - d[1][0] * g[0][0]) > tol:
                                            v_new_right = False
                                        elif (d[0][0] * g[1][0] - d[1][0] * g[0][0]) < -tol:
                                            v_new_right = True
                                        else:
                                            v_new_right = v0_right
                                        if v0_right == v_new_right:
                                            if (a + 1) < num_vertex:
                                                temp[0][0] = self.__vertices[0][a + 1]
                                                temp[1][0] = self.__vertices[1][a + 1]
                                                pol = np.append(pol, temp, axis=1)
                                                polNum = np.append(polNum, a + 1)
                                            else:
                                                z = len(straight_order)
                                if flag == 0:
                                    q = v
                                    while q < num_vertex:
                                        g[0][0] = self.__vertices[0][q] - s1[0][0]
                                        g[1][0] = self.__vertices[1][q] - s1[1][0]
                                        if (d[0][0] * g[1][0] - d[1][0] * g[0][0]) > tol:
                                            v_new_right = False
                                        elif (d[0][0] * g[1][0] - d[1][0] * g[0][0]) < -tol:
                                            v_new_right = True
                                        else:
                                            v_new_right = v0_right
                                        if v0_right == v_new_right:
                                            temp[0][0] = self.__vertices[0][q]
                                            temp[1][0] = self.__vertices[1][q]
                                            pol = np.append(pol, temp, axis=1)
                                            polNum = np.append(polNum, q)
                                            v = q + 1
                                            q = num_vertex
                                            b = np.size(self.__intersection_points, axis=1)
                                            summit_just_added = True
                                        q += 1
                            b += 1
                    else:
                        summit_just_added = False
            else:
                if (abs(self.__vertices[0][v] - pol[0][v - 1]) >= tol) or \
                        (abs(self.__vertices[1][v] - pol[1][v - 1]) >= tol):
                    temp[0][0] = self.__vertices[0][v]
                    temp[1][0] = self.__vertices[1][v]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, v)
            if int_found is True:
                pol = np.round(pol, 4)
                pol_0 = pol[0, :]
                pol_1 = pol[1, :]
                pol = list(zip(pol_0, pol_1))
                cut_polygons.insert(len(cut_polygons), pol)
                new_polygons_vertex.insert(len(new_polygons_vertex), list(map(int, polNum)))
                j += 1
                pol = np.array([[0], [0]], float)
                polNum = []
            v += 1
        second_int: int = 0
        pol_finish: bool = False
        vert_straight: bool = False
        i = 0
        rangeI: int = -1
        rangeProv: int = 0
        rangeF: int = 0
        while i < num_intersection:
            temp[0][0] = self.__intersection_points[0][i]
            temp[1][0] = self.__intersection_points[1][i]
            pol = np.append(pol, temp, axis=1)
            z: int = np.size(corresponding_points, axis=1) - 1
            a: int = 0
            while a < z:
                if (abs(corresponding_points[0][a + 1] - self.__intersection_points[0][i]) <= tol) and \
                        (abs(corresponding_points[1][a + 1] - self.__intersection_points[1][i]) <= tol):
                    z = a
                    polNum = np.append(polNum, straight_order[a])
                    rangeI = a
                a += 1
            if rangeI == -1:
                polNum = np.append(polNum, num_vertex)
            v = int(num_l + 1)
            i += 1
            temp[0][0] = self.__vertices[0][v]
            temp[1][0] = self.__vertices[1][v]
            pol = np.append(pol, temp, axis=1)
            polNum = np.append(polNum, v)
            v += 1
            k = int(num_l + 1)
            while k < num_vertex:
                if self.__edge_with_intersection[k] is True:
                    temp[0][0] = self.__intersection_points[0][i]
                    temp[1][0] = self.__intersection_points[1][i]
                    pol = np.append(pol, temp, axis=1)
                    z = np.size(corresponding_points, axis=1) - 1
                    a = 0
                    while a < z:
                        if (abs(corresponding_points[0][a + 1] - self.__intersection_points[0][i]) <= tol) and \
                                (abs(corresponding_points[1][a + 1] - self.__intersection_points[1][i]) <= tol):
                            z = a - 1
                            polNum = np.append(polNum, straight_order[a])
                            rangeF = a
                        a += 1
                    for f in range(0, num_vertex):
                        if (abs(self.__vertices[0][f] - self.__intersection_points[0][i]) <= tol) and \
                                (abs(self.__vertices[1][f] - self.__intersection_points[1][i]) <= tol):
                            vert_straight = True
                    i += 1
                    a: int = z + 1
                    segment = Seg.Segment(self.__s1, self.__s2)
                    direction = Seg.Segment.direction(segment)
                    if v < num_vertex:
                        temp[0][0] = self.__vertices[0][v]
                        temp[1][0] = self.__vertices[1][v]
                        temp2[0][0] = self.__vertices[0][v - 1]
                        temp2[1][0] = self.__vertices[1][v - 1]
                        edge = temp - temp2
                    if (direction[0][0] * edge[0][0] + direction[1][0] * edge[1][0]) > 0 and k + 1 < num_vertex and \
                            vert_straight is False:
                        x = a + 1
                        while x < num_vertex:
                            if self.__edge_with_intersection[a] is True:
                                temp[0][0] = self.__intersection_points[0][i]
                                temp[1][0] = self.__intersection_points[1][i]
                                pol = np.append(pol, temp, axis=1)
                                z = np.size(corresponding_points, axis=1) - 1
                                b = 0
                                while b < z:
                                    if (abs(corresponding_points[0][b + 1] - self.__intersection_points[0][i]) <= tol) \
                                      and (abs(corresponding_points[1][b + 1] -
                                               self.__intersection_points[1][i]) <= tol):
                                        z = b - 1
                                        polNum = np.append(polNum, straight_order[b])
                                    b += 1
                                c = int(whatEdgeInters[i] + 1)
                                while c < num_vertex:
                                    if self.__edge_with_intersection[c] is True:
                                        i += 1
                                        temp[0][0] = self.__intersection_points[0][i]
                                        temp[1][0] = self.__intersection_points[1][i]
                                        pol = np.append(pol, temp, axis=1)
                                        z = np.size(corresponding_points) - 1
                                        a = 0
                                        while a < z:
                                            if (abs(corresponding_points[0][a + 1] - self.__intersection_points[0][i])
                                                <= tol) and (abs(corresponding_points[1][a + 1] -
                                                                 self.__intersection_points[1][i]) <= tol):
                                                z = a - 1
                                                polNum = np.append(polNum, straight_order[a])
                                                rangeProv = a
                                                c = num_vertex
                                            a += 1
                                        for h in range(rangeProv + 1, rangeI):
                                            temp[0][0] = corresponding_points[0][h + 1]
                                            temp[1][0] = corresponding_points[1][h + 1]
                                            pol = np.append(pol, temp, axis=1)
                                            polNum = np.append(polNum, straight_order[h])
                                            pol_finish = True
                                            x = num_vertex
                                    else:
                                        v = c + 1
                                        temp[0][0] = self.__vertices[0][v]
                                        temp[1][0] = self.__vertices[1][v]
                                        pol = np.append(pol, temp, axis=1)
                                        polNum = np.append(polNum, v)
                                    c += 1
                            else:
                                temp[0][0] = self.__vertices[0][x + 1]
                                temp[1][0] = self.__vertices[1][x + 1]
                                pol = np.append(pol, temp, axis=1)
                                polNum = np.append(polNum, x + 1)
                            x += 1
                    else:
                        if rangeF > rangeI:
                            for f in range(rangeF - 1, rangeI, -1):
                                temp[0][0] = corresponding_points[0][f + 1]
                                temp[1][0] = corresponding_points[1][f + 1]
                                pol = np.append(pol, temp, axis=1)
                                polNum = np.append(polNum, straight_order[f])
                                pol_finish = True
                            if rangeF - 1 == rangeI:
                                pol_finish = True
                        else:
                            for f in range(rangeF + 1, rangeI):
                                temp[0][0] = corresponding_points[0][f + 1]
                                temp[1][0] = corresponding_points[1][f + 1]
                                pol = np.append(pol, temp, axis=1)
                                polNum = np.append(polNum, straight_order[f])
                                pol_finish = True
                else:
                    temp[0][0] = self.__vertices[0][v]
                    temp[1][0] = self.__vertices[1][v]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, v)
                    v += 1
                    k += 1
                if pol_finish is True:
                    pol = np.transpose(pol)
                    pol = np.delete(pol, 0, 0)
                    pol = np.transpose(pol)
                    pol = np.round(pol, 4)
                    pol_0 = pol[0, :]
                    pol_1 = pol[1, :]
                    pol = list(zip(pol_0, pol_1))
                    cut_polygons.insert(len(cut_polygons), pol)
                    new_polygons_vertex.insert(len(new_polygons_vertex), list(map(int, polNum)))
                    pol = np.array([[0], [0]], float)
                    polNum = []
                    j += 1
                    how_many_times = []
                    flag = 0
                    a = 0
                    inters = np.round(self.__intersection_points, 4)
                    while a < num_intersection:
                        j = 0
                        len_cut = len(cut_polygons)
                        while j < len_cut:
                            e = 0
                            len_cut_j = len(cut_polygons[j])
                            while e < len_cut_j:
                                if (abs(cut_polygons[j][e][0] - inters[0][a]) <= tol) and \
                                        (abs(cut_polygons[j][e][1] - inters[1][a]) <= tol):
                                    flag += 1
                                    e = len(cut_polygons[j])
                                e += 1
                            j += 1
                        how_many_times = np.append(how_many_times, flag)
                        flag = 0
                        a += 1
                    for x in range(0, num_intersection):
                        if (abs(self.__intersection_points[0][x] - corresponding_points[0][rangeF + 1]) <= tol) and \
                                (abs(self.__intersection_points[1][x] - corresponding_points[1][rangeF + 1]) <= tol):
                            second_int = x
                    if how_many_times[second_int] < 2:
                        i = second_int
                    else:
                        finish: bool = True
                        for h in range(0, len(how_many_times)):
                            if how_many_times[h] < 2:
                                finish = False
                        if finish is True:
                            i = num_intersection
                        elif vert_straight is True:
                            i = second_int
                        else:
                            i = second_int + 1
                    if i < num_intersection:
                        num_l = whatEdgeInters[i]
                    if vert_straight is True:
                        num_l += 1
                    k = num_vertex + 1
                    pol_finish = False
                    vert_straight = False
        return cut_polygons, new_polygons_vertex

    @staticmethod
    def return_new_polygons(cut_polygons, new_polygons_vertex):
        new_polygons_vertex = str(new_polygons_vertex)
        result = "The coordinates of the vertices of the new polygons are:\n" + str(cut_polygons) + \
                 "\nThe numerical order is:\n" + str(new_polygons_vertex)
        return result
