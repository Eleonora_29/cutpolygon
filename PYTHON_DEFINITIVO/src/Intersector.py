import numpy as np
import src.Vector2D as V2d
tolerance_parallelism = 1e-7
tolerance_intersection = 1e-7


class Intersector:
    def __init__(self):
        self.__matrix_tangent_vector = np.array([[0., 0.], [0., 0.]], float)

    def set_straight(self, origin_l1, end_l1):
        origin_l1 = V2d.Vector2d.into_matrix(origin_l1)
        end_l1 = V2d.Vector2d.into_matrix(end_l1)
        self.__matrix_tangent_vector[0][0] = end_l1[0][0] - origin_l1[0][0]
        self.__matrix_tangent_vector[1][0] = end_l1[1][0] - origin_l1[1][0]

    def set_edge(self, origin_l2, end_l2):
        origin_l2 = V2d.Vector2d.into_matrix(origin_l2)
        end_l2 = V2d.Vector2d.into_matrix(end_l2)
        self.__matrix_tangent_vector[0][1] = origin_l2[0][0] - end_l2[0][0]
        self.__matrix_tangent_vector[1][1] = origin_l2[1][0] - end_l2[1][0]

    def intersection(self, origin_l1, origin_l2):
        origin_l1 = V2d.Vector2d.into_matrix(origin_l1)
        origin_l2 = V2d.Vector2d.into_matrix(origin_l2)
        parametric_coordinates = np.array([[0.], [0.]], float)
        right_hand_side = np.array([[0.], [0.]], float)
        right_hand_side[0][0] = origin_l2[0][0] - origin_l1[0][0]
        right_hand_side[1][0] = origin_l2[1][0] - origin_l1[1][0]
        det = np.linalg.det(self.__matrix_tangent_vector)
        intersection: bool = False
        check = tolerance_parallelism * tolerance_parallelism * np.linalg.norm(self.__matrix_tangent_vector[:, 0]) * \
            np.linalg.norm(self.__matrix_tangent_vector[:, 1])
        if det * det >= check:
            solve_matrix = np.linalg.inv(self.__matrix_tangent_vector)
            parametric_coordinates = (np.dot(solve_matrix, right_hand_side))
            if parametric_coordinates[1][0] - 1.0 < tolerance_intersection and\
                    parametric_coordinates[1][0] > -tolerance_intersection:
                intersection = True
            else:
                intersection = False
        return parametric_coordinates, intersection

    def intersection_coordinates(self, origin_l1, parametric_coord):
        origin_l1 = V2d.Vector2d.into_matrix(origin_l1)
        intersection_coordinates = np.array([[0.], [0.]], float)
        intersection_coordinates[0][0] = origin_l1[0][0] + self.__matrix_tangent_vector[0][0] * parametric_coord[0]
        intersection_coordinates[1][0] = origin_l1[1][0] + self.__matrix_tangent_vector[1][0] * parametric_coord[0]
        return intersection_coordinates
