from abc import ABC
import src.FactoryMethod as Fm
import src.NewPoints as Np
import src.CutPolygons as Pol


class Result(Fm.FactoryMethod, ABC):
    def __init__(self, vertices, s1, s2):
        self.__vertices = vertices
        self.__s1 = s1
        self.__s2 = s2
        self.__product: list = []

    def add_new_points(self) -> None:
        pol = Pol.CutPolygon(self.__vertices, self.__s1, self.__s2)
        intersection_points, num_intersection = Pol.CutPolygon.cut(pol)
        new_points = Np.NewPoints(self.__vertices, intersection_points, self.__s1, self.__s2)
        new_points = Np.NewPoints.calc_points(new_points)
        new_points = Np.NewPoints.return_points(new_points)
        self.__product = new_points

    def add_new_polygons(self) -> None:
        pol = Pol.CutPolygon(self.__vertices, self.__s1, self.__s2)
        Pol.CutPolygon.cut(pol)
        cut_polygons, new_polygons_vertex = Pol.CutPolygon.choose_function(pol)
        new_polygons = Pol.CutPolygon.return_new_polygons(cut_polygons, new_polygons_vertex)
        new_polygons = "\n" + new_polygons
        self.__product = self.__product + new_polygons

    def return_product(self):
        return str(self.__product)
