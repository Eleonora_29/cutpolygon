import numpy as np
import math as math
import src.Vector2D as V2d


class Segment:
    def __init__(self, s1, s2):
        self.__s1 = V2d.Vector2d.into_matrix(s1)
        self.__s2 = V2d.Vector2d.into_matrix(s2)

    def to_matrix(self):
        return np.concatenate((self.__s1, self.__s2), axis=1)

    def distance(self):
        return math.sqrt(math.pow((self.__s1[0][0] - self.__s2[0][0]), 2) +
                         math.pow((self.__s1[1][0] - self.__s2[1][0]), 2))

    def direction(self):
        return self.__s2 - self.__s1
