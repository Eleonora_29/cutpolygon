from unittest import TestCase
import src.Vector2D as V2d
import src.Polygon as Pol
import numpy as np


class TestPolygon(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(1.0000e+00, 2.0000e+00),
                    V2d.Vector2d(7.6489e-01, 1.3236e+00),
                    V2d.Vector2d(4.8943e-02, 1.3090e+00),
                    V2d.Vector2d(6.1958e-01, 8.7639e-01),
                    V2d.Vector2d(4.1221e-01, 1.9098e-01),
                    V2d.Vector2d(1.0000e+00, 6.0000e-01),
                    V2d.Vector2d(1.5878e+00, 1.9098e-01),
                    V2d.Vector2d(1.3804e+00, 8.7639e-01),
                    V2d.Vector2d(1.9511e+00, 1.3090e+00),
                    V2d.Vector2d(1.2351e+00, 1.3236e+00)]
        return vertices

    def test_create_polygon(self):
        vertices = TestPolygon.FillPolygonVertices()
        vertices_matrix = np.array([[1., 0.76489, 0.048943, 0.61958,  0.41221,  1.,       1.5878,   1.3804, 1.9511,  1.2351  ],
                                    [2.,       1.3236,   1.309,    0.87639,  0.19098,  0.6,      0.19098,  0.87639, 1.309,    1.3236]])
        try:
            self.assertEqual(str(Pol.Polygon.create_polygon(vertices)), str(vertices_matrix))
        except:
            self.fail()

    def test_create_polygon_fail(self):
        vertices_matrix = np.array([[1., 0.76489, 0.048943, 0.61958,  0.41221,  1.,       1.5878,   1.3804, 1.9511,  1.2351  ],
                                    [2.,       1.3236,   1.309,    0.87639,  0.19098,  0.6,      0.19098,  0.87639, 1.309,    1.3236]])
        try:
            self.assertEqual(str(Pol.Polygon.create_polygon(vertices_matrix)), str(vertices_matrix))
        except:
            self.fail()
