from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestConvex(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(2.4, 1.1),
                    V2d.Vector2d(5.8, 2.6),
                    V2d.Vector2d(3.3, 6.8),
                    V2d.Vector2d(1.7, 6)]
        return vertices

    def test_Polygon_Convex(self):
        try:
            vertices = TestConvex.FillPolygonVertices()
            s1 = V2d.Vector2d(2.8, 2.0)
            s2 = V2d.Vector2d(4.4, 3.9)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(2.4, 1.1), (5.8, 2.6), (3.3, 6.8), (1.7, 6.0), (2.8, 2.0), (4.4, 3.9), "
                             "(4.7669, 4.3357), (2.3481, 1.4634)]\nThe number of new points is:\n8\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(2.4, 1.1), (5.8, 2.6), (4.7669, 4.3357), (4.4, 3.9), (2.8, 2.0), (2.3481, 1.4634)], "
                             "[(4.7669, 4.3357), (3.3, 6.8), (1.7, 6.0), (2.3481, 1.4634), (2.8, 2.0), (4.4, 3.9)])"
                             "\nThe numerical order is:\n([0, 1, 4, 5, 6, 7], [4, 2, 3, 7, 6, 5])")
        except:
            self.fail()
