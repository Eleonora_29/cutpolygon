import numpy as np


class Vector2d:
    def __init__(self, x: float, y: float):  # Costruttore
        self.x = x
        self.y = y
        self.components = []

    def InMatrix(self):
        self.components = np.zeros((2, 1), float)  # Vettore colonna
        self.components[0][0] = self.x
        self.components[1][0] = self.y
        return self.components

    def X(self):  # Metodo per le X
        return self.x

    def Y(self):  # Metodo per le Y
        return self.y


'''
Classe Vettore a due dimensioni -> FUNZIONA
Descrizione generale. 
    In questa classe si formano i Vector2d che sono elementi con x e y.
    Si forma una matrice colonna di due righe e una colonna
1. Metodo costruttore: prende in input x e y e le assegna
2. Metodo InMatrix: prende x e y e le mette in una matrice colonna
2. Metodo X: resituisce il valore dell'ascissa
3. Metodo Y: restituisce il valore dell'ordinata
'''
