from abc import ABC


class Builder(ABC):
    def produce_new_points(self) -> None:
        pass

    def produce_new_polygons(self) -> None:
        pass

