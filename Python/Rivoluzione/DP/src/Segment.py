import numpy as np
import math as math
import src.Vector2D as V2d


class Segment:
    def __init__(self, s1, s2):
        s1 = V2d.Vector2d.into_matrix(s1)
        s2 = V2d.Vector2d.into_matrix(s2)
        self.s1 = s1
        self.s2 = s2

    def to_matrix(self):
        return np.concatenate((self.s1, self.s2), axis=1)

    def distance(self):
        return math.sqrt(math.pow((self.s1[0][0] - self.s2[0][0]), 2) + math.pow((self.s1[1][0] - self.s2[1][0]), 2))

    def direction(self):
        return self.s2 - self.s1
