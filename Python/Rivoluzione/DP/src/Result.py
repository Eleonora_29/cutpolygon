from abc import ABC
import src.Builder as Builder
import src.NewPoints as Np
import src.CutPolygons as Pol
import src.Segment as Seg


class Result(Builder.Builder, ABC):
    def __init__(self, vertices, s1, s2):
        self.vertices = vertices
        self.s1 = s1
        self.s2 = s2
        self._product: list = []

    def add_new_points(self) -> None:
        pol = Pol.CutPolygon(self.vertices, self.s1, self.s2)
        intersection_points, num_intersection = Pol.CutPolygon.cut(pol)
        segment = Seg.Segment(self.s1, self.s2)
        segment = Seg.Segment.to_matrix(segment)
        new_points = Np.NewPoints(self.vertices, intersection_points, self.s1, self.s2)
        new_points = Np.NewPoints.calc_points(new_points)
        new_points = Np.NewPoints.return_points(new_points)
        self._product = new_points

    def add_new_polygons(self) -> None:
        pol = Pol.CutPolygon(self.vertices, self.s1, self.s2)
        intersection_points, num_intersection = Pol.CutPolygon.cut(pol)
        cut_polygons, new_polygons_vertex = Pol.CutPolygon.choose_function(pol)
        new_polygons = Pol.CutPolygon.return_new_polygons(cut_polygons, new_polygons_vertex)
        new_polygons = "\n" + new_polygons
        self._product = self._product + new_polygons

    def return_product(self):
        return str(self._product)
