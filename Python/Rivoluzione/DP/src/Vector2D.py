import numpy as np


class Vector2d:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def x(self):
        return self.x

    def y(self):
        return self.y

    def into_matrix(self):
        vector = np.array([[0.], [0.]], float)
        vector[0][0] = self.x
        vector[1][0] = self.y
        return vector
