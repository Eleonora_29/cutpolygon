import src.Vector2D as V2d
import numpy as np


class Polygon:
    @staticmethod
    def create_polygon(vertices):
        if str(type(vertices)) == "<class 'numpy.ndarray'>":
            return vertices
        else:
            polygonVertices = []
            num_vertices = len(vertices)
            for v in range(0, num_vertices):
                vertex: V2d = vertices[v]
                polygonVertices.append(vertex)
            vertices_matrix = np.zeros((2, num_vertices), float)
            v = 0
            while v < num_vertices:
                for vector in polygonVertices:
                    vertices_matrix[0][v] = V2d.Vector2d.x(vector)
                    vertices_matrix[1][v] = V2d.Vector2d.y(vector)
                    v += 1
            return vertices_matrix
