import numpy as np
import src.Segment as Seg
import src.Polygon as Pol
tol = 1e-7


class NewPoints:
    def __init__(self, vertices, intersection_points, s1, s2):
        self.vertices = vertices
        self.intersection_points = intersection_points
        self.s1 = s1
        self.s2 = s2
        segment = Seg.Segment(s1, s2)
        self.segment = Seg.Segment.to_matrix(segment)

    def ordered_vertex(self):
        vertices_in_matrix = Pol.Polygon.create_polygon(self.vertices)
        ordered_vertex = np.array([[0.], [0.]], float)
        primoIndex: int = 0
        minX: float = vertices_in_matrix[0][0]
        minY: float = vertices_in_matrix[1][0]
        flagX: int = 0
        flagY: int = 0
        to_order: bool = False
        temp = np.array([[0.], [0.]], float)
        for v in range(1, np.size(vertices_in_matrix, axis=1)):
            if vertices_in_matrix[0][v] <= minX:
                flagX = v
            if vertices_in_matrix[1][v] <= minY:
                flagY = v
            if flagX != 0 and flagY != 0:
                minX = vertices_in_matrix[0][v]
                minY = vertices_in_matrix[1][v]
                primoIndex = v
                flagX = 0
                flagY = 0
                to_order = True
            else:
                flagX = 0
                flagY = 0
        if to_order is True:
            for a in range(primoIndex, np.size(vertices_in_matrix, axis=1)):
                temp[0][0] = vertices_in_matrix[0][a]
                temp[1][0] = vertices_in_matrix[1][a]
                ordered_vertex = np.append(ordered_vertex, temp, axis=1)
            for b in range(0, primoIndex):
                temp[0][0] = vertices_in_matrix[0][b]
                temp[1][0] = vertices_in_matrix[1][b]
                ordered_vertex = np.append(ordered_vertex, temp, axis=1)
        else:
            ordered_vertex = vertices_in_matrix
        if np.size(ordered_vertex, axis=1) > np.size(vertices_in_matrix, axis=1):
            ordered_vertex = np.transpose(ordered_vertex)
            ordered_vertex = np.delete(ordered_vertex, 0, 0)
            ordered_vertex = np.transpose(ordered_vertex)
        return ordered_vertex

    def calc_points(self):
        ordered = NewPoints(self.vertices, self.intersection_points, self.s1, self.s2)
        self.vertices = NewPoints.ordered_vertex(ordered)
        intersection_segment = np.array([[0.], [0.]], float)
        num_intersections = np.size(self.intersection_points, axis=1)
        temp = np.array([[0.], [0.]], float)
        for i in range(0, 2):
            temp[0][0] = self.segment[0][i]
            temp[1][0] = self.segment[1][i]
            intersection_segment = np.append(intersection_segment, temp, axis=1)
        intersection_segment = np.transpose(intersection_segment)
        intersection_segment = np.delete(intersection_segment, 0, 0)
        intersection_segment = np.transpose(intersection_segment)

        num_vertex = np.size(self.vertices, axis=1)
        position = 0
        for j in range(0, num_intersections):
            flag = 0
            for i in range(0, 2):
                if (abs(self.intersection_points[0][j] - intersection_segment[0][i]) <= tol) \
                        and (abs(self.intersection_points[1][j] - intersection_segment[1][i]) <= tol):
                    flag += 1
            if flag == 0:
                temp[0][0] = self.intersection_points[0][j]
                temp[1][0] = self.intersection_points[1][j]
                intersection_segment = np.append(intersection_segment, temp, axis=1)
                position += 1

        new_points = np.array([[0.], [0.]], float)
        for i in range(0, num_vertex):
            temp = np.array([[0.], [0.]], float)
            temp[0][0] = self.vertices[0][i]
            temp[1][0] = self.vertices[1][i]
            new_points = np.append(new_points, temp, axis=1)
        new_points = np.transpose(new_points)
        new_points = np.delete(new_points, 0, 0)
        new_points = np.transpose(new_points)

        position = 0
        num_int_seg = np.size(intersection_segment, axis=1)
        for j in range(0, num_int_seg):
            flag = 0
            for i in range(0, num_vertex):
                if (abs(intersection_segment[0][j] - self.vertices[0][i]) <= tol) \
                        and (abs(intersection_segment[1][j] - self.vertices[1][i]) <= tol):
                    flag += 1
            if flag == 0:
                temp[0][0] = intersection_segment[0][j]
                temp[1][0] = intersection_segment[1][j]
                new_points = np.append(new_points, temp, axis=1)
                position += 1

        new_points = np.round(new_points, 4)
        new_points_0 = new_points[0, :]
        new_points_1 = new_points[1, :]
        new_points = list(zip(new_points_0, new_points_1))
        return new_points

    @staticmethod
    def return_points(new_points):
        result = "The set of vertices, endpoints of the segment and intersection points is:\n" + str(new_points) +\
                 "\nThe number of new points is:\n" + str(int(np.size(new_points)/2))
        return result
