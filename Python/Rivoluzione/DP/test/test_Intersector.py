from unittest import TestCase
import src.Intersector as Int
import src.Vector2D as V2d


class TestIntersection(TestCase):
    def test_intersection(self):
        matrix_tangent_vector = Int.Intersector()
        try:
            origin_l1 = V2d.Vector2d(3, 0)
            end_l1 = V2d.Vector2d(3, 5)
            self.assertEqual(str(Int.Intersector.set_straight(matrix_tangent_vector, origin_l1, end_l1)), "None")
        except:
            self.fail()

        try:
            origin_l2 = V2d.Vector2d(1, 1)
            end_l2 = V2d.Vector2d(5, 3)
            self.assertEqual(str(Int.Intersector.set_edge(matrix_tangent_vector, origin_l2, end_l2)), "None")
        except:
            self.fail()

        try:
            resultParametricCoordinates, intersection = \
                Int.Intersector.intersection(matrix_tangent_vector, origin_l1, origin_l2)
            self.assertEqual(str(str(resultParametricCoordinates) + "\n " + str(intersection)), "[[0.4]\n [0.5]]\n True")
        except:
            self.fail()

        try:
            self.assertEqual(str(Int.Intersector.intersection_coordinates(matrix_tangent_vector, origin_l1,
                                                                          resultParametricCoordinates)), "[[3.]\n [2.]]")
        except:
            self.fail()
