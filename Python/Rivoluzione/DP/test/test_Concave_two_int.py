from unittest import TestCase
import src.NewPoints as Np
import src.CutPolygons as Pol
import src.Result as Result
import src.Vector2D as V2d


class TestConcaveTwoInt(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(3.5, 1),
                    V2d.Vector2d(6.7, 4.2),
                    V2d.Vector2d(5.3, 6.8),
                    V2d.Vector2d(3.7, 6),
                    V2d.Vector2d(2.2, 8.5),
                    V2d.Vector2d(1, 5.6)]
        return vertices

    def test_Polygon(self):
        try:
            vertices = TestConcaveTwoInt.FillPolygonVertices()
            s1 = V2d.Vector2d(2, 6.5)
            s2 = V2d.Vector2d(5.1, 3.9)
            pol = Pol.CutPolygon(vertices, s1, s2)
            intersection_points, num_intersection = Pol.CutPolygon.cut(pol)
            self.assertEqual(str(num_intersection), "2")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.CutPolygon.choose_function(pol)
            self.assertEqual(Pol.CutPolygon.return_new_polygons(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(3.5, 1.0), (5.807, 3.307), (5.1, 3.9), (2.0, 6.5), (1.5341, 6.8908), (1.0, 5.6)], "
                             "[(5.807, 3.307), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.5341, 6.8908), "
                             "(2.0, 6.5), (5.1, 3.9)])"
                             "\nThe numerical order is:\n([0, 6, 7, 8, 9, 5], [6, 1, 2, 3, 4, 9, 8, 7])")
        except:
            self.fail()

        try:
            new_points = Np.NewPoints(vertices, intersection_points, s1, s2)
            new_points = Np.NewPoints.calc_points(new_points)
            self.assertEqual(str(Np.NewPoints.return_points(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(3.5, 1.0), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.0, 5.6), "
                             "(2.0, 6.5), (5.1, 3.9), (5.807, 3.307), (1.5341, 6.8908)]"
                             "\nThe number of new points is:\n10")
        except:
            self.fail()

    def test_Polygon_ConC2int(self):
        vertices = TestConcaveTwoInt.FillPolygonVertices()
        s1 = V2d.Vector2d(2, 6.5)
        s2 = V2d.Vector2d(5.1, 3.9)
        product = Result.Result(vertices, s1, s2)
        Result.Result.add_new_points(product)
        Result.Result.add_new_polygons(product)
        result = Result.Result.return_product(product)
        self.assertEqual(str(result),
                         "The set of vertices, endpoints of the segment and intersection points is:\n"
                         "[(3.5, 1.0), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.0, 5.6), "
                         "(2.0, 6.5), (5.1, 3.9), (5.807, 3.307), (1.5341, 6.8908)]"
                         "\nThe number of new points is:\n10\n"
                         "The coordinates of the vertices of the new polygons are:\n"
                         "([(3.5, 1.0), (5.807, 3.307), (5.1, 3.9), (2.0, 6.5), (1.5341, 6.8908), (1.0, 5.6)], "
                         "[(5.807, 3.307), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.5341, 6.8908), "
                         "(2.0, 6.5), (5.1, 3.9)])"
                         "\nThe numerical order is:\n([0, 6, 7, 8, 9, 5], [6, 1, 2, 3, 4, 9, 8, 7])"
                         )
