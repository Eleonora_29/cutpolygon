from unittest import TestCase
import src.NewPoints as Np
import src.CutPolygons as Pol
import src.Result as Result
import src.Vector2D as V2d


class TestPentagon(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(2.5, 1),
                    V2d.Vector2d(4, 2.1),
                    V2d.Vector2d(3.4, 4.2),
                    V2d.Vector2d(1.6, 4.2),
                    V2d.Vector2d(1, 2.1)]
        return vertices

    def test_Polygon(self):
        try:
            vertices = TestPentagon.FillPolygonVertices()
            s1 = V2d.Vector2d(1.4, 2.75)
            s2 = V2d.Vector2d(3.6, 2.2)
            pol = Pol.CutPolygon(vertices, s1, s2)
            intersection_points, num_intersection = Pol.CutPolygon.cut(pol)
            self.assertEqual(str(num_intersection), "2")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.CutPolygon.choose_function(pol)
            self.assertEqual(Pol.CutPolygon.return_new_polygons(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(2.5, 1.0), (4.0, 2.1), (3.6, 2.2), (1.4, 2.75), (1.2, 2.8), (1.0, 2.1)], [(4.0, 2.1), "
                             "(3.4, 4.2), (1.6, 4.2), (1.2, 2.8), (1.4, 2.75), (3.6, 2.2)])"
                             "\nThe numerical order is:\n([0, 1, 5, 6, 7, 4],"
                             " [1, 2, 3, 7, 6, 5])")
        except:
            self.fail()

        try:
            new_points = Np.NewPoints(vertices, intersection_points, s1, s2)
            new_points = Np.NewPoints.calc_points(new_points)
            self.assertEqual(str(Np.NewPoints.return_points(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(2.5, 1.0), (4.0, 2.1), (3.4, 4.2), (1.6, 4.2), (1.0, 2.1), (1.4, 2.75), (3.6, 2.2), "
                             "(1.2, 2.8)]\nThe number of new points is:\n8")
        except:
            self.fail()

    def test_Polygon_Pent(self):
        vertices = TestPentagon.FillPolygonVertices()
        s1 = V2d.Vector2d(1.4, 2.75)
        s2 = V2d.Vector2d(3.6, 2.2)
        product = Result.Result(vertices, s1, s2)
        Result.Result.add_new_points(product)
        Result.Result.add_new_polygons(product)
        result = Result.Result.return_product(product)
        self.assertEqual(str(result),
                         "The set of vertices, endpoints of the segment and intersection points is:\n"
                         "[(2.5, 1.0), (4.0, 2.1), (3.4, 4.2), (1.6, 4.2), (1.0, 2.1), (1.4, 2.75), (3.6, 2.2), "
                         "(1.2, 2.8)]\nThe number of new points is:\n8\n"
                         "The coordinates of the vertices of the new polygons are:\n"
                         "([(2.5, 1.0), (4.0, 2.1), (3.6, 2.2), (1.4, 2.75), (1.2, 2.8), (1.0, 2.1)], [(4.0, 2.1), "
                         "(3.4, 4.2), (1.6, 4.2), (1.2, 2.8), (1.4, 2.75), (3.6, 2.2)])"
                         "\nThe numerical order is:\n([0, 1, 5, 6, 7, 4],"
                         " [1, 2, 3, 7, 6, 5])"
                         )
