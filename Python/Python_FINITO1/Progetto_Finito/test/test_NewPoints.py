from unittest import TestCase
import numpy as np
import src.NewPoints as Np
import src.Segment as Seg


class TestPoint(TestCase):
    def test_new_points(self):
        try:
            vertices = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
            intersection_points = np.array([[1.77777777778, 4.11111111111], [1., 3.1]])
            s1 = np.array([[2.], [1.2]])
            s2 = np.array([[4.], [3.]])
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(Np.NewPoints.new_points(new_points)),
                             "[(1.0, 1.0), (5.0, 1.0), (5.0, 3.1), (1.0, 3.1), (2.0, 1.2), (4.0, 3.0), (1.7778, 1.0),"
                             " (4.1111, 3.1)]")
        except:
            self.fail()

    def test_result(self):
        try:
            vertices = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
            intersection_points = np.array([[1.77777777778, 4.11111111111], [1., 3.1]])
            s1 = np.array([[2.], [1.2]])
            s2 = np.array([[4.], [3.]])
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(Np.NewPoints.result(new_points),
                             "The set of vertices, endpoints of the segment and intersection points is:\n[(1.0, 1.0), "
                             "(5.0, 1.0), (5.0, 3.1), (1.0, 3.1), (2.0, 1.2), (4.0, 3.0), "
                             "(1.7778, 1.0), (4.1111, 3.1)]")
        except:
            self.fail()

    def test_order_vertex(self):
        try:
            vertices = np.array([[2., 0., 3., 0., 3., 3., -1., -3., 0., -3.],
                                 [-2., -1., 1., 2., 2., 3., 3., 1., 0., -2.]])
            s1 = np.array([[-1], [-1]])
            s2 = np.array([[1], [1]])
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            intersection_points = np.array([[1.77777777778, 4.11111111111], [1., 3.1]])
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(Np.NewPoints.ordered_vertex(new_points)),"[[-3.  2.  0.  3.  0.  3.  3. -1. -3.  0.]\n [-2. -2. -1.  1.  2.  2.  3.  3.  1.  0.]]")
        except:
            self.fail()