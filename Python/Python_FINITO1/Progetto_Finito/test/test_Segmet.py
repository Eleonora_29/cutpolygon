from unittest import TestCase
import src.Segment as Seg
import numpy as np


class TestSegment(TestCase):
    def test_to_matrix(self):
        try:
            s1 = np.array([[1], [2]])
            s2 = np.array([[5], [9]])
            segment = Seg.Segment(s1, s2)
            self.assertEqual(str(Seg.Segment.to_matrix(segment)), "[[1 5]\n [2 9]]")
        except:
            self.fail()

    def test_distance(self):
        try:
            s1 = np.array([[1], [2]])
            s2 = np.array([[2], [3]])
            segment = Seg.Segment(s1, s2)
            self.assertTrue(abs(Seg.Segment.distance(segment) - 1.414213562) < 1e-6)
        except:
            self.fail()

    def test_angular_coefficient(self):
        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[0], [2]])
            segment = Seg.Segment(s1, s2)
            self.assertTrue(abs(Seg.Segment.angular_coefficient(segment) - 1) < 1e-6)
        except:
            self.fail()

        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[2], [2]])
            segment = Seg.Segment(s1, s2)
            self.assertEqual(Seg.Segment.angular_coefficient(segment), 'Non Define')

        except:
            self.fail()

        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[0], [4]])
            segment = Seg.Segment(s1, s2)
            self.assertTrue(abs(Seg.Segment.angular_coefficient(segment) - 0) < 1e-6)
        except:
            self.fail()

        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[2], [4]])
            segment = Seg.Segment(s1, s2)
            self.assertEqual(Seg.Segment.angular_coefficient(segment), 'The two past points coincide')
        except:
            self.fail()

    def test_direction(self):
        try:
            s1 = np.array([[2], [4]])
            s2 = np.array([[0], [4]])
            segment = Seg.Segment(s1, s2)
            self.assertTrue(any(abs(Seg.Segment.direction(segment) - [[-2], [0]])) < 1e-6)
        except:
            self.fail()
