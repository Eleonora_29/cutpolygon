from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestRectangle(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
            s1 = np.array([[2], [1.2]])
            s2 = np.array([[4], [3]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "2")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(1.0, 1.0), (1.7778, 1.0), (2.0, 1.2), (4.0, 3.0), (4.1111, 3.1), (1.0, 3.1)], "
                             "[(1.7778, 1.0), (5.0, 1.0), (5.0, 3.1), (4.1111, 3.1), (4.0, 3.0), (2.0, 1.2)])"
                             "\nThe numerical order is:\n([0, 4, 5, 6, 7, 3], [4, 1, 2, 7, 6, 5])")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n[(1.0, 1.0), "
                             "(5.0, 1.0), (5.0, 3.1), (1.0, 3.1), (2.0, 1.2), (4.0, 3.0), (1.7778, 1.0), "
                             "(4.1111, 3.1)]")
        except:
            self.fail()
