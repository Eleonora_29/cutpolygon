from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestConcaveTwoInt(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[3.5, 6.7, 5.3, 3.7, 2.2, 1.], [1., 4.2, 6.8, 6., 8.5, 5.6]])
            s1 = np.array([[2.], [6.5]])
            s2 = np.array([[5.1], [3.9]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "2")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(3.5, 1.0), (5.807, 3.307), (5.1, 3.9), (2.0, 6.5), (1.5341, 6.8908), (1.0, 5.6)], "
                             "[(5.807, 3.307), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.5341, 6.8908), "
                             "(2.0, 6.5), (5.1, 3.9)])"
                             "\nThe numerical order is:\n([0, 6, 7, 8, 9, 5], [6, 1, 2, 3, 4, 9, 8, 7])")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(3.5, 1.0), (6.7, 4.2), (5.3, 6.8), (3.7, 6.0), (2.2, 8.5), (1.0, 5.6), "
                             "(2.0, 6.5), (5.1, 3.9), (5.807, 3.307), (1.5341, 6.8908)]")
        except:
            self.fail()
