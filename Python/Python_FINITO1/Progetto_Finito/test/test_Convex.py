from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestConvex(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[2.4, 5.8, 3.3, 1.7], [1.1, 2.6, 6.8, 6.]])
            s1 = np.array([[2.8], [2.0]])
            s2 = np.array([[4.4], [3.9]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "2")
        except:
           self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(2.4, 1.1), (5.8, 2.6), (4.7669, 4.3357), (4.4, 3.9), (2.8, 2.0), (2.3481, 1.4634)], "
                             "[(4.7669, 4.3357), (3.3, 6.8), (1.7, 6.0), (2.3481, 1.4634), (2.8, 2.0), (4.4, 3.9)])"
                             "\nThe numerical order is:\n([0, 1, 4, 5, 6, 7], [4, 2, 3, 7, 6, 5])")

        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(2.4, 1.1), (5.8, 2.6), (3.3, 6.8), (1.7, 6.0), (2.8, 2.0), (4.4, 3.9), "
                             "(4.7669, 4.3357), (2.3481, 1.4634)]")
        except:
            self.fail()
