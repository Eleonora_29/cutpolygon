from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestConcavePolygon1(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[1.5, 5.6, 5.5, 4., 3.2, 1.], [1., 1.5, 4.8, 6.2, 4.2, 4.]])
            s1 = np.array([[2], [3.7]])
            s2 = np.array([[4.1], [5.9]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "4")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "[[(1.5, 1.0), (5.6, 1.5), (5.5, 4.8), (4.2043, 6.0093), (4.1, 5.9), (3.7213, 5.5033), "
                             "(3.2, 4.2), (2.4086, 4.1281), (2.0, 3.7), (1.1912, 2.8527)], [(4.2043, 6.0093), "
                             "(4.0, 6.2), (3.7213, 5.5033), (4.1, 5.9)], [(2.4086, 4.1281), (1.0, 4.0), "
                             "(1.1912, 2.8527), (2.0, 3.7)]]"
                             "\nThe numerical order is:\n[[0, 1, 2, 6, 7, 8, 4, 9, 10, 11], "
                             "[6, 3, 8, 7], [9, 5, 11, 10]]")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(1.5, 1.0), (5.6, 1.5), (5.5, 4.8), (4.0, 6.2), (3.2, 4.2), (1.0, 4.0), "
                             "(2.0, 3.7), (4.1, 5.9), (4.2043, 6.0093), (3.7213, 5.5033), (2.4086, 4.1281), "
                             "(1.1912, 2.8527)]")
        except:
            self.fail()
