from unittest import TestCase
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg
import numpy as np


class TestConcavePolygon3(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[1., 4.5, 5., 3.5, 3., 2., 0.5], [1., 0.5, 4.5, 3.5, 5., 3.5, 5.5]])
            s1 = np.array([[1.1], [3.8]])
            s2 = np.array([[2.9], [3.9]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            self.assertEqual(str(num_intersection), "6")
        except:
            self.fail()

        try:
            cut_polygons, new_polygons_vertex = Pol.NewPolygons.choose_function(pol, num_intersection)
            self.assertEqual(Pol.NewPolygons.result(cut_polygons, new_polygons_vertex),
                             "The coordinates of the vertices of the new polygons are:\n"
                             "[[(1.0, 1.0), (4.5, 0.5), (4.9392, 4.0133), (4.2091, 3.9727), (3.5, 3.5), "
                             "(3.3582, 3.9255), (2.9, 3.9), (2.2423, 3.8635), (2.0, 3.5), (1.748, 3.836), (1.1, 3.8), "
                             "(0.6914, 3.7773)], [(4.9392, 4.0133), (5.0, 4.5), (4.2091, 3.9727)], [(3.3582, 3.9255), "
                             "(3.0, 5.0), (2.2423, 3.8635), (2.9, 3.9)], [(1.748, 3.836), (0.5, 5.5), (0.6914, 3.7773),"
                             " (1.1, 3.8)]]"
                             "\nThe numerical order is:\n[[0, 1, 7, 8, 3, 9, 10, 11, 5, 12, 13, 14], [7, 2, 8], "
                             "[9, 4, 11, 10], [12, 6, 14, 13]]")
        except:
            self.fail()


        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.to_matrix(segment)
            new_points = Np.NewPoints(vertices, intersection_points, segment)
            new_points = Np.NewPoints.new_points(new_points)
            self.assertEqual(str(Np.NewPoints.result(new_points)),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(1.0, 1.0), (4.5, 0.5), (5.0, 4.5), (3.5, 3.5), (3.0, 5.0), (2.0, 3.5), (0.5, 5.5),"
                             " (1.1, 3.8), (2.9, 3.9), (4.9392, 4.0133), (4.2091, 3.9727), (3.3582, 3.9255),"
                             " (2.2423, 3.8635), (1.748, 3.836), (0.6914, 3.7773)]")
        except:
            self.fail()
