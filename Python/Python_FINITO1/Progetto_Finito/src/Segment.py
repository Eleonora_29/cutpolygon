import numpy as np
import math as math


class Segment:
    def __init__(self, s1, s2):
        self.s1 = s1
        self.s2 = s2

    def to_matrix(self):
        return np.concatenate((self.s1, self.s2), axis=1)

    def distance(self):
        return math.sqrt(math.pow((self.s1[0][0] - self.s2[0][0]), 2) + math.pow((self.s1[1][0] - self.s2[1][0]), 2))

    def angular_coefficient(self):
        tol = 1e-7
        m_y = self.s2[1][0] - self.s1[1][0]
        m_x = self.s2[0][0] - self.s1[0][0]
        if (-tol <= m_x <= tol) and (m_y <= -tol or m_y >= tol):
            return 'Non Define'
        elif (-tol <= m_y <= tol) and (m_x <= -tol or m_x >= tol):
            return 0
        elif (-tol <= m_x <= tol) and (-tol <= m_y <= tol):
            return 'The two past points coincide'
        else:
            return m_y / m_x

    def direction(self):
        return self.s2 - self.s1
