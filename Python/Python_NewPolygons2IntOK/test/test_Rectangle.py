from unittest import TestCase
import numpy as np
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg


class TestRectangle(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
            s1 = np.array([[2], [1.2]])
            s2 = np.array([[4], [3]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            num_intersection = str(num_intersection)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()

        try:
            Pol.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Pol.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result,
                             "[[1.         1.77777778 2.         4.         4.11111111 1.\n  1.77777778 5.    "
                             "     5.         4.11111111 4.         2.        ]\n [1.         1.         1.2  "
                             "      3.         3.1        3.1\n  1.         1.         3.1        3.1        "
                             "3.         1.2       ]]\n[0. 4. 5. 6. 7. 3. 4. 1. 2. 7. 6. 5.]")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.tomatrix(segment)
            newpoints = Np.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(Np.NewPoints.newpoints(newpoints)), "[[1.         5.         5.         1.         2.         4.\n  1.77777778 4.11111111]\n [1.         1.         3.1        3.1        1.2        3.\n  1.         3.1       ]]")
        except:
            self.fail()
