from unittest import TestCase
import numpy as np
import src.NewPoints as Np
import src.NewPolygons as Pol
import src.Segment as Seg


class TestPentagon(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
            s1 = np.array([[1.4], [2.75]])
            s2 = np.array([[3.6], [2.2]])
            pol = Pol.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Pol.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()
        try:
            Pol.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Pol.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result,
                             "[[2.5  4.   3.6  1.4  1.2  1.   4.   3.4  1.6  1.2  1.4  3.6 ]\n [1.   2.1  2.2  2.75 2.8  2.1  2.1  4.2  4.2  2.8  2.75 2.2 ]]\n[0. 1. 5. 6. 7. 4. 1. 2. 3. 7. 6. 5.]")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.tomatrix(segment)
            newpoints = Np.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(Np.NewPoints.newpoints(newpoints)),
                             "[[2.5  4.   3.4  1.6  1.   1.4  3.6  1.2 ]\n [1.   2.1  4.2  4.2  2.1  2.75 2.2  2.8 ]]")
        except:
            self.fail()
