from unittest import TestCase
import numpy as np
import src.NewPoints as Np
import src.NewPolygons as Polyg
import src.Segment as Seg


class TestConcaveTwoInt(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[3.5, 6.7, 5.3, 3.7, 2.2, 1.], [1., 4.2, 6.8, 6., 8.5, 5.6]])
            s1 = np.array([[2.], [6.5]])
            s2 = np.array([[5.1], [3.9]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()

        try:
            Polyg.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Polyg.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result, "[[3.5        5.80701754 5.1        2.         1.53410405 1.\n  5.80701754 6.7        5.3        3.7        2.2        1.53410405\n  2.         5.1       ]\n [1.         3.30701754 3.9        6.5        6.89075145 5.6\n  3.30701754 4.2        6.8        6.         8.5        6.89075145\n  6.5        3.9       ]]\n[0. 6. 7. 8. 9. 5. 6. 1. 2. 3. 4. 9. 8. 7.]")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.tomatrix(segment)
            newpoints = Np.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(Np.NewPoints.newpoints(newpoints)), "[[3.5        6.7        5.3        3.7        2.2        1.\n  2.         5.1        5.80701754 1.53410405]\n [1.         4.2        6.8        6.         8.5        5.6\n  6.5        3.9        3.30701754 6.89075145]]")
        except:
            self.fail()
