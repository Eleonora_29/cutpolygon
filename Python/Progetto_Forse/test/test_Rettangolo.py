from unittest import TestCase
import numpy as np
import src.Point as P
import src.Polygon as Pol
import src.Intersector as Int


class TestRectangle(TestCase):
    def test_NewPoints(self):
        vertex = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
        intersection_points = np.array([[1.77777777778, 4.11111111111], [1., 3.1]])
        num_vertex = 4
        num_intersection = 2
        s1 = np.array([[2], [1.2]])
        s2 = np.array([[4], [3]])
        self.assertEqual(
            str(P.Point.NewPoints(P, vertex, intersection_points, s1, s2, num_vertex, num_intersection)),
            "[[1.         5.         5.         1.         1.77777778 4.11111111\n  1.         5.        ]\n [1.      "
            "   1.         3.1        3.1        1.         3.1\n  2.         9.        ]]")

    def test_Cut(self):
        vertex = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
        s1 = np.array([[2], [1.2]])
        s2 = np.array([[4], [3]])
        self.edgeWithInters, self.intersection_point, self.num_intersection = Pol.Polygon.Cut(Pol, s1, s2, 4, vertex)
        intersection_point = str(self.intersection_point)
        num_intersection = str(self.num_intersection)
        edgeWithInters = str(self.edgeWithInters)
        result = edgeWithInters + "\n" + intersection_point + "\n" + num_intersection
        self.assertEqual(result, "[True, False, True, False]\n[[1.77777778 4.11111111]\n [1.         3.1       ]]\n2")