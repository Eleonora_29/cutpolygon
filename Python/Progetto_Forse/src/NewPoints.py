import numpy as np


class NewPoints:
    def __init__(self, vertices, intersection_points, segment):
        self.vertices = vertices
        self.intersection_points = intersection_points
        self.segment = segment

    def newpoints(self):
        tol = 1e-7
        intersection_segment = np.array([[0], [0]], float)
        num_intersections = np.size(self.intersection_points, axis=1)
        for i in range(0, 2):
            temp = np.array([[0], [0]], float)
            temp[0][0] = self.segment[0][i]
            temp[1][0] = self.segment[1][i]
            intersection_segment = np.append(intersection_segment, temp, axis=1)
        intersection_segment = np.transpose(intersection_segment)
        intersection_segment = np.delete(intersection_segment, 0, 0)
        intersection_segment = np.transpose(intersection_segment)

        num_vertex = np.size(self.vertices, axis=1)
        position = 0
        for j in range(0, num_intersections):
            flag = 0
            for i in range(0, 2):
                if (abs(self.intersection_points[0][j] - intersection_segment[0][i]) <= tol) \
                        and (abs(self.intersection_points[1][j] - intersection_segment[1][i]) <= tol):
                    flag += 1
            if flag == 0:
                temp[0][0] = self.intersection_points[0][j]
                temp[1][0] = self.intersection_points[1][j]
                intersection_segment = np.append(intersection_segment, temp, axis=1)
                position += 1

        new_points = np.array([[0], [0]], float)
        for i in range(0, num_vertex):
            temp = np.array([[0], [0]], float)
            temp[0][0] = self.vertices[0][i]
            temp[1][0] = self.vertices[1][i]
            new_points = np.append(new_points, temp, axis=1)
        new_points = np.transpose(new_points)
        new_points = np.delete(new_points, 0, 0)
        new_points = np.transpose(new_points)

        position = 0
        num_intseg = np.size(intersection_segment, axis=1)
        for j in range(0, num_intseg):
            flag = 0
            for i in range(0, num_vertex):
                if (abs(intersection_segment[0][j] - self.vertices[0][i]) <= tol) \
                        and (abs(intersection_segment[1][j] - self.vertices[1][i]) <= tol):
                    flag += 1
            if flag == 0:
                temp[0][0] = intersection_segment[0][j]
                temp[1][0] = intersection_segment[1][j]
                new_points = np.append(new_points, temp, axis=1)
                position += 1
        return new_points

    def resultnewpoints(self, new_points):
        new_points = np.transpose(new_points)
        result = "The set of vertices, endpoints of the segment and intersection points is:\n" + str(new_points)
        return result


'''
Classe Punto -> FUNZIONA
1. Metodo cotruttore: vertici e punti di intersezione
2. Metodo newpoints: resituisce la matrice dei nuovi punti con vertici, intersezioni e gli estremi del segmento
3. Metodo resultnewpoints: restituisce la stringa dei nuovi punti

-Tengo conto della tolleranza
'''