from unittest import TestCase
import src.empty_class as empty_class


class TestEmptyClass(TestCase): #Testa la classe
    def test_empty_method(self): #Testa il metodo
        empty_object = empty_class.EmptyClass()

        try:
            self.assertEqual(empty_object.empty_method(), True)
        except Exception as ex:
            self.fail()

'''
Test vuoto da fare
'''