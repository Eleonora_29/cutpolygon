import Point
import numpy as numpy

class Intersector:
    def __init__(self):
        self._origin1 = _origin1
        self._origin2 = _origin2

    def SetStraight(self, _origin1: Point, end1: Point):
        self.end1 = end1
        self.matrixTangentVector = matrixTangentVector
        self.matrixTangentVector[0].x = end1.x - _origin1.x
        self.matrixTangentVector[0].y = end1.y - _origin1.y

    def SetEdge(self, _origin2: Point, end2: Point):
        self.end2 = end2
        self.matrixTangentVector[1].x = _origin2.x - end2.x
        self.matrixTangentVector[1].y = _origin2.y - end2.y

    def ComputeIntersection(self):
        self.rightHandSide = rightHandSide
        self.rightHandSide.x = self._origin2.x - self._origin1.x
        self.rightHandSide.y = self._origin2.y - self._origin1.y
        self.intersectionCoordinates = intersectionCoordinates

        solverMatrix = numpy.linalg.inv([self.matrixTangentVector[0].x, self.matrixTangentVector[0].y, self.matrixTangentVector[1].x, self.matrixTangentVector[1].y])
        resultParametricCoordinates = solverMatrix * rightHandSide

        if 1 > resultParametricCoordinates[0] > 0:
            intersection = True
        else:
            intersection = False

        intersectionCoordinates = self._origin1 + self.matrixTangentVector[0] * resultParametricCoordinates[0]

        if intersection is True:
            numIntersections += 1

        intersectionPoints[numIntersections - 1] = intersectionCoordinates

        return self.intersectionCoordinates

    def ParametricCoordinates(self):
        return self.resultParametricCoordinates[0], self.resultParametricCoordinates[1]

    def IntersectionPoints(self):
        return self.IntersectionPoints

'''
Classe Intersector: ho copiato ciò che ci ha mandato Cami. 
Se vuoi mettere dei commenti o modificare tutto tuo!:)
Non ho ancora fatto niente

'''