import src.Intersector as Int
import src.Point as P
import numpy as np


class Polygon:
    def __init__(self):
        pass

    def Cut(self, s1, s2, num_vertex, vertex):
        i: int = 0
        edgeWithInters = []
        intersection_points = np.array([[0],[0]], float)
        num_intersections = 1
        for i in range(0, num_vertex, 1):
            matrix_tangent_vector = Int.Intersector.SetStraight(Int, s1, s2)
            l0 = np.zeros((2, 1), float)
            l0[0][0] = vertex[0][0]
            l0[1][0] = vertex[1][0]
            li = np.zeros((2, 1), float)
            li[0][0] = vertex[0][i]
            li[1][0] = vertex[1][i]
            if (i != (num_vertex-1)):
                li_1 = np.zeros((2, 1), float)
                li_1[0][0] = vertex[0][i + 1]
                li_1[1][0] = vertex[1][i + 1]
                Int.Intersector.SetEdge(Int, matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.SetEdge(Int, matrix_tangent_vector, li, l0)
            resultParametricCoordinates, inters = Int.Intersector.Intersection(Int, matrix_tangent_vector, s1, li)
            if (inters == True):
                intersectionCoordinates = Int.Intersector.IntersectionCoordinates(Int, matrix_tangent_vector, s1, resultParametricCoordinates)
                if (num_intersections == 0 or (intersectionCoordinates[0][0] != intersection_points[0][num_intersections-1] or intersectionCoordinates[1][0] != intersection_points[1][num_intersections-1])):
                    edgeWithInters.append(True)
                    num_intersections += 1
                    flag = np.array([[0],[0]], float)
                    flag = intersectionCoordinates
                    intersection_points = np.append(intersection_points, flag, axis = 1)
                else:
                    edgeWithInters.append(False)

            else:
                edgeWithInters.append(False)
        intersection_points = np.transpose(intersection_points)
        intersection_points = np.delete(intersection_points, 0, 0)
        intersection_points = np.transpose(intersection_points)
        return edgeWithInters, intersection_points, num_intersections-1

    def ChooseFunction(self, num_intersections):
        if num_intersections == 2:
            Polygon.NewPolygonsTwoInt()
        else:
            Polygon.NewPolygonsMoreInt()

    def NewPolygonsTwoInt(self, vertex, num_vertex, edgeWithInters, intersection_points, s1, s2, num_intersection):
        pol1 = np.array([[0],[0]], float)
        pol2 = np.array([[0],[0]], float)
        polNum1 = [0]
        polNum2 = []
        flag = np.zeros((2, 1), float)
        flag1 = np.zeros((2, 1), float)
        flag[0][0] = vertex[0][0]
        flag[1][0] = vertex[1][0]
        pol1 = np.append(pol1, flag, axis=1)
        pol1 = np.transpose(pol1)
        pol1 = np.delete(pol1, 0, 0)
        pol1 = np.transpose(pol1)
        punti_corrispondenti = np.array([[0],[0]], float)
        cuttedPolygons = np.array([[0],[0]], float)
        newPolygonsVertex = []
        temp2 = np.array([[0],[0]], float)
        distances = []
        i: int = 0
        x: int = num_vertex
        n: int = num_vertex
        p1: int = 1
        p2: int = 0
        v2: int = 0
        e: float = 0
        f: float = 0
        v1=1
        if(v1<x):
            if edgeWithInters[v1-1] == True:
                x = v1-1
                v2 = v1
                if intersection_points[0][i] != pol1[0][p1-1] or intersection_points[1][i] != pol1[1][p1-1]:
                    if intersection_points[0][i] != vertex[0][v1] or intersection_points[1][i] != vertex[1][v1]:
                        flag[0][0] = intersection_points[0][i]
                        flag[1][0] = intersection_points[1][i]
                        pol1 = np.append(pol1,flag, axis = 1)
                        pol2 = np.append(pol2,flag, axis = 1)
                        polNum1 = np.append(polNum1, n)
                        polNum2 = np.append(polNum2, n)
                        n += 1

                    else:
                        flag[0][0] = vertex[0][v1]
                        flag[1][0] = vertex[1][v1]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, v1)
                        polNum2 = np.append(polNum2, v1)
                        v2 += 1
                    p1 += 1
                    p2 += 1


                flag[0][0] = intersection_points[0][i]
                flag[1][0] = intersection_points[1][i]
                e = P.Point.Distance(P, flag, s1)
                distances = np.append(distances, e)
                punti_corrispondenti = np.append(punti_corrispondenti, s1, axis=1)
                f = P.Point.Distance(P, flag, s2)
                distances = np.append(distances, f)
                punti_corrispondenti = np.append(punti_corrispondenti, s2, axis=1)

                z = i+1
                for z in range (z, num_intersection, 1):
                    flag[0][0] = intersection_points[0][i]
                    flag[1][0] = intersection_points[1][i]
                    flag1[0][0] = intersection_points[0][z]
                    flag1[1][0] = intersection_points[1][z]
                    g = P.Point.Distance(P, flag, flag1)
                    distances = np.append(distances, g)
                    punti_corrispondenti = np.append(punti_corrispondenti, flag1, axis=1)

                z = 0
                distance_size = np.size(distances)
                for z in range (0, distance_size-1, 1):
                    #distances = np.sort(distances)
                    #punti_corrispondenti[0][z] = punti_corrispondenti[0][1]
                    #punti_corrispondenti[1][z] = punti_corrispondenti[1][1]

                    min = z
                    w = z+1
                    for w in range (w, distance_size, 1):
                        if distances[w] < distances[min]:
                            min = w
                        temp1 = distances[min]
                        temp2[0][0] = punti_corrispondenti[0][min]
                        temp2[1][0] = punti_corrispondenti[1][min]
                        distances[min] = distances[z]
                        punti_corrispondenti[0][min] = punti_corrispondenti[0][z]
                        punti_corrispondenti[1][min] = punti_corrispondenti[0][z]
                        distances[z] = temp1
                        punti_corrispondenti[0][z] = temp2[0][0]
                        punti_corrispondenti[1][z] = temp2[1][0]

                for a in range(0, distance_size, 1):
                    if punti_corrispondenti[0][a+1] != pol1[0][p1-1] or punti_corrispondenti[1][a+1] != pol1[1][p1-1]:
                        flag[0][0] = punti_corrispondenti[0][a+1]
                        flag[1][0] = punti_corrispondenti[1][a+1]
                        pol1 = np.append(pol1, flag, axis=1)
                        p1 += 1
                        polNum1 = np.append(polNum1, n)
                        n += 1

                for b in range(v1, num_vertex, 1):
                    if edgeWithInters[b] == True:
                        c = b+1
                        for c in range(c, num_vertex, 1):
                            flag[0][0] = vertex[0][c]
                            flag[1][0] = vertex[1][c]
                            pol1 = np.append(pol1, flag, axis=1)
                            polNum1 = np.append(polNum1, c)
                        c = v2
                        for c in range(c, b+1, 1):
                            if vertex[0][c] != pol2[0][p2] or vertex[1][c] != pol2[1][p2]:
                                flag[0][0] = vertex[0][c]
                                flag[1][0] = vertex[1][c]
                                pol2 = np.append(pol2, flag, axis=1)
                                polNum2 = np.append(polNum2, c)

                        flag[0][0] = intersection_points[0][1]
                        flag[1][0] = intersection_points[1][1]
                        pol2 = np.append(pol2, flag, axis=1)
                        m = n-1
                        polNum2 = np.append(polNum2, m)
                        m -= 1

                        if f < e:
                            pol2 = np.append(pol2, s1, axis=1)
                            pol2 = np.append(pol2, s2, axis=1)
                        else:
                            pol2 = np.append(pol2, s2, axis=1)
                            pol2 = np.append(pol2, s1, axis=1)

                        polNum2 = np.append(polNum2, m)
                        polNum2 = np.append(polNum2, m-1)

            else:
                if vertex[0][v1] != pol1[0][p1-1] or vertex[1][v1] != pol1[1][p1-1]:
                    flag[0][0] = vertex[0][v1]
                    flag[1][0] = vertex[1][v1]
                    pol1 = np.append(pol1, flag, axis=1)
                    polNum1 = np.append(polNum1, v1)

        pol2 = np.transpose(pol2)
        pol2 = np.delete(pol2, 0, 0)
        pol2 = np.transpose(pol2)

        pol = np.concatenate((pol1, pol2), axis=1)
        cuttedPolygons = np.append(cuttedPolygons, pol, axis=1)
        cuttedPolygons = np.transpose(cuttedPolygons)
        cuttedPolygons = np.delete(cuttedPolygons, 0, 0)
        cuttedPolygons = np.transpose(cuttedPolygons)
        newPolygonsVertex = np.append(newPolygonsVertex, polNum1)
        newPolygonsVertex = np.append(newPolygonsVertex, polNum2)

        return cuttedPolygons, newPolygonsVertex

    def NewPolygonsMoreInt(self):
        pass

vertex = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
s1 = np.array([[1.4], [2.75]])
s2 = np.array([[3.6], [2.2]])
num_vertex = 5
edgeWithInters, intersection_points, num_intersection = Polygon.Cut(Polygon, s1, s2,  num_vertex, vertex)
cuttedPolygons, newPolygonsVertex = Polygon.NewPolygonsTwoInt(Polygon, vertex, num_vertex, edgeWithInters, intersection_points, s1, s2, num_intersection)
print(cuttedPolygons, newPolygonsVertex)


'''
distance = []
e = 10
distance = np.append(distance, e)
f = 4
distance = np.append(distance, f)
print(distance)
distance_size = np.size(distance)
print(distance_size)
distance = np.sort(distance)
print(distance)

vertex = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
s1 = np.array([[2], [1.2]])
s2 = np.array([[4], [3]])
edgeWithInters, intersection_points, num_intersection = Polygon.Cut(Polygon, s1, s2, 4, vertex)
num_vertex = 4

cuttedPolygons, newPolygonsVertex = Polygon.NewPolygonsTwoInt(Polygon, vertex, num_vertex, edgeWithInters, intersection_points, s1, s2, num_intersection)
print("cuttedPolygons\n", str(cuttedPolygons), "\nnewPolygonsVertex\n", newPolygonsVertex)

'''