from unittest import TestCase
#import src.Vector2d as V2d -> quello che dovrebbe essere scritto per far importare il test

class Vector2d: #Visto che a me non funziona quella cosa ho importato direttamente la classe Vector2d
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def Abscissa(self):
        return self.x

    def Ordered(self):
        return self.y


class TestVector2d(TestCase): #Definita la classe di Test
    def test_Abscissa(self): #Definito il test per l'ascissa che chiede 1 come risultato
        vector = Vector2d(1, 2)
        self.assertEqual(Vector2d.Abscissa(vector), 1)

    def test_Ordered(self): #Definito il test per l'ordinata che chiede 2 come risultato
        vector = Vector2d(1, 2)
        self.assertEqual(Vector2d.Ordered(vector), 2)


'''
Ho creato il test per la classe Vector2d. Il test funziona.
Passa il vettore (1,2) e chiede di restituire l'ascissa e l'ordinata.

Per Betta -> PROBLEMA: non mi fa importare il file Vector2d da fuori! Trovare una soluzione!
'''