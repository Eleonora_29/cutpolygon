from unittest import TestCase
#import src.Point as Point -> quello che dovrebbe essere scritto per far importare il test
import math as math
import numpy as np

class Vector2d: #Visto che a me non funziona quella cosa ho importato direttamente la classe Vector2d
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def Abscissa(self):
        return self.x

    def Ordered(self):
        return self.y

class Point: #Visto che a me non funziona quella cosa ho importato direttamente la classe Point
    def __init__(self):
        pass

    def Distance(self, A: Vector2d, B: Vector2d):
        distance = math.sqrt(math.pow((A.x - B.x), 2) + math.pow((A.y - B.y), 2))
        return distance

    def New_points(self, vertices, intersection_points, num_vertex: int, num_intersections: int):
        self.vertices = np.zeros((num_vertex, 2), float)
        self.intersection_points = np.zeros((num_intersections, 2), float)

        really_Intersection = 0
        for j in range(0, num_intersections, 1):
            flag = 0
            for i in range(0, num_vertex, 1):
                if intersection_points[j][0] != vertices[i][0] or intersection_points[j][1] != vertices[i][1]:
                    flag = flag + 1
            if flag == num_vertex:
                really_Intersection = really_Intersection + 1

        num_tot = really_Intersection + num_vertex
        self.new_points = np.zeros((num_tot, 2))
        for i in range(0, num_vertex, 1):  # range(start, stop, step)
            self.new_points[i][0] = vertices[i][0]
            self.new_points[i][1] = vertices[i][1]
        position = 0
        for j in range(0, num_intersections, 1):
            flag = 0
            for i in range(0, num_vertex, 1):
                if intersection_points[j][0] == vertices[i][0] and intersection_points[j][1] == vertices[i][1]:
                    flag += 1
            if flag == 0:
                self.new_points[num_vertex + position][0] = intersection_points[j][0]
                self.new_points[num_vertex + position][1] = intersection_points[j][1]
                position += 1
        result = str(self.new_points)
        return result



class TestPoint(TestCase): #Definita la classe di Test
    def test_Distance(self): #Definito il test per la distanza che chiede 1.4142135623730951 come risultato
        vector1 = Vector2d(1, 2)
        vector2 = Vector2d(2, 3)
        self.assertEqual(Point.Distance(self, vector1, vector2), 1.4142135623730951)

    def test_NewPoints(self): #Definisco il test per la restituzione di punti che restiuisce una stringa come risultato
        vertices = ((1, 2), (3, 4), (7, 8),)
        intersection_points = ((3, 4), (8, 12),)
        num_vertex = 3
        num_intersection = 2
        self.assertEqual(Point.New_points(Point, vertices, intersection_points, num_vertex, num_intersection),  "[[ 1.  2.]\n [ 3.  4.]\n [ 7.  8.]\n [ 8. 12.]]")



'''
Ho creato il test per la classe Point. Il test funziona.
Test sul metodo distanza. Passa due punti e ne calcola la distanza
Test sul metodo che restituisce i punti. Passa i vertici, le intersezioni, il numero dei vertici e delle intersezioni
e restituisce una stringa con i new_points

'''