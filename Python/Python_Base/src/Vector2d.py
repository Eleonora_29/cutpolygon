class Vector2d:
    def __init__(self, x: float, y: float): #Costruttore
        self.x = x
        self.y = y

    def Abscissa(self): #Metodo per le X
        return self.x

    def Ordered(self): #Metodo per le Y
        return self.y


'''
Ho creato la classe vettore a due dimensioni come aveva fatto Vicini nell'esercitazione 7_Maintenance 
La classe ha un costruttore per assegnare i valori delle coordinate x e y
La classe ha due metodi "Abscissa" e "Ordered" che restituiscono, rispettivamente, il valore dell'ascissa e dell'ordinata
'''