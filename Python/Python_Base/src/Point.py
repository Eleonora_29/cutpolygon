import math as math
import numpy as np
import Vector2d as V2d


class Point:
    def __init__(self): #Costruttore vuoto
        pass

    def Distance(self, A: V2d, B: V2d): #Metodo distanza
        distance = math.sqrt(math.pow((A.x - B.x), 2) + math.pow((A.y - B.y), 2)) #I punti dati sono V2d quindi posso usare i metodi X e Y
        return distance

    def New_points(self, vertices, intersection_points, num_vertex: int, num_intersections: int): #Metodo New_points
        self.vertices = np.zeros((num_vertex, 2), float) #Dichiaro una matrice di num_vertex righe e 2 colonne riempita di zeri
        self.intersection_points = np.zeros((num_intersections, 2), float) #Dichiaro una matrice di num_intersections righe e 2 colonne riempita di zeri

        #Cerco il numero reale delle intersezioni -> quanti punti devo effettivamente aggiungere al vettore dei vertices
        really_Intersection = 0 #Dichiaro il contatore
        for j in range(0, num_intersections, 1): #For sulla matrice contenente le intersezioni
            flag = 0 #Dichiaro il contatore
            for i in range(0, num_vertex, 1): #For sulla matrice dei vertici
                #If: controllo se una tra le x o le y di intersection_points e vertices sono diversi. Se succede allora aumento il flag
                if intersection_points[j][0] != vertices[i][0] or intersection_points[j][1] != vertices[i][1]:
                    flag += 1
            #If: se il flag = num_vertex allora so che è un punto di intersezione e non è un vertice.Aumento il contatore delle really_Intersection
            if flag == num_vertex:
                really_Intersection += 1

        num_tot = really_Intersection + num_vertex #Così so quanti sono i punti che deve restituire New_points
        self.new_points = np.zeros((num_tot, 2)) #Dichiaro una matrice di num_tot righe e 2 colonne riempita di zeri
        for i in range(0, num_vertex, 1): #For: riempio new_points con i vertici
            self.new_points[i][0] = vertices[i][0]#Sulle x
            self.new_points[i][1] = vertices[i][1]#Sulle y

        position = 0 #Contatore per sapere dove mettere i nuovi punti
        for j in range(0, num_intersections, 1): #For sulla matrice contenente le intersezioni
            flag = 0 #Contatore
            for i in range(0, num_vertex, 1): #For sulla matrice dei vertici
                #If: se le x e le y di intersection_points e vertices sono uguali aumento il flag
                if intersection_points[j][0] == vertices[i][0] and intersection_points[j][1] == vertices[i][1]:
                    flag += 1
            #If: se il flag è uguale a zero allora vuol dire che l'intersezione non è un vertice devo aggiungere il punto in new_points nella posizione num_vertex + position
            if flag == 0:
                self.new_points[num_vertex + position][0] = intersection_points[j][0] #Sulle x
                self.new_points[num_vertex + position][1] = intersection_points[j][1] #Sulle y
                position = position + 1 #Aumento la posizione
            result = str(self.new_points) #Converto new_points in str solamente per riuscire a far passare il test con facilità
        return result

'''
Ho creato la classe Point importando math per la radice quadrata e Vector2d per poter usare i vettori
La classe ha un costruttore vuoto
La classe ha un metodo chiamato "Distance" che prende in input due punti come vettori a due dimensioni e restituisce la distanza tra di essi
La classe ha un metodo chiamato "New_points" che prende in input i vertici e i punti di intersezione e restituisce un'unica stringa di punti 
'''
