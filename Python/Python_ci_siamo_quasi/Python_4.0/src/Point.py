import math as math
import numpy as np
import src.Vector2d as V2d
import src.Segment as Seg


class Point:
    def __init__(self):
        pass

    def Distance(self, A: V2d, B: V2d):
        A = V2d.Vector2d.InMatrix(A)
        B = V2d.Vector2d.InMatrix(B)
        return math.sqrt(math.pow((A[0][0] - B[0][0]), 2) + math.pow((A[1][0] - B[1][0]), 2))

    def ReallyDif(self, cont1: int, cont2: int, mat1: np.matrix, mat2: np.matrix):
        tol = 1e-6
        really_intersection = 0  # Dichiaro il contatore
        for j in range(0, cont1, 1):  # For sul prima dimensione
            flag = 0  # Dichiaro un flag
            for i in range(0, cont2, 1):  # For sulla seconda dimensione
                # If: controllo se una tra le x o le y di mat1 e mat2 sono diversi. Se succede allora aumento il flag
                if (abs(mat1[0][j] - mat2[0][i]) >= tol) or (abs(mat1[1][j] - mat2[1][i]) >= tol):
                    flag += 1
            # If: se il flag = cont2 allora so che è un punto nuovo. Aumento il contatore delle really_intersection
            if flag == cont2:
                really_intersection += 1
        return really_intersection

    def SearchDif(self, cont1: int, cont2: int, indice: int, mat1: np.matrix, mat2: np.matrix, mat3: np.matrix, mat4: np.matrix):
        tol = 1e-6
        position = 0  #Contatore per sapere dove mettere i nuovi punti
        for j in range(0, cont1, 1):  #For sulla matrice 1
            flag = 0  #Contatore
            for i in range(0, cont2, 1):  #For sulla matrice 2
                #If: se le x e le y di mat1 e mat2 sono uguali aumento il flag
                if (abs(mat1[0][j] - mat2[0][i]) <= tol) and (abs(mat1[1][j] - mat2[1][i]) <= tol):
                    flag += 1
            #If: se il flag è uguale a zero allora vuol dire che l'elemento è nuovo e devo aggiungerlo a mat3 nella posizione indice + position
            if flag == 0:
                mat3[0][indice + position] = mat4[0][j]
                mat3[1][indice + position] = mat4[1][j]
                position = position + 1  #Aumento la posizione
        return mat3

    def NewPoints(self, vertices , intersection_points, s1: V2d, s2: V2d, num_vertex: int, num_intersections: int):
        self.vertices = np.zeros((2, num_vertex), float)  # Dichiaro la matrice
        self.intersection_points = np.zeros((2, num_intersections), float)  # Dichiaro la matrice
        segment12 = Seg.Segment.InMatrix(Seg, s1, s2)  # Dati gli estremi del segmento formo la matrice segmento
        really_int_seg = Point.ReallyDif(Point, num_intersections, 2, intersection_points, segment12)  # Conto le vere intersezioni tra intersection_points e segment12
        int_seg = np.zeros((2, really_int_seg + num_intersections), float)  # Dichiaro la matrice
        for i in range(0, num_intersections, 1):  # For: riempio int_seg con i intersection_points
            int_seg[0][i] = intersection_points[0][i]  # Sulle x
            int_seg[1][i] = intersection_points[1][i]  # Sulle y
        int_seg = Point.SearchDif(Point, really_int_seg + num_intersections, 2, num_intersections, int_seg, segment12, int_seg, segment12)  # Riempio la matrice int_seg con le intersezioni e gli estremi del segmento
        really_Intersection = Point.ReallyDif(Point, num_intersections + really_int_seg, num_vertex, int_seg, vertices)  # Conto le vere intersezioni tra int_seg e vertices
        num_tot = really_Intersection + num_vertex  # Conto i punti che deve restituire NewPoints
        new_points = np.zeros((2, num_tot))  # Dichiaro la matrice
        for i in range(0, num_vertex, 1):  # For: riempio new_points con i vertici
            new_points[0][i] = vertices[0][i]  # Sulle x
            new_points[1][i] = vertices[1][i]  # Sulle y
        new_points = Point.SearchDif(Point, num_intersections + really_int_seg, num_vertex, num_vertex, int_seg, vertices, new_points, int_seg)  # Riempio la matrice new_points con i vertici, le intersezioni e gli estremi del segmento
        return new_points

    def PrintNewPoints(self, new_points):
        p_new_points = np.transpose(new_points)
        cout = "L'insieme dei vertici, punti di intersezioni e estremi del segmento è: \n" + str(p_new_points)
        return cout


'''
Classe Punto -> FUNZIONA
1. Metodo cotruttore: vuoto
2. Metodo Distance: prende due V2d e ne calcola la distanza
3. Metodo ReallyDif: prende due matrici e calcola quanti elementi della seconda matrice sono diversi dalla prima
4. Metodo SearchDif: prende quattro matrici e riempie la terza (Serve per creare la nuova matrice solo con i punti interessanti)
5. Metodo NewPoints: prende i vari punti e resituisce la matrice dei nuovi punti con vertici, intersezioni e estremo del segmento
6. Metodo PrintNewPoints: restituisce la stringa dei nuovi punti

-Tengo conto della tolleranza
'''
