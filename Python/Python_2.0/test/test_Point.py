from unittest import TestCase
import src.Point as Point
import src.Vector2d as V2d
import numpy as np

class TestPoint(TestCase):
    def test_InMatrix(self):
        vector1 = V2d.Vector2d(1,2)
        result = str(Point.Point.InMatrix(Point, vector1))
        self.assertEqual(result, "[[1. 2.]]")

    def test_Distance(self):
        vector1 = V2d.Vector2d(1, 2)
        vector2 = V2d.Vector2d(2, 3)
        self.assertTrue(abs(Point.Point.Distance(Point, vector1, vector2) - 1.4142135) < 1e-6)

    def test_ReallyDiff(self):
        cont1 = 3
        cont2 = 2
        mat1 = ((3, 4), (8, 12), (3,5))
        mat2 = ((1,2), (8,12))
        self.assertEqual(Point.Point.ReallyDif(Point,cont1, cont2, mat1, mat2), 2)

    def test_SearchDif(self):
        cont1 = 4
        cont2 = 3
        indice = 3
        mat1 = np.array([[3, 4], [8, 12], [1, 2], [5, 9]])
        mat2 = np.array([[1, 2], [3, 4], [7, 8]])
        mat3 = np.array([[1, 2], [3, 4], [7, 8], [0, 0], [0, 0]])
        mat4 = mat1
        mat = Point.Point.SearchDif(Point, cont1, cont2, indice, mat1, mat2, mat3, mat4)
        result = str(mat)
        self.assertEqual(result, "[[ 1  2]\n [ 3  4]\n [ 7  8]\n [ 8 12]\n [ 5  9]]")

    def test_NewPoints(self):
        vertices = ((1, 2), (3, 4), (7, 8))
        intersection_points = ((3, 4), (8, 12))
        num_vertex = 3
        num_intersection = 2
        s1 = V2d.Vector2d(1,2)
        s2 = V2d.Vector2d(5,9)
        self.assertEqual(Point.Point.NewPoints(Point, vertices, intersection_points, s1, s2, num_vertex, num_intersection),  "[[ 1.  2.]\n [ 3.  4.]\n [ 7.  8.]\n [ 8. 12.]\n [ 5.  9.]]")


'''
Classe di TestPoint -> FUNZIONA
1. Test su InMatrix: da V2d a np.matrix
2. Test su Distance: distanza tra due V2d
3. Test su ReallyDiff: conta quanti punti sono differenti tra due matrici
4. Test su SearchDif: costruisce una matrice nuova a partire da mat1 con gli elementi nuovi delle altre
5. Test su NewPoints: restiuisce i punti 

-Tengo conto della tolleranza
'''