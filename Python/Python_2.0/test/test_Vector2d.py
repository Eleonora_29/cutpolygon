from unittest import TestCase
import src.Vector2d as V2d

class TestVector2d(TestCase):
    def test_X(self):
        vector = V2d.Vector2d(1, 2)
        self.assertEqual(V2d.Vector2d.X(vector), 1)

    def test_Y(self):
        vector = V2d.Vector2d(1, 2)
        self.assertEqual(V2d.Vector2d.Y(vector), 2)

    def test_IsV2d(self):
        vector = V2d.Vector2d(1, 2)
        self.assertEqual(str(V2d.Vector2d.IsV2d(V2d, vector)), "True")

'''
Classe di TestVector2d -> FUNZIONA
1. Test su X
2. Test su Y
3. Test 
'''