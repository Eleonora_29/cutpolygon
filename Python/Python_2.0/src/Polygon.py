import Vector2d as V2d


class Polygon:
    # Costruttore
    def __init__(self, num_vertex: int, vertex: V2d, s1: V2d, s2: V2d):
        self.num_vertex = num_vertex
        self.vertex = vertex
        self.s1 = s1
        self.s2 = s2

    def Cut(self, num_vertex, vertex, intersector, edgeWithInters, s1, s2, intersectionPoints):
        numIntersections = 0
        for i in range(0, num_vertex, 1):
            if i != (num_vertex - 1):
                intersector.SetEdge(vertex[i], vertex[i + 1])
            else:
                intersector.SetEdge(vertex[i], vertex[0])
            intersector.SetStraight(s1, s2)
            inters = intersector.Intersection()
            if inters:
                intersectionCoordinates = intersector.IntersectionCoordinates()
                edgeWithInters.push_back(True)
                numIntersections += 1
                intersectionPoints.push_back(intersectionCoordinates)
            else:
                edgeWithInters.push_back(False)
        return None

    def NewPolygons(self, vertex, edgeWithInters, numVertex, numIntersections, intersectionPoints, pol):
        pol[0] = vertex[0]
        i = 0
        v = 1
        for k in range(1, numVertex + numIntersections, 1):
            if edgeWithInters[k - 1]:
                if intersectionPoints[i] != pol[k - 1]:
                    pol[k] = intersectionPoints[i]
                    k += 1
                pol[k] = intersectionPoints[i + 1]
                k += 1
                for j in range(k, numVertex, 1):
                    if edgeWithInters[j]:
                        pol[k] = vertex[j + 1]
            else:
                if vertex[v] != pol[k - 1]:
                    pol[k] = vertex[v]
                    k += 1
                v += 1
        return None

'''
Classe Polygon: ho copiato ciò che ci ha mandato Camilla. 
Se vuoi mettere dei commenti o modificare tutto tuo!:)
Non ho ancora fatto niente...
'''