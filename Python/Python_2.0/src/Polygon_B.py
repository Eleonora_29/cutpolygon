import src.Vector2d as V2d
import src.Intersector_B as Int
import numpy as np

class Polygon:
    def __init__(self, num_vertx: int, vertex, s1: V2d, s2: V2d):
        self.num_vertex = num_vertx
        self.s1 = s1
        self.s2 = s2
        self.vertex = vertex
        self.numIntersections = 0
        self.intersectionPoints = np.zeros((2, 2), float) #Crea vettori di quello che vuoi
        self.edgeWithInters = []

    def Cut(self):
        i: int = 0
        for i in range(0, self.num_vertex, 1):
            if(i!=(self.num_vertex - 1)):
                Int.Intersector.SetEdge(self.vertex[i], self.vertex[i+1])
            else:
                Int.Intersector.SetEdge(self.vertex[i], self.vertex[0])
            Int.Intersector.SetStraight(self.s1, self.s2)
            inters: bool = Int.Intersector.Intersection()
            if(inters == True):
                intersectionCoordinates = Int.Intersector.IntersectionCoordinates()
                if(self.numIntersections == 0 and intersectionCoordinates != self.intersectionPoints[self.numIntersections -1]):
                    self.edgeWithInters.append(True)
                    self.numIntersections += 1
                    self.intersectionPoint.append(intersectionCoordinates)
                else:
                    self.edgeWithInters.append(False)

            else:
                self.edgeWithInters.append(False)