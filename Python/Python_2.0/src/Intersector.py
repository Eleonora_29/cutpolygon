import numpy as np
import Vector2d as V2d


class Intersector:
    def __init__(self):
        pass

    def SetStraight(self, _origin1: V2d, end1: V2d):
        self._origin1 = _origin1
        self.end1 = end1
        matrixTangentVector = np.zeros((2, 2), float)
        matrixTangentVector[0][0] = self.end1[0] - self._origin1[0]
        matrixTangentVector[0][1] = self.end1[1] - self._origin1[1]
        return None

    def SetEdge(self, _origin2: V2d, end2: V2d):
        self._origin2 = _origin2
        self.end2 = end2
        matrixTangentVector = np.zeros((2, 2), float)
        matrixTangentVector[1][0] = self._origin2[0] - self.end2[0]
        matrixTangentVector[1][1] = self._origin2[1] - self.end2[1]
        return None

    def Intersection(self, matrixTangentVector):
        toleranceParallelism = 1.0E-7
        toleranceIntersection = 1.0E-7
        rightHandSide = np.zeros((2, 1), float)
        rightHandSide[0] = self._origin2[0] - self._origin1[0]
        rightHandSide[1] = self._origin2[1] - self._origin1[1]

        resultParametricCoordinates = np.zeros((2, 1), float)
        det = np.linalg.det(matrixTangentVector)
        check = toleranceParallelism * toleranceParallelism * (np.linalg.norm(matrixTangentVector[0], ord=2) ** 2) * (
                np.linalg.norm(matrixTangentVector[1], ord=2) ** 2)
        if pow(det, 2) >= check:
            solverMatrix = np.linalg.inv(matrixTangentVector[0][0], matrixTangentVector[0][1],
                                         matrixTangentVector[1][0],
                                         matrixTangentVector[1][1])  # funzione di numpy per trovare la matrice inversa
            resultParametricCoordinates = (solverMatrix * rightHandSide) / det

        if resultParametricCoordinates[1] - 1.0 < toleranceIntersection and resultParametricCoordinates[
                1] > -toleranceIntersection:
            intersection = True
        else:
            intersection = False
        return intersection

    def IntersectionCoordinates(self, matrixTangentVector, resultParametricCoordinates):
        intersectionCoordinates = np.zeros((2, 1), float)
        intersectionCoordinates[0] = self._origin1[0] + matrixTangentVector[0][0] * resultParametricCoordinates[0]
        intersectionCoordinates[1] = self._origin1[1] + matrixTangentVector[0][1] * resultParametricCoordinates[0]
        return intersectionCoordinates

'''
Classe Intersector: ho copiato ciò che ci ha mandato Camilla. 
Se vuoi mettere dei commenti o modificare tutto tuo!:)
Non ho ancora fatto niente...

'''