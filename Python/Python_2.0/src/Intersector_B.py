import src.Point as P
import src.Vector2d as V2d
import numpy as np


class Intersector:
    def __init__(self, matrix_tangent_vector):
        matrix_tangent_vector = np.zeros((2, 2), float)
        self.matrix_tangent_vector = matrix_tangent_vector

    def SetStraight(self, origin_l1, end_l1):
        self.origin_l1 = origin_l1
        self.end_l1 = end_l1
        type_origin_l1 = str(V2d.Vector2d.IsV2d(P, origin_l1))
        type_end_l1 = str(V2d.Vector2d.IsV2d(P, origin_l1))
        if (type_origin_l1 == "False" and type_end_l1 == "False"):
            self.matrix_tangent_vector[0][0] = end_l1[0][0] - origin_l1[0][0]  # Sulle x
            self.matrix_tangent_vector[1][0] = end_l1[0][1] - origin_l1[0][1]  # Sulle y
            return self.matrix_tangent_vector
        else:
            origin_l1 = P.Point.InMatrix(P,origin_l1)  # Richiama il metodo della classe Point per costruire matrici (1*2) dato un V2d
            end_l1 = P.Point.InMatrix(P, end_l1)
            self.matrix_tangent_vector[0][0] = end_l1[0][0] - origin_l1[0][0]  # Sulle x
            self.matrix_tangent_vector[1][0] = end_l1[0][1] - origin_l1[0][1]  # Sulle y
            return self.matrix_tangent_vector

    def SetEdge(self, origin_l2: V2d, end_l2: V2d):
        self.origin_l2 = origin_l2
        self.end_l2 = end_l2
        type_origin_l2 = str(V2d.Vector2d.IsV2d(P, origin_l2))
        type_end_l2 = str(V2d.Vector2d.IsV2d(P, end_l2))
        if (type_origin_l2 == "False" and type_end_l2 == "False"):
            self.matrix_tangent_vector[0][1] = origin_l2[0][0] - end_l2[0][0]  # Sulle x
            self.matrix_tangent_vector[1][1] = origin_l2[0][1] - end_l2[0][1]  # Sulle y
            return self.matrix_tangent_vector
        else:
            origin_l2 = P.Point.InMatrix(P, origin_l2)
            end_l2 = P.Point.InMatrix(P, end_l2)
            self.matrix_tangent_vector[0][1] = origin_l2[0][0] - end_l2[0][0]  # Sulle x
            self.matrix_tangent_vector[1][1] = origin_l2[0][1] - end_l2[0][1]  # Sulle y
            return self.matrix_tangent_vector

    def Intersection(self, origin_l1, origin_l2):
        toleranceParallelism = 1e-7
        toleranceIntersection = 1e-7
        right_hand_side = np.zeros((2, 1), float) #Dichiarazione b
        origin_l1 = P.Point.InMatrix(P, origin_l1) #Richiama il metodo della classe Point per costruire matrici (1*2) dato un V2d
        origin_l2 = P.Point.InMatrix(P, origin_l2)
        right_hand_side[0][0] = origin_l2[0][0] - origin_l1[0][0] #Sulle x
        right_hand_side[1][0] = origin_l2[0][1] - origin_l1[0][1] #Sulle y
        det = np.linalg.det(self.matrix_tangent_vector)

        intersection: bool = False
        check = toleranceParallelism * toleranceParallelism * np.linalg.norm(self.matrix_tangent_vector[:, 0], axis=0) * np.linalg.norm(self.matrix_tangent_vector[:, 1], axis=0)

        if (det * det >= check):
            solve_matrix = np.zeros((2,2), float)
            solve_matrix = np.linalg.inv(self.matrix_tangent_vector) #Calcolo l'inversa della matrice
            resultParametricCoordinates = (np.dot(solve_matrix, right_hand_side)) #Prodotto matriciale
            if (resultParametricCoordinates[1] - 1.0 < toleranceIntersection and resultParametricCoordinates[1] > -toleranceIntersection): #Capire con Ele che cosa devo controllare
                intersection = True
            else:
                intersection = False
        return resultParametricCoordinates, intersection

    def IntersectionCoordinates(self, origin_l1, resultParametricCoordinates):
        intersection_coordinates = np.zeros((2,1), float) #Dichiarazione della matrice
        type_origin_l1 = str(V2d.Vector2d.IsV2d(P, origin_l1))
        if (type_origin_l1 == "False"):
            intersection_coordinates[0][0] = origin_l1[0][0] + self.matrix_tangent_vector[0][0] *  resultParametricCoordinates[0]  # Sulle x
            intersection_coordinates[1][0] = origin_l1[0][1] + self.matrix_tangent_vector[1][0] *  resultParametricCoordinates[0]  # Sulle y
            return intersection_coordinates
        else:
            origin_l1 = P.Point.InMatrix(P, origin_l1)
            intersection_coordinates[0][0] = origin_l1[0][0] + self.matrix_tangent_vector[0][0] * resultParametricCoordinates[0] #Sulle x
            intersection_coordinates[1][0] = origin_l1[0][1] + self.matrix_tangent_vector[1][0] * resultParametricCoordinates[0] #Sulle y
            return intersection_coordinates

'''
Classe Intersector -> FUNZIONA
1. Metodo cotruttore: vuoto
2. Metodo SetStraight: Riempie la matrix_tangent_vector
3. Metodo Intersection: Verifica se l'intersezione è presente o meno e ritorna le coordinate parametriche
4. Metodo IntersectionCoordinates: trova le intersezioni
'''
