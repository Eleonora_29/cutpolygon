import math as math
import numpy as np
import src.Vector2d as V2d
import src.Segment as Seg

class Point:
    def __init__(self):
        pass

    def InMatrix(self, A: V2d):
        p_matrix = np.zeros((1, 2), float)
        p_matrix[0][0] = V2d.Vector2d.X(A)
        p_matrix[0][1] = V2d.Vector2d.Y(A)
        return p_matrix

    def Distance(self, A: V2d, B: V2d):
        distance = math.sqrt(math.pow((A.x - B.x), 2) + math.pow((A.y - B.y), 2))
        return distance

    def ReallyDif(self, cont1: int, cont2: int, mat1: np.matrix, mat2: np.matrix):
        tol = 1e-6
        really_intersection = 0  # Dichiaro il contatore
        for j in range(0, cont1, 1):  # For sul prima dimensione
            flag = 0  # Dichiaro un flag
            for i in range(0, cont2, 1):  # For sulla seconda dimensione
                # If: controllo se una tra le x o le y di mat1 e mat2 sono diversi. Se succede allora aumento il flag
                if (abs(mat1[j][0] - mat2[i][0]) >= tol) or (abs(mat1[j][1] - mat2[i][1]) >= tol):
                    flag += 1
            # If: se il flag = cont2 allora so che è un punto nuovo. Aumento il contatore delle really_intersection
            if flag == cont2:
                really_intersection += 1
        return really_intersection

    def SearchDif(self, cont1: int, cont2: int, indice: int, mat1: np.matrix, mat2: np.matrix, mat3: np.matrix, mat4: np.matrix):
        tol = 1e-6
        position = 0  #Contatore per sapere dove mettere i nuovi punti
        for j in range(0, cont1, 1):  #For sulla matrice 1
            flag = 0  #Contatore
            for i in range(0, cont2, 1):  #For sulla matrice 2
                #If: se le x e le y di mat1 e mat2 sono uguali aumento il flag
                if (abs(mat1[j][0] - mat2[i][0]) <= tol) and (abs(mat1[j][1] - mat2[i][1]) <= tol):
                    flag += 1
            #If: se il flag è uguale a zero allora vuol dire che l'elemento è nuovo e devo aggiungerlo a mat3 nella posizione indice + position
            if flag == 0:
                mat3[indice + position][0] = mat4[j][0]
                mat3[indice + position][1] = mat4[j][1]
                position = position + 1  #Aumento la posizione
        return mat3

    def NewPoints(self, vertices, intersection_points, s1: V2d, s2: V2d, num_vertex: int, num_intersections: int):
        self.vertices = np.zeros((num_vertex, 2), float) #Dichiaro la matrice
        self.intersection_points = np.zeros((num_intersections, 2), float) #Dichiaro la matrice
        segment12 = Seg.Segment.InMatrix(Seg, s1, s2) #Dati gli estremi del segmento formo la matrice segmento
        really_int_seg = Point.ReallyDif(Point,num_intersections, 2, intersection_points, segment12) #Conto le vere intersezioni tra intersection_points e segment12
        int_seg = np.zeros((really_int_seg + num_intersections, 2), float) #Dichiaro la matrice
        for i in range(0, num_intersections, 1):  #For: riempio int_seg con i intersection_points
            int_seg[i][0] = intersection_points[i][0]  #Sulle x
            int_seg[i][1] = intersection_points[i][1]  #Sulle y
        int_seg = Point.SearchDif(Point,really_int_seg + num_intersections, 2, num_intersections, int_seg, segment12, int_seg, segment12) #Riempio la matrice int_seg con le intersezioni e gli estremi del segmento
        really_Intersection = Point.ReallyDif(Point, num_intersections+really_int_seg, num_vertex, int_seg, vertices) #Conto le vere intersezioni tra int_seg e vertices
        num_tot = really_Intersection + num_vertex  #Conto i punti che deve restituire NewPoints
        new_points = np.zeros((num_tot, 2))  #Dichiaro la matrice
        for i in range(0, num_vertex, 1):  #For: riempio new_points con i vertici
            new_points[i][0] = vertices[i][0]  #Sulle x
            new_points[i][1] = vertices[i][1]  #Sulle y
        new_points = Point.SearchDif(Point, num_intersections+really_int_seg, num_vertex, num_vertex, int_seg, vertices, new_points, int_seg) #Riempio la matrice new_points con i vertici, le intersezioni e gli estremi del segmento
        result = str(new_points)
        return result

'''
Classe Punto -> FUNZIONA
1. Metodo cotruttore: vuoto
2. Metodo InMatrix: prende un V2d e lo porta in np.matrix
3. Metodo Distance: prende due V2d e ne calcola la distanza
4. Metodo ReallyDif: prende due matrici e calcola quanti elementi della seconda matrice sono diversi dalla prima
5. Metodo SearchDif: prende quattro matrici e riempie la terza (Serve per creare la nuova matrice solo con i punti interessanti)
6. Metodo NewPoints: prende i vari punti e resituisce la matrice dei nuovi punti con vertici, intersezioni e estremo del segmento

-Tengo conto della tolleranza
'''
