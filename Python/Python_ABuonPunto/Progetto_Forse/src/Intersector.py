import numpy as np


class Intersector:
    def __init__(self):
        self.matrix_tangent_vector = np.array([[0., 0.],[0., 0.]], float)

    def setstraight(self, origin_l1, end_l1):
        self.matrix_tangent_vector[0][0] = end_l1[0][0] - origin_l1[0][0]
        self.matrix_tangent_vector[1][0] = end_l1[1][0] - origin_l1[1][0]

    def setedge(self, origin_l2, end_l2):
        self.matrix_tangent_vector[0][1] = origin_l2[0][0] - end_l2[0][0]
        self.matrix_tangent_vector[1][1] = origin_l2[1][0] - end_l2[1][0]

    def intersection(self, origin_l1, origin_l2):
        global parametric_coord
        tolerance_parallelism = 1e-7
        tolerance_intersection = 1e-7
        right_hand_side = np.zeros((2, 1), float)
        right_hand_side[0][0] = origin_l2[0][0] - origin_l1[0][0]
        right_hand_side[1][0] = origin_l2[1][0] - origin_l1[1][0]
        det = np.linalg.det(self.matrix_tangent_vector)
        intersection: bool = False
        check = tolerance_parallelism * tolerance_parallelism * np.linalg.norm(self.matrix_tangent_vector[:, 0]) * np.linalg.norm(self.matrix_tangent_vector[:, 1])
        if det*det >= check:
            solve_matrix = np.linalg.inv(self.matrix_tangent_vector)
            parametric_coord = (np.dot(solve_matrix, right_hand_side))
            if parametric_coord[1] - 1.0 < tolerance_intersection and parametric_coord[1] > -tolerance_intersection:
                intersection = True
            else:
                intersection = False
        return parametric_coord, intersection

    def intersectioncoordinates(self, origin_l1, parametric_coord):
        intersection_coordinates = np.zeros((2,1), float)
        intersection_coordinates[0][0] = origin_l1[0][0] + self.matrix_tangent_vector[0][0] * parametric_coord[0]
        intersection_coordinates[1][0] = origin_l1[1][0] + self.matrix_tangent_vector[1][0] * parametric_coord[0]
        return intersection_coordinates


'''
Classe Intersector -> FUNZIONA
1. Metodo cotruttore: costruisce matrix_tangent_vector
2. Metodo setstraight: Riempie la prima colonna di matrix_tangent_vector
3. Metodo setedge: Riempie la seconda colonna di matrix_tangent_vector
4. Metodo intersection: Verifica se l'intersezione è presente o meno e ritorna le coordinate parametriche
5. Metodo intersectioncoordinates: trova le coordinate delle intersezioni

'''
