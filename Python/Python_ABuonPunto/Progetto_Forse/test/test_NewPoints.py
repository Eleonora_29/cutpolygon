from unittest import TestCase
import src.NewPoints as NP
import numpy as np


class TestPoint(TestCase):
    def test_newpoints_rectangle(self):
        try:
            vertices = np.array([[1., 5., 5., 1.], [1., 1., 3.1, 3.1]])
            intersection_points = np.array([[1.77777777778, 4.11111111111], [1., 3.1]])
            segment = np.array([[2, 4], [1.2, 3]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)), "[[1.         5.         5.         1.         2.         4.\n  1.77777778 4.11111111]\n [1.         1.         3.1        3.1        1.2        3.\n  1.         3.1       ]]")
        except:
            self.fail()

    def test_newpoints_pentagon(self):
        try:
            vertices = np.array([[2.5, 4., 3.4, 1.6, 1.], [1., 2.1, 4.2, 4.2, 2.1]])
            intersection_points = np.array([[4., 1.2], [2.1, 2.8]])
            segment = np.array([[1.4, 3.6], [2.75, 2.2]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[2.5  4.   3.4  1.6  1.   1.4  3.6  1.2 ]\n [1.   2.1  4.2  4.2  2.1  2.75 2.2  2.8 ]]")
        except:
            self.fail()

    def test_newpoints_convex(self):
        try:
            vertices = np.array([[2.4, 5.8, 3.3, 1.7], [1.1, 2.6, 6.8, 6.]])
            intersection_points = np.array([[4.71402715, 2.35241935], [4.42443439, 1.43306452]])
            segment = np.array([[2.8, 4.4], [2.0, 3.9]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[2.4        5.8        3.3        1.7        2.8        4.4\n  4.71402715 2.35241935]\n [1.1        2.6        6.8        6.         2.         3.9\n  4.42443439 1.43306452]]")
        except:
            self.fail()

    def test_newpoints_concave1(self):
        try:
            vertices = np.array([[1.5, 5.6, 5.5, 4., 3.2, 1.], [1., 1.5, 4.8, 6.2, 4.2, 4.]])
            intersection_points = np.array([[4.20432692, 3.72131148, 2.40859729, 1.19121622], [6.00929487, 5.50327869, 4.1280543, 2.8527027]])
            segment = np.array([[2, 4.1], [3.7, 5.9]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[1.5        5.6        5.5        4.         3.2        1.\n  2.         4.1        4.20432692 3.72131148 2.40859729 1.19121622]\n [1.         1.5        4.8        6.2        4.2        4.\n  3.7        5.9        6.00929487 5.50327869 4.1280543  2.8527027 ]]")
        except:
            self.fail()

    def test_newpoints_concave2(self):
        try:
            vertices = np.array([[3.5, 6.7, 5.3, 3.7, 2.2, 1.], [1., 4.2, 6.8, 6., 8.5, 5.6]])
            intersection_points = np.array([[5.80701754, 1.53410405], [3.30701754, 6.89075145]])
            segment = np.array([[2, 6.5], [5.1, 3.9]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[3.5        6.7        5.3        3.7        2.2        1.\n  2.         6.5        5.80701754 1.53410405]\n [1.         4.2        6.8        6.         8.5        5.6\n  5.1        3.9        3.30701754 6.89075145]]")
        except:
            self.fail()

    def test_newpoints_concave3(self):
        try:
            vertices = np.array([[1., 4.5, 5., 3.5, 3., 2., 0.5], [1., 0.5, 4.5, 3.5, 5., 3.5, 5.5]])
            intersection_points = np.array([[4.93916084, 4.20909091, 3.35818182, 2.24230769, 1.748, 0.69141104],
                                            [4.01328671, 3.97272727, 3.92545455, 3.86346154, 3.836, 3.77730061]])
            segment = np.array([[1.1, 3.8], [2.9, 3.9]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[1.         4.5        5.         3.5        3.         2.\n  0.5        1.1        3.8        4.93916084 4.20909091 3.35818182\n  2.24230769 1.748      0.69141104]\n [1.         0.5        4.5        3.5        5.         3.5\n  5.5        2.9        3.9        4.01328671 3.97272727 3.92545455\n  3.86346154 3.836      3.77730061]]")
        except:
            self.fail()

    def test_newpoints_d1(self):
        try:
            vertices = np.array([[2., 0., 3., 0., 3., 3., -1., -3., 0., -3], [-2., -1., 1., 2., 2., 3., 3., 1., 0., -2.]])
            intersection_points = np.array([[1.5,  2.,   3.,   0., - 2.],[1.5, 2., 3., 0., - 2.]])
            segment = np.array([[-1, 1], [-1, 1]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[ 2.   0.   3.   0.   3.   3.  -1.  -3.   0.  -3.  -1.   1.   1.5  2.\n  -2. ]\n [-2.  -1.   1.   2.   2.   3.   3.   1.   0.  -2.  -1.   1.   1.5  2.\n  -2. ]]")
        except:
            self.fail()

    def test_newpoints_d2(self):
        try:
            vertices = vertices = np.array([[2., 0., 3., 0., 3., 3., -1., -3., 0., -3], [-2., -1., 1., 2., 2., 3., 3., 1., 0., -2.]])
            intersection_points = np.array([[0.,  0.,  0.,  0.,  0.], [-1., 2., 3., 0., - 2.]])
            segment = np.array([[0., 0.], [-1.8, 1]])
            newpoints = NP.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(NP.NewPoints.newpoints(newpoints)),
                             "[[ 2.   0.   3.   0.   3.   3.  -1.  -3.   0.  -3.   0.   0.   0.   0. ]\n [-2.  -1.   1.   2.   2.   3.   3.   1.   0.  -2.  -1.8  1.   3.  -2. ]]")
        except:
            self.fail()



    def test_resultnewpoints(self):
        try:
            new_points = np.array([[1., 3., 7., 8., 5.], [2., 4., 8., 12., 9.]])
            self.assertEqual(NP.NewPoints.resultnewpoints(self, new_points),
                             "The set of vertices, endpoints of the segment and intersection points is:\n[[ 1.  2.]\n [ 3.  4.]\n [ 7.  8.]\n [ 8. 12.]\n [ 5.  9.]]")
        except:
            self.fail()


'''
Classe di TestPoint -> FUNZIONA
Ho fatto tutti i test di newpoints così che poi da poterli mettere nel test generale

-Tengo conto della tolleranza
'''