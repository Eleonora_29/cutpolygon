import numpy as np
import src.Intersector as Int
import src.Segment as Seg


class NewPolygons:
    def __init__(self, vertices, s1, s2):
        self.vertices = vertices
        self.edge_with_intersection = []
        self.intersection_points = np.array([[0], [0]], float)
        self.s1 = s1
        self.s2 = s2

    def cut(self):
        num_intersections = 1
        num_vertex = np.size(self.vertices, axis=1)
        for i in range(0, num_vertex, 1):
            matrix_tangent_vector = Int.Intersector()
            Int.Intersector.setstraight(matrix_tangent_vector, self.s1, self.s2)
            l0 = np.zeros((2, 1), float)
            l0[0][0] = self.vertices[0][0]
            l0[1][0] = self.vertices[1][0]
            li = np.zeros((2, 1), float)
            li[0][0] = self.vertices[0][i]
            li[1][0] = self.vertices[1][i]
            if i != (num_vertex - 1):
                li_1 = np.zeros((2, 1), float)
                li_1[0][0] = self.vertices[0][i + 1]
                li_1[1][0] = self.vertices[1][i + 1]
                Int.Intersector.setedge(matrix_tangent_vector, li, li_1)
            else:
                Int.Intersector.setedge(matrix_tangent_vector, li, l0)
            parametric_coord, inters = Int.Intersector.intersection(matrix_tangent_vector, self.s1, li)
            if inters is True:
                intersectionCoordinates = Int.Intersector.intersectioncoordinates(matrix_tangent_vector, self.s1,
                                                                                  parametric_coord)
                if (num_intersections == 0 or (
                        intersectionCoordinates[0][0] != self.intersection_points[0][num_intersections - 1] or
                        intersectionCoordinates[1][0] != self.intersection_points[1][num_intersections - 1])):
                    self.edge_with_intersection.append(True)
                    num_intersections += 1
                    flag = intersectionCoordinates
                    self.intersection_points = np.append(self.intersection_points, flag, axis=1)
                else:
                    self.edge_with_intersection.append(False)

            else:
                self.edge_with_intersection.append(False)
        self.intersection_points = np.transpose(self.intersection_points)
        self.intersection_points = np.delete(self.intersection_points, 0, 0)
        self.intersection_points = np.transpose(self.intersection_points)
        num_intersections -= 1
        return self.intersection_points, num_intersections

    def choosefunction(self, num_intersections):
        if num_intersections == '2':
            cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonstwoint(self)
            return cuttedPolygons, newPolygonsVertex
        else:
            cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonsmoreint(self)
            return cuttedPolygons, newPolygonsVertex

    def newpolygonstwoint(self):
        tol = 1e-7
        pol1 = np.array([[0], [0]], float)
        pol2 = np.array([[0], [0]], float)
        polNum1 = [0]
        polNum2 = []
        flag = np.zeros((2, 1), float)
        flag1 = np.zeros((2, 1), float)
        flag[0][0] = self.vertices[0][0]
        flag[1][0] = self.vertices[1][0]
        pol1 = np.append(pol1, flag, axis=1)
        pol1 = np.transpose(pol1)
        pol1 = np.delete(pol1, 0, 0)
        pol1 = np.transpose(pol1)
        punti_corrispondenti = np.array([[0], [0]], float)
        cuttedPolygons = np.array([[0], [0]], float)
        newPolygonsVertex = []
        temp2 = np.array([[0], [0]], float)
        distances = []
        i: int = 0
        num_vertex = np.size(self.vertices, axis=1)
        num_intersection = np.size(self.intersection_points, axis=1)
        n: int = num_vertex
        p1: int = 1
        p2: int = 0
        v1 = 1
        x: int = num_vertex
        while v1 < x:
            if self.edge_with_intersection[v1 - 1] is True:
                x = v1 - 1
                v2 = v1
                if (abs(self.intersection_points[0][i] - pol1[0][p1 - 1]) >= tol or
                        (abs(self.intersection_points[1][i] - pol1[1][p1 - 1] >= tol))):
                    if (abs(self.intersection_points[0][i] - self.vertices[0][v1]) >= tol or
                            (abs(self.intersection_points[1][i] - self.vertices[1][v1]) >= tol)):
                        flag[0][0] = self.intersection_points[0][i]
                        flag[1][0] = self.intersection_points[1][i]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, n)
                        polNum2 = np.append(polNum2, n)
                        n += 1

                    else:
                        flag[0][0] = self.vertices[0][v1]
                        flag[1][0] = self.vertices[1][v1]
                        pol1 = np.append(pol1, flag, axis=1)
                        pol2 = np.append(pol2, flag, axis=1)
                        polNum1 = np.append(polNum1, v1)
                        polNum2 = np.append(polNum2, v1)
                        v2 += 1

                    p1 += 1
                    p2 += 1

                flag[0][0] = self.intersection_points[0][i]
                flag[1][0] = self.intersection_points[1][i]
                segment = Seg.Segment(flag, self.s1)
                e = Seg.Segment.distance(segment)
                distances = np.append(distances, e)
                punti_corrispondenti = np.append(punti_corrispondenti, self.s1, axis=1)
                segment = Seg.Segment(flag, self.s2)
                f = Seg.Segment.distance(segment)
                distances = np.append(distances, f)
                punti_corrispondenti = np.append(punti_corrispondenti, self.s2, axis=1)

                z = i + 1
                for z in range(z, num_intersection):
                    flag[0][0] = self.intersection_points[0][i]
                    flag[1][0] = self.intersection_points[1][i]
                    flag1[0][0] = self.intersection_points[0][z]
                    flag1[1][0] = self.intersection_points[1][z]
                    segment = Seg.Segment(flag, flag1)
                    d = Seg.Segment.distance(segment)
                    distances = np.append(distances, d)
                    punti_corrispondenti = np.append(punti_corrispondenti, flag1, axis=1)

                distance_size = np.size(distances)
                for z in range(0, distance_size - 1):
                    min = z
                    w = z + 1
                    for w in range(w, distance_size):
                        if distances[w] < distances[min]:
                            min = w
                    temp1 = distances[min]
                    temp2[0][0] = punti_corrispondenti[0][min + 1]
                    temp2[1][0] = punti_corrispondenti[1][min + 1]
                    distances[min] = distances[z]
                    punti_corrispondenti[0][min + 1] = punti_corrispondenti[0][z + 1]
                    punti_corrispondenti[1][min + 1] = punti_corrispondenti[1][z + 1]
                    distances[z] = temp1
                    punti_corrispondenti[0][z + 1] = temp2[0][0]
                    punti_corrispondenti[1][z + 1] = temp2[1][0]

                for a in range(0, distance_size):
                    if (abs(punti_corrispondenti[0][a + 1] - pol1[0][p1 - 1]) >= tol or
                            (abs(punti_corrispondenti[1][a + 1] - pol1[1][p1 - 1]) >= tol)):
                        flag[0][0] = punti_corrispondenti[0][a + 1]
                        flag[1][0] = punti_corrispondenti[1][a + 1]
                        pol1 = np.append(pol1, flag, axis=1)
                        p1 += 1
                        polNum1 = np.append(polNum1, n)
                        n += 1

                for b in range(v1, num_vertex):
                    if self.edge_with_intersection[b] is True:
                        for c in range(b + 1, num_vertex):
                            flag[0][0] = self.vertices[0][c]
                            flag[1][0] = self.vertices[1][c]
                            pol1 = np.append(pol1, flag, axis=1)
                            polNum1 = np.append(polNum1, c)

                        for c in range(v2, b + 1):
                            if (abs(self.vertices[0][c] - pol2[0][p2]) >= tol or
                                    (abs(self.vertices[1][c] - pol2[1][p2]) >= tol)):
                                flag[0][0] = self.vertices[0][c]
                                flag[1][0] = self.vertices[1][c]
                                pol2 = np.append(pol2, flag, axis=1)
                                polNum2 = np.append(polNum2, c)

                        flag[0][0] = self.intersection_points[0][1]
                        flag[1][0] = self.intersection_points[1][1]
                        pol2 = np.append(pol2, flag, axis=1)
                        m = n - 1
                        polNum2 = np.append(polNum2, m)
                        m -= 1

                        if f < e:
                            pol2 = np.append(pol2, self.s1, axis=1)
                            pol2 = np.append(pol2, self.s2, axis=1)
                        else:
                            pol2 = np.append(pol2, self.s2, axis=1)
                            pol2 = np.append(pol2, self.s1, axis=1)

                        polNum2 = np.append(polNum2, m)
                        polNum2 = np.append(polNum2, m - 1)

            else:
                if (abs(self.vertices[0][v1] - pol1[0][p1 - 1]) >= tol or
                        (abs(self.vertices[1][v1] - pol1[1][p1 - 1]) >= tol)):
                    flag[0][0] = self.vertices[0][v1]
                    flag[1][0] = self.vertices[1][v1]
                    pol1 = np.append(pol1, flag, axis=1)
                    polNum1 = np.append(polNum1, v1)
            v1 += 1
        pol2 = np.transpose(pol2)
        pol2 = np.delete(pol2, 0, 0)
        pol2 = np.transpose(pol2)

        pol = np.concatenate((pol1, pol2), axis=1)
        cuttedPolygons = np.append(cuttedPolygons, pol, axis=1)
        cuttedPolygons = np.transpose(cuttedPolygons)
        cuttedPolygons = np.delete(cuttedPolygons, 0, 0)
        cuttedPolygons = np.transpose(cuttedPolygons)
        newPolygonsVertex = np.append(newPolygonsVertex, polNum1)
        newPolygonsVertex = np.append(newPolygonsVertex, polNum2)

        return cuttedPolygons, newPolygonsVertex

    @staticmethod
    def resultnewpolygonstwoint(new_polygons_vertex, pol1, pol2):  # Capire come fare...
        new_polygons_vertex = str(new_polygons_vertex)
        result = "The vertices of the new polygons are:\n" + str(new_polygons_vertex) + "\nThe first one is:\n" + \
                 str(pol1) + "\nThe second one is:\n" + str(pol2)
        return result

    def newpolygonsmoreint(self):  # Manca: Aiuto
        #cuttedPolygons, newPolygonsVertex = 0, 0
        #return cuttedPolygons, newPolygonsVertex
        tol = 1e-7
        j = 0
        cuttedPolygons = np.array([[0], [0]], float)
        newPolygonsVertex = []
        distances = []
        punti_corrispondenti = np.array([[0], [0]], float)
        whatEdgeInters = []
        num_vertex = np.size(self.vertices, axis=1)
        num_intersection = np.size(self.intersection_points, axis=1)
        for a in range(0, num_vertex):
            if self.edge_with_intersection[a] is True:
                whatEdgeInters = np.append(whatEdgeInters, a)
        pol = np.array([[0], [0]], float)
        polNum = []
        temp = np.zeros((2, 1), float)
        temp2 = np.zeros((2, 1), float)
        g = np.zeros((2, 1), float)
        temp[0][0] = self.vertices[0][0]
        temp[1][0] = self.vertices[1][0]
        pol = np.append(pol, temp, axis=1)
        pol = np.transpose(pol)
        pol = np.delete(pol, 0, 0)
        pol = np.transpose(pol)
        polNum = np.append(polNum, 0)
        ordineRetta = []
        vertAppenaAggiunto: bool = False
        v0Destra: bool
        vNuovoDestra: bool
        i: int = 0
        l: int = num_vertex
        n: int = num_vertex
        intTrovata: bool = False
        temp_d2 = np.array([[0.], [0.]])
        d = self.s2 - self.s1
        flag1: int = 0
        flag2: int = 0
        temp[0][0] = self.vertices[0][0]
        temp[1][0] = self.vertices[1][0]
        h = temp - self.s1
        if (d[0][0] * h[1][0] - d[1][0] * h[0][0]) > tol:
            v0Destr = False
        elif (d[0][0] * h[1][0] - d[1][0] * h[0][0]) < -tol:
            v0Destr = True
        v = 1
        while v < l:
            if self.edge_with_intersection[v - 1] is True:
                l = v - 1
                if self.intersection_points[0][i] != pol[0][v - 1] or self.intersection_points[1][i] != pol[1][v - 1]:
                    temp[0][0] = self.intersection_points[0][i]
                    temp[1][0] = self.intersection_points[1][i]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, n)
                    n += 1
                if self.s1[0][0] == self.intersection_points[0][i] and self.s1[1][0] == self.intersection_points[1][i]:
                    flag1 = 1
                if self.s2[0][0] == self.intersection_points[0][i] and self.s2[1][0] == self.intersection_points[1][i]:
                    flag2 = 1

                for z in range(i + 1, num_intersection):
                    temp[0][0] = self.intersection_points[0][i]
                    temp[1][0] = self.intersection_points[1][i]
                    temp2[0][0] = self.intersection_points[0][z]
                    temp2[1][0] = self.intersection_points[1][z]
                    seg = Seg.Segment(temp, temp2)
                    e = Seg.Segment.distance(seg)
                    distances = np.append(distances, e)
                    punti_corrispondenti = np.append(punti_corrispondenti, temp2, axis=1)
                    if self.s1[0][0] == self.intersection_points[0][z] and self.s1[1][0] == self.intersection_points[1][z]:
                        flag1 = 1
                    if self.s2[0][0] == self.intersection_points[0][z] and self.s2[1][0] == self.intersection_points[1][z]:
                        flag2 = 1
                if flag1 == 0:
                    temp[0][0] = self.intersection_points[0][i]
                    temp[1][0] = self.intersection_points[1][i]
                    seg = Seg.Segment(temp, self.s1)
                    e = Seg.Segment.distance(seg)
                    distances = np.append(distances, e)
                    punti_corrispondenti = np.append(punti_corrispondenti, self.s1, axis=1)
                if flag2 == 0:
                    temp[0][0] = self.intersection_points[0][i]
                    temp[1][0] = self.intersection_points[1][i]
                    seg = Seg.Segment(temp, self.s2)
                    e = Seg.Segment.distance(seg)
                    distances = np.append(distances, e)
                    punti_corrispondenti = np.append(punti_corrispondenti, self.s2, axis=1)

                distance_size = np.size(distances)
                for z in range(0, distance_size - 1):
                    min: int = z
                    w = z + 1
                    for w in range(w, distance_size):
                        if distances[w] < distances[min]:
                            min = w
                    temp_d = distances[min]
                    temp_d2[0][0] = punti_corrispondenti[0][min + 1]
                    temp_d2[1][0] = punti_corrispondenti[1][min + 1]
                    distances[min] = distances[z]
                    punti_corrispondenti[0][min + 1] = punti_corrispondenti[0][z + 1]
                    punti_corrispondenti[1][min + 1] = punti_corrispondenti[1][z + 1]
                    distances[z] = temp_d
                    punti_corrispondenti[0][z + 1] = temp_d2[0][0]
                    punti_corrispondenti[1][z + 1] = temp_d2[1][0]
                e = num_vertex + 1
                flag = 0
                for h in range(0, np.size(distances)):
                    for f in range(0, num_vertex):
                        if punti_corrispondenti[0][h + 1] == self.vertices[0][f] and punti_corrispondenti[1][h + 1] == \
                                self.vertices[1][f]:
                            flag = 1
                            ordineRetta = np.append(ordineRetta, f)
                    if flag == 0:
                        ordineRetta = np.append(ordineRetta, e)
                        e += 1
                    flag = 0
                for z in range(0, np.size(ordineRetta)):
                    intTrovata = True
                    flag = 0
                    i += 1
                    temp[0][0] = punti_corrispondenti[0][z + 1]
                    temp[1][0] = punti_corrispondenti[1][z + 1]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, ordineRetta[z])
                    if vertAppenaAggiunto is False:
                        b = 0
                        while b < np.size(self.intersection_points, axis=1):
                            if punti_corrispondenti[0][z + 1] == self.intersection_points[0][b] and \
                                    punti_corrispondenti[1][z + 1] == self.intersection_points[1][b]:
                                for a in range(1, num_vertex):
                                    if self.vertices[0][a] == self.intersection_points[0][b] and self.vertices[1][a] == \
                                            self.intersection_points[1][b]:
                                        flag = 1
                                        g[0][0] = self.vertices[0][a + 1] - self.s1[0][0]
                                        g[1][0] = self.vertices[1][a + 1] - self.s1[1][0]
                                        if (d[0][0] * g[1][0] - d[1][0] * g[0][0]) > tol:
                                            vNuovoDestra = False
                                        elif (d[0][0] * g[1][0] - d[1][0] * g[0][0]) < -tol:
                                            vNuovoDestra = True
                                        else:
                                            vNuovoDestra = v0Destr
                                        if v0Destr == vNuovoDestra:
                                            if (a + 1) < num_vertex:
                                                temp[0][0] = self.vertices[0][a + 1]
                                                temp[1][0] = self.vertices[1][a + 1]
                                                pol = np.append(pol, temp, axis=1)
                                                polNum = np.append(polNum, a + 1)
                                            else:
                                                z = len(ordineRetta)
                                if flag == 0:
                                    q = v
                                    while q < num_vertex:
                                        g[0][0] = self.vertices[0][q] - self.s1[0][0]
                                        g[1][0] = self.vertices[1][q] - self.s1[1][0]
                                        if (d[0][0] * g[1][0] - d[1][0] * g[0][0]) > tol:
                                            vNuovoDestra = False
                                        elif (d[0][0] * g[1][0] - d[1][0] * g[0][0]) < -tol:
                                            vNuovoDestra = True
                                        else:
                                            vNuovoDestra = v0Destr
                                        if v0Destr == vNuovoDestra:
                                            temp[0][0] = self.vertices[0][q]
                                            temp[1][0] = self.vertices[1][q]
                                            pol = np.append(pol, temp, axis=1)
                                            polNum = np.append(polNum, q)
                                            v = q + 1
                                            q = num_vertex
                                            b = len(self.intersection_points)
                                            vertAppenaAggiunto = True
                                        q += 1
                            b += 1
                    else:
                        vertAppenaAggiunto = False
            else:
                if self.vertices[0][v] != pol[0][v - 1] or self.vertices[1][v] != pol[1][v - 1]:
                    temp[0][0] = self.vertices[0][v]
                    temp[1][0] = self.vertices[1][v]
                    pol = np.append(pol, temp, axis=1)
                    polNum = np.append(polNum, v)
            if intTrovata is True:
                cuttedPolygons = np.append(cuttedPolygons, pol, axis=1)

                newPolygonsVertex = np.append(newPolygonsVertex, polNum)
                j += 1
                pol = np.array([[0], [0]], float)
                polNum = []
            v += 1
        return cuttedPolygons, newPolygonsVertex

'''
vertices = np.array([[1.5, 5.6, 5.5, 4., 3.2, 1.], [1., 1.5, 4.8, 6.2, 4.2, 4.]])
s1 = np.array([[2], [3.7]])
s2 = np.array([[4.1], [5.9]])
pol = NewPolygons(vertices, s1, s2)
intersection_points, num_intersections = NewPolygons.cut(pol)
print(intersection_points, num_intersections)
cuttedPolygons, newPolygonsVertex = NewPolygons.newpolygonsmoreint(pol)
print(cuttedPolygons, newPolygonsVertex)
'''