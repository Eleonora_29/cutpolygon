from unittest import TestCase
import src.NewPolygons as Polyg
import numpy as np


class TestPolygon(TestCase):
    def test_rectangle(self):
        try:
            vertices = np.array([[1, 5, 5, 1], [1, 1, 3.1, 3.1]])
            s1 = np.array([[2], [1.2]])
            s2 = np.array([[4], [3]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()
        try:
            Polyg.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Polyg.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result, "[[1.         1.77777778 2.         4.         4.11111111 1.\n  1.77777778 5.         5.         4.11111111 4.         2.        ]\n [1.         1.         1.2        3.         3.1        3.1\n  1.         1.         3.1        3.1        3.         1.2       ]]\n[0. 4. 5. 6. 7. 3. 4. 1. 2. 7. 6. 5.]")
        except:
            self.fail()

    def test_cut_pentagon(self):
        try:
            vertices = np.array([[2.5, 4, 3.4, 1.6, 1], [1, 2.1, 4.2, 4.2, 2.1]])
            s1 = np.array([[1.4], [2.75]])
            s2 = np.array([[3.6], [2.2]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()
        try:
            Polyg.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Polyg.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result,
                             "[[2.5  4.   3.6  1.4  1.2  1.   4.   3.4  1.6  1.2  1.4  3.6 ]\n [1.   2.1  2.2  2.75 2.8  2.1  2.1  4.2  4.2  2.8  2.75 2.2 ]]\n[0. 1. 5. 6. 7. 4. 1. 2. 3. 7. 6. 5.]")
        except:
            self.fail()

    def test_cut_convex(self):
        try:
            vertices = np.array([[2.4, 5.8, 3.3, 1.7], [1.1, 2.6, 6.8, 6.]])
            s1 = np.array([[2.8], [2.0]])
            s2 = np.array([[4.3], [3.9]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result,  "2")
        except:
            self.fail()

        try:
            Polyg.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Polyg.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result,
                             "[[2.4        5.8        4.71402715 4.3        2.8        2.35241935\n  4.71402715 3.3        1.7        2.35241935 2.8        4.3       ]\n [1.1        2.6        4.42443439 3.9        2.         1.43306452\n  4.42443439 6.8        6.         1.43306452 2.         3.9       ]]\n[0. 1. 4. 5. 6. 7. 4. 2. 3. 7. 6. 5.]")
        except:
            self.fail()

    def test_cut_concavo1(self):
        try:
            vertices = np.array([[1.5, 5.6, 5.5, 4, 3.2, 1], [1, 1.5, 4.8, 6.2, 4.2, 4]])
            s1 = np.array([[2], [3.7]])
            s2 = np.array([[4.1], [5.9]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result,  "4")
        except:
            self.fail()

    def test_cut_concavo2(self):
        try:
            vertices = np.array([[3.5, 6.7, 5.3, 3.7, 2.2, 1.], [1., 4.2, 6.8, 6., 8.5, 5.6]])
            s1 = np.array([[2], [6.5]])
            s2 = np.array([[5.1], [3.9]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "2")
        except:
            self.fail()
        try:
            Polyg.NewPolygons.choosefunction(pol, num_intersection)
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Polyg.NewPolygons.newpolygonstwoint(pol)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result,
                             "[[3.5        5.80701754 5.1        2.         1.53410405 1.\n  5.80701754 6.7        5.3        3.7        2.2        1.53410405\n  2.         5.1       ]\n [1.         3.30701754 3.9        6.5        6.89075145 5.6\n  3.30701754 4.2        6.8        6.         8.5        6.89075145\n  6.5        3.9       ]]\n[0. 6. 7. 8. 9. 5. 6. 1. 2. 3. 4. 9. 8. 7.]")
        except:
            self.fail()

    def test_cut_concavo3(self):
        try:
            vertices = np.array([[1., 4.5, 5., 3.5, 3., 2., 0.5], [1., 0.5, 4.5, 3.5, 5., 3.5, 5.5]])
            s1 = np.array([[1.1], [3.8]])
            s2 = np.array([[2.9], [3.9]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "6")
        except:
            self.fail()

    def test_cut_d1(self):
        try:
            vertices = np.array([[2., 0., 3., 0., 3., 3., -1., -3., 0., -3], [-2., -1., 1., 2., 2., 3., 3., 1., 0., -2.]])
            s1 = np.array([[-1.], [-1.]])
            s2 = np.array([[1.], [1]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "5")
        except:
            self.fail()

    def test_cut_d2(self):
        try:
            vertices = np.array([[2., 0., 3., 0., 3., 3., -1., -3., 0., -3], [-2., -1., 1., 2., 2., 3., 3., 1., 0., -2.]])
            s1 = np.array([[0.], [-1.8]])
            s2 = np.array([[0.], [1.]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            result = str(num_intersection)
            self.assertEqual(result, "5")
        except:
            self.fail()









































'''
Test vuoto da fare
'''

