from unittest import TestCase
import numpy as np
import src.NewPoints as Np
import src.NewPolygons as Polyg
import src.Segment as Seg


class TestConvex(TestCase):
    def test_Polygon(self):
        try:
            vertices = np.array([[2.4, 5.8, 3.3, 1.7], [1.1, 2.6, 6.8, 6.]])
            s1 = np.array([[2.8], [2.0]])
            s2 = np.array([[4.3], [3.9]])
            pol = Polyg.NewPolygons(vertices, s1, s2)
            intersection_points, num_intersection = Polyg.NewPolygons.cut(pol)
            num_intersection = str(num_intersection)
            self.assertEqual(num_intersection, "2")
        except:
            self.fail()

        try:
            cuttedPolygons, newPolygonsVertex = Polyg.NewPolygons.choosefunction(pol, num_intersection)
            result = str(cuttedPolygons) + "\n" + str(newPolygonsVertex)
            self.assertEqual(result,
                             "[[2.4        5.8        4.71402715 4.3        2.8        2.35241935\n  4.71402715 3.3        1.7        2.35241935 2.8        4.3       ]\n [1.1        2.6        4.42443439 3.9        2.         1.43306452\n  4.42443439 6.8        6.         1.43306452 2.         3.9       ]]\n[0. 1. 4. 5. 6. 7. 4. 2. 3. 7. 6. 5.]")
        except:
            self.fail()

        try:
            segment = Seg.Segment(s1, s2)
            segment = Seg.Segment.tomatrix(segment)
            newpoints = Np.NewPoints(vertices, intersection_points, segment)
            self.assertEqual(str(Np.NewPoints.newpoints(newpoints)),"[[2.4        5.8        3.3        1.7        2.8        4.3\n  4.71402715 2.35241935]\n [1.1        2.6        6.8        6.         2.         3.9\n  4.42443439 1.43306452]]")
        except:
            self.fail()
