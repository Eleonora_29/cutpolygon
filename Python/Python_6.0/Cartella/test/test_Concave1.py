from unittest import TestCase
import src.Point as P
import numpy as np



class TestConcavePolygon1(TestCase):
    def test_NewPoints(self):
            vertex = np.array([[1.5, 5.6, 5.5, 4., 3.2, 1.], [1., 1.5, 4.8, 6.2, 4.2, 4.]])
            intersection_points = np.array([[4.20432692, 3.72131148, 2.40859729, 1.19121622], [6.00929487, 5.50327869, 4.1280543, 2.8527027]])
            num_vertex = 6
            num_intersection = 4
            s1 = np.array([[2], [3.7]])
            s2 = np.array([[4.1], [5.9]])
            self.assertEqual(
                str(P.Point.NewPoints(P, vertex, intersection_points, s1, s2, num_vertex, num_intersection)),
                "[[1.5        5.6        5.5        4.         3.2        1.         4.20432692 3.72131148 2.40859729 1.19121622 2.         4.1       ]\n [1.         1.5        4.8        6.2        4.2        4.         6.00929487 5.50327869 4.1280543  2.8527027  3.7        5.9       ]]")