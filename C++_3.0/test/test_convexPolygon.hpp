#ifndef __TEST_CONVEXPOLYGON_H
#define __TEST_CONVEXPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace PolygonCutLibrary;
using namespace testing;
using namespace std;

namespace ConvexPolygonTesting {

  //questo test funziona ma non è finito

  TEST(TestPolygonCut, TestConvexPolygon)
  {
      Vector2d v1, v2, v3, v4, v5;
      v1 << 2.5, 1.0;
      v2 << 4.0, 2.1;
      v3 << 3.4, 4.2;
      v4 << 1.6, 4.2;
      v5 << 1.0, 2.1;

      Vector2d s1, s2;
      s1 << 1.4, 2.75;
      s2 << 3.6, 2.2;


      vector<Vector2d> convexPolygonVertex;
      convexPolygonVertex.push_back(v1);
      convexPolygonVertex.push_back(v2);
      convexPolygonVertex.push_back(v3);
      convexPolygonVertex.push_back(v4);
      convexPolygonVertex.push_back(v5);

      try
      {
        PolygonCutLibrary::Polygon(5,convexPolygonVertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Polygon convexPolygon(5, convexPolygonVertex, s1, s2);

      convexPolygon.Cut();
      EXPECT_EQ(2, convexPolygon.NumIntersection());
      Vector2d inters1, inters2, a, b;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 4.0, 2.1;
      inters2 << 1.2, 2.8;
      // i punti di intersezione che calcola
      a = convexPolygon.IntersectionPoints()[0];
      b = convexPolygon.IntersectionPoints()[1];
      intersectionPoints = convexPolygon.IntersectionPoints();
      //controllo che coincidano
      EXPECT_EQ(a, inters1);
      EXPECT_EQ(b, inters2);

      try
      {
            PolygonCutLibrary::Point();
      }
      catch (const exception& exception)
      {
            FAIL();
      }

      Point point;
      vector<Vector2d> newPoints;
      point.CalcPoints(convexPolygonVertex, 5, intersectionPoints, 2, s1, s2);
      EXPECT_EQ(8, point.NumNewPoints()); //un'intersezione coincide con un vertice
      newPoints = point.NewPoints();
      // controllo che i primi 5 punti corrispondano ai vertici
      EXPECT_EQ(v1, newPoints[0]);
      EXPECT_EQ(v2, newPoints[1]);
      EXPECT_EQ(v3, newPoints[2]);
      EXPECT_EQ(v4, newPoints[3]);
      EXPECT_EQ(v5, newPoints[4]);
      // controllo che il sesto sia l'intersezione che non coincide con il vertice
      EXPECT_EQ(inters2, newPoints[5]);
      //controllo che gli ultimi due siano gli estremi del segmento
      EXPECT_EQ(s1, newPoints[6]);
      EXPECT_EQ(s2, newPoints[7]);

      convexPolygon.ChooseFunction();
      vector<Vector2d> cuttedPolygon1, cuttedPolygon2;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(v2);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v5);
      newVertex1.push_back(0);
      newVertex1.push_back(1);
      newVertex1.push_back(5);
      newVertex1.push_back(6);
      newVertex1.push_back(7);
      newVertex1.push_back(4);

      cuttedPolygon2.push_back(v2);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(v4);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s1);
      cuttedPolygon2.push_back(s2);
      newVertex2.push_back(1);
      newVertex2.push_back(2);
      newVertex2.push_back(3);
      newVertex2.push_back(7);
      newVertex2.push_back(6);
      newVertex2.push_back(5);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);

      EXPECT_EQ(allCuttedPol, convexPolygon.CuttedPolygons());
      EXPECT_EQ(allNewVertex, convexPolygon.NewPolygonsVertex());

  }
}

#endif // __TEST_CONVEXPOLYGON_H
