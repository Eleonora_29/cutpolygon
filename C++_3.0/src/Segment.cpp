#include "Segment.hpp"

namespace PolygonCutLibrary {

  Segment::Segment(const Vector2d& s1,
                   const Vector2d& s2)
  {
    _s1 = s1;
    _s2 = s2;
    //segment = Segment(_s1, _s2);
  }
  Segment::~Segment()
  {
  }

  double Segment::CoeffAngolare(Vector2d s1, Vector2d s2)
  {
      double coeff;
      coeff = (s2(1)-s1(1))/(s2(0)-s1(0));
      return coeff;
  }

}
