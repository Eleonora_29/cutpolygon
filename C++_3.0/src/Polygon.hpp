#ifndef POLYGON_H
#define POLYGON_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Intersector.hpp>
#include <Segment.hpp>
#include <Point.hpp>
#include <Eigen>

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Polygon {
    private:
      vector<Vector2d > _vertex;
      Vector2d _s1, _s2;
      int _numVertex;
      vector<bool> edgeWithInters;
      int numIntersections = 0;
      Vector2d intersectionCoordinates;
      vector<Vector2d> intersectionPoints;
      vector<vector<Vector2d>> cuttedPolygons;
      vector<vector<int>> newPolygonsVertex;
      vector<double> distances;
      vector<Vector2d> puntiCorrispondenti;
      Point point;

  public:
      Polygon(int numVertex, vector<Vector2d> vertex, Vector2d& s1, Vector2d& s2);
      ~Polygon();

      void Cut();
      int& NumIntersection() {return numIntersections;}
      vector<Vector2d> IntersectionPoints() { return intersectionPoints;}

      void ChooseFunction(); //scelgo se usare la funzione per i poligoni che hanno due intersezioni o l'altra
      void NewPolygonsTwoInt(); //per i convessi e per i concavi con due intersezioni
      void NewPolygonsMoreInt(); //per gli altri concavi
      vector<vector<Vector2d>> CuttedPolygons() {return cuttedPolygons;}
      vector<vector<int>> NewPolygonsVertex() {return newPolygonsVertex;}

  };
}

#endif // POLYGON_H
