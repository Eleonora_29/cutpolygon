#include "Point.hpp"
#include <math.h>

namespace PolygonCutLibrary {

  Point::Point()
  {

  }

  Point::~Point()
  {

  }

  double Point::Distance(const Vector2d &A, const Vector2d &B)
  {
      return sqrt(pow((A(0) - B(0)), 2) + pow((A(1) - B(1)), 2));
  }

  void Point::CalcPoints(const vector<Vector2d> vertex, const int numVertex,
                         const vector<Vector2d> intersectionPoints, const int numIntersections,
                         const Vector2d s1, const Vector2d s2)
  {
      for (int i=0; i<numVertex; i++)
        newPoints.push_back(vertex[i]); // prima inserisco i vertici del poligono

      int flag=0, flag1=0, flag2=0;

      for (int j=0; j<numIntersections; j++) //scorro i punti di intersezione
      {
          for (int i=0; i<numVertex; i++) //scorro i vertici
          {
              if (intersectionPoints[j] == vertex[i]) //se il punto di intersezione è un vertice
                  flag = 1;
          }
          if (flag == 0)
              newPoints.push_back(intersectionPoints[j]); //aggiungo il punto a newPoints solo se non è un vertice
          flag = 0;
      }

      for (unsigned int k=0; k<newPoints.size(); k++)
      {
          if (s1 == newPoints[k])  //se il primo estremo del segmento coincide con vertici o intersezioni
              flag1 = 1;
          if (s2 == newPoints[k]) //stessa cosa per il secondo estremo
              flag2 = 1;
      }
      // se non coincidono con niente, li aggiungo
      if (flag1 == 0)
          newPoints.push_back(s1);
      if (flag2 == 0)
          newPoints.push_back(s2);
  }

}
