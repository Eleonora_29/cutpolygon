#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <Eigen>
#include <vector>
#include <Segment.hpp>

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Point {
    public:
      vector<Vector2d> newPoints;

      Point();
      ~Point();

      static double Distance (const Vector2d& A, const Vector2d& B);
      void CalcPoints (const vector<Vector2d> vertex, const int numVertex,
                       const vector<Vector2d> intersectionPoints , const int numIntersections,
                       const Vector2d s1, const Vector2d s2);
      vector<Vector2d> NewPoints () {return newPoints;} //contiene prima i vertici e poi le intersezioni
      const int NumNewPoints () {return newPoints.size();}
  };
}

#endif // POINT_H
