#ifndef SEGMENT_H
#define SEGMENT_H

#include <iostream>
#include <Intersector.hpp>
#include <Eigen>
#include <vector>

using namespace Eigen;
using namespace std;

namespace PolygonCutLibrary {

  class Segment {
    public:
      Vector2d _s1, _s2;
      //static Segment segment;

      Segment(const Vector2d& s1,
              const Vector2d& s2);
      ~Segment();

      double CoeffAngolare(Vector2d s1, Vector2d s2);
  };
}

#endif // SEGMENT_H
