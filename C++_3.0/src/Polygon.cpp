#include "Polygon.hpp"
#include <math.h>


namespace PolygonCutLibrary {

Polygon::Polygon(int numVertex, vector <Vector2d> vertex, Vector2d& s1, Vector2d& s2)
{
    _numVertex = numVertex;
    _s1 = s1;
    _s2 = s2;
    _vertex=vertex;
}

Polygon::~Polygon()
{

}

void Polygon::Cut()
{
    for (int i=0; i<_numVertex; i++)
    {
        Intersector intersector; //mi serve per calcolare l'intersezione tra ogni lato e il segmento
        if (i!=(_numVertex-1)) //se non sono arrivata all'ultimo vertice, il lato è da i a i+1, mentre l'ultimo lato è da i a 0
            intersector.SetEdge(_vertex[i], _vertex[i+1]);
        else
            intersector.SetEdge(_vertex[i], _vertex[0]);

        intersector.SetStraight(_s1, _s2);

        bool inters = intersector.Intersection();
        if (inters == true) //se c'è intersezione
        {
            intersectionCoordinates = intersector.IntersectionCoordinates(); //calcolo le coordinate del punto di intersezione
            if (numIntersections==0 || intersectionCoordinates != intersectionPoints[numIntersections-1])
            {
                edgeWithInters.push_back(true); //mi segno che quel lato ha un'intersezione
                numIntersections ++;
                intersectionPoints.push_back(intersectionCoordinates); //lo aggiungo alla lista dei punti di intersezione
            }
            else
                edgeWithInters.push_back(false);
        }
        else
            edgeWithInters.push_back(false); //segno anche se il lato non ha intersezione
    }

}

void Polygon::ChooseFunction()
{
    if (numIntersections == 2)
        Polygon::NewPolygonsTwoInt();
    else
        Polygon::NewPolygonsMoreInt();
}

void Polygon::NewPolygonsTwoInt()
{
    vector<Vector2d> pol1, pol2;
    vector<int> polNumerati1, polNumerati2;
    pol1.push_back(_vertex[0]); //parto dal vetice 0
    polNumerati1.push_back(0);
    int i=0; //scorre le intersezioni
    int x=_numVertex;
    int n=_numVertex;
    int m; //è un indice, mi serve per il pol2
    int p1=1, p2=0; //tengono conto di dove sono arrivata a riempire pol1 e pol2
    int v2=0; //scorre i vertici del poligono originale per metterli in pol2
    float e,f;

    for (int v1=1; v1<x; v1++) //fa scorrere i vertici del poligono originale per metterli in pol1, parto da 1 perché 0 è occupato
    {
        if(edgeWithInters[v1-1] == true) //se il lato contiene l'intersezione
        {
            x=v1-1; //quando trovo il lato con l'intersezione non voglio più entrare nel for grande, quindi faccio in modo che l<x
            v2 = v1; //memorizzo qual è il primo vertice dopo l'intersezione, mi servirà per pol2

            if (intersectionPoints[i]!=pol1[p1-1]) //se l'intersezione trovata non coincide con il punto già inserito
            {
                if (intersectionPoints[i] != _vertex[v1]) //se l'intersezione non coincide con il vertice
                {
                    pol1.push_back(intersectionPoints[i]); //aggiungo il punto di intersezione al pol1
                    pol2.push_back(intersectionPoints[i]); //metto l'intersezione come primo punto del secondo poligono
                    polNumerati1.push_back(n); //alle intersezioni dò numeri successivi rispetto a quelli dei vertici
                    polNumerati2.push_back(n);
                    n++;
                }
                else //se coincide, aggiungo quel vertice
                {
                    pol1.push_back(_vertex[v1]);
                    pol2.push_back(_vertex[v1]);
                    polNumerati1.push_back(v1);
                    polNumerati2.push_back(v1);
                    v2++; //v1 non lo incremento perché mi serve che tenga conto del lato con la prima intersezione
                }
                p1++; p2++;
            }
            //calcolo le distanze tra la prima intersezione e i vertici del segmento e memorizzo rispetto a quale punto la sto calcolando in "puntiCorrispondenti"
            e=point.Distance(intersectionPoints[i], _s1);
            distances.push_back(e);
            puntiCorrispondenti.push_back(_s1);
            f=point.Distance(intersectionPoints[i], _s2);
            distances.push_back(f);
            puntiCorrispondenti.push_back(_s2);

            // calcolo le distanze tra la prima intersezione e le altre e salvo rispetto a quale punto le sto calcolando
            for (int z=i+1; z<numIntersections; z++)
            {
                distances.push_back(point.Distance(intersectionPoints[i], intersectionPoints[z]));
                puntiCorrispondenti.push_back(intersectionPoints[z]);
            }

            //metto in ordine crescente il vettore delle distanze (le distanze partono tutte dal primo punto di intersezione e arrivano agli estremi
            //del segmento e alle altre intersez)
            //ordino anche i punti a cui le distanze si riferiscono
            for(unsigned int z=0; z<distances.size()-1; z++)
            {
                double min = z;
                for(unsigned int w=z+1; w<distances.size(); w++)
                {
                    if(distances[w] < distances[min])
                        min = w;
                }
            double temp1=distances[min];
            Vector2d temp2=puntiCorrispondenti[min];
            distances[min]=distances[z];
            puntiCorrispondenti[min]=puntiCorrispondenti[z];
            distances[z]=temp1;
            puntiCorrispondenti[z]=temp2;
            }

            for (unsigned int a=0; a<distances.size(); a++)
            {
                if (puntiCorrispondenti[a] != pol1[p1-1]) //controllo che i punti con cui ho calcolato la distanza, non siano già stati inseriti in pol1
                {
                    pol1.push_back(puntiCorrispondenti[a]); //li inserisco in ordine, dal più vicino al più lontano
                    p1++;
                    polNumerati1.push_back(n); //numero i punti che sto inserendo
                    n++;
                }
            }

            for (int b=v1; b<_numVertex; b++)
            {
                if (edgeWithInters[b]==true) //cerco in che lato è l'ultimo punto di intersezione
                {
                    for (int c=b+1; c<_numVertex; c++)
                    {
                        pol1.push_back(_vertex[c]); //in pol1 inserisco i vertici successivi al lato in cui c'è l'ultima intersezione
                        polNumerati1.push_back(c);
                    }
                    for (int c=v2; c<b+1; c++)
                    {
                        if (_vertex[c] != pol2[p2-1]) //se il vertice non coincide con l'intersezione inserita
                        {
                            pol2.push_back(_vertex[c]); //in pol2 inserisco i vertici precedenti al lato in cui c'è l'ultima intersezione
                            polNumerati2.push_back(c);
                        }
                    }
                    pol2.push_back(intersectionPoints[1]); //inserisco la seconda intersezione
                    m=n-1; //m è l'indice delle intersezioni che parte dal fondo
                    polNumerati2.push_back(m);
                    m--;

                    //inserisco gli estremi del segmento in ordine di distanza inversa dalla prima intersezione
                    if (f<e)
                    {
                        pol2.push_back(_s1);
                        pol2.push_back(_s2);
                    }
                    else
                    {
                        pol2.push_back(_s2);
                        pol2.push_back(_s1);
                    }
                    polNumerati2.push_back(m);
                    polNumerati2.push_back(m-1);
                }
            }
        }

        else //altrimenti aggiungo solo il vertice successivo
        {
            if (_vertex[v1] != pol1[p1-1]) // se il vertice non coincide con l'intersezione precedente (se c'è)
            {
                pol1.push_back(_vertex[v1]); //lo aggiungo e vado avanti con il for
                polNumerati1.push_back(v1);
            }
        }

    }
    cuttedPolygons.push_back(pol1);
    cuttedPolygons.push_back(pol2);
    newPolygonsVertex.push_back(polNumerati1); //in realtà mi serve solo questo
    newPolygonsVertex.push_back(polNumerati2);

}

void Polygon::NewPolygonsMoreInt()
{
    //scelgo di numerare le intersezioni dalla prima trovata, e proseguo seguendo la retta

    int j=0; //scorre i poligoni tagliati

    //li uso come vettori di appoggio, per riempire cuttedPolygon e newPolygonsVertex
    vector<Vector2d> pol;
    vector<int> polNumerati;

    //inizio a cercare il poligono "grande"

    pol.push_back(_vertex[0]); //parto dal vertice 0 e lo inserisco nel poligono
    polNumerati.push_back(0); //qui salvo i numeri corrispondenti ai vertici

    int i=0; //fa scorrere le intersezioni
    int f=0; //è una flag
    int l=_numVertex;
    int n=_numVertex;


    for (int v=1; v<l; v++) //fa scorrere i punti del nuovo poligono, parte da 1 perché il posto 0 è già occupato
    {
        if (edgeWithInters[v-1]==true) //se c'è intersezione sul lato che sto considerando
        {
             l=v-1; //quando trovo il lato con l'intersezione non voglio più entrare nel for grande, quindi faccio in modo che v<l
             if (intersectionPoints[i] != pol[v-1]) //se l'intersezione non è sul vertice già inserito
             {
                 pol.push_back(intersectionPoints[i]); //aggiungo il punto di intersezione
                 polNumerati.push_back(n); //alle intersezioni dò i numeri successivi a quelli dei vertici
                 n++;
             }
             distances.push_back(point.Distance(intersectionPoints[i], _s1)); //distanza tra la prima intersezione e s1
             puntiCorrispondenti.push_back(_s1); //salvo s1, così so a cosa si riferisce la distanza
             distances.push_back(point.Distance(intersectionPoints[i], _s2)); //distanza tra la prima intersezione e s2
             puntiCorrispondenti.push_back(_s2); //salvo s2, così so a cosa si riferisce la distanza

             // calcolo le distanze tra la prima intersezione e le altre e salvo rispetto a quale punto le sto calcolando
             for (int z=i+1; z<numIntersections; z++)
             {
                 distances.push_back(point.Distance(intersectionPoints[i], intersectionPoints[z])); //
                 puntiCorrispondenti.push_back(intersectionPoints[z]);
             }

             //metto in ordine crescente il vettore delle distanze
             for(unsigned int z=0; z<distances.size()-1; z++)
             {
                 double min = z;
                 for(unsigned int w=z+1; w<distances.size(); w++)
                 {
                     if(distances[w] < distances[min])
                          min = w;
                 }
                 double temp1=distances[min];
                 Vector2d temp2=puntiCorrispondenti[min];
                 distances[min]=distances[z];
                 puntiCorrispondenti[min]=puntiCorrispondenti[z];
                 distances[z]=temp1;
                 puntiCorrispondenti[z]=temp2;
             }

             Segment segment(_s1, _s2);
             double coeff = segment.CoeffAngolare(_s1, _s2); //calcolo il coeff angolare del segmento

             //voglio trovare quali vertici, tra quelli non ancora inseriti, stanno sotto il segmento di taglio
             for (int q=v; q<_numVertex; q++)
             {
                 double ySegmento = _s1(1) - coeff*(_s1(0) - _vertex[q](0)); //calcolo la y del punto, appartenente al segmento, che ha la stessa x del vertice che considero
                 if (_vertex[q](1) < ySegmento) //se la y del vertice è minore della y calcolata
                 {
                     for (unsigned int z=0; z<distances.size(); z++) //cerco quanti punti (intersezioni o estremi del segmento) sono da inserire prima del vertice trovato
                     {
                         if (_vertex[q](0)<puntiCorrispondenti[z](0)) //per farlo confronto le loro coordinate x
                              f++; //mi indica quante intersezioni o estremi devo mettere prima del vertice
                     }
                     for (int a=0; a<f; a++)
                     {
                         pol.push_back(puntiCorrispondenti[a]); //aggiungo un numero di intersezioni e estremi pari alla f trovata
                         polNumerati.push_back(n);
                         n++; //incremento l'indice che identifica i punti del nuovo poligono
                         i++; //incremento le intersezioni inserite
                     }
                     pol.push_back(_vertex[q]); //e poi posso aggiungere il vertice (quello sotto al segmento)
                     polNumerati.push_back(q);
                  }
                  v=q; //dò a v il valore di q, che è il numero di vertici attualmente aggiunti
             }

             //aggiungo intersezioni e estremi che non sono ancora stati aggiunti
             for (unsigned int b=i; b<puntiCorrispondenti.size(); b++)
             {
                 pol.push_back(puntiCorrispondenti[b]);
                 polNumerati.push_back(n);
                 n++;
             }

             //aggiungo i vertici mancanti
             for (int c=v+1; c<_numVertex; c++)
             {
                 pol.push_back(_vertex[c]);
                 polNumerati.push_back(c);
             }
        }
        else //altrimenti aggiungo solo il vertice successivo
        {
            if (_vertex[v] != pol[v-1]) // se il vertice non coincide con l'intersezione precedente (se c'è)
            {
                 pol.push_back(_vertex[v]);
                 polNumerati.push_back(v);
            }
        }
    }
    cuttedPolygons.push_back(pol);
    newPolygonsVertex.push_back(polNumerati);
    j++;

    i=0;
    n=_numVertex+1;
    int v=0;
    int rangeI=_numVertex; //è il primo punto di intersezione che considero
    int rangeF; // è il secondo punto di intersezione che considero
    // considero questo range per capire se ci sono degli estremi del segmento in mezzo

    for (int y=0; i<numIntersections; y++) //finché non finiscono le intersezioni
    {
        int w = _numVertex;
        pol.clear();
        polNumerati.clear();
        pol.push_back(intersectionPoints[i]); //parto dall'intersezione
        polNumerati.push_back(rangeI);
        i++;
        v=l+1;
        pol.push_back(_vertex[v]); //aggiungo il vertice successivo
        polNumerati.push_back(v);
        v++;
        for (int k=l+1; k<w; k++) // mi ero già salvata che l è il primo lato che contiene un'intersezione
        {
            if (edgeWithInters[k] == true)
            {
                w=k-1; //così non entro più nel for
                pol.push_back(intersectionPoints[i]);
                int z = puntiCorrispondenti.size();
                for (int a=0; a<z; a++)
                {
                    if (puntiCorrispondenti[a] == intersectionPoints[i]) //quando trovo in punti corrispondenti l'intersezione che sto inserendo
                    {
                        z=a-1; //quando trovo il punto non voglio più entrare in questo for
                        polNumerati.push_back(n+a); //così è il numero corrispondente all'intersezione
                        rangeF = n+a;
                        i++;
                    }
                }
                for (int x=rangeI+1; x<rangeF; x++)
                {
                    pol.push_back(puntiCorrispondenti[x-n]);
                    polNumerati.push_back(x);
                }
            }
            else
            {
                if (_vertex[v] != intersectionPoints[i-1]) //se il vertice non coincide con il punto di intersezione aggiunto
                {
                     pol.push_back(_vertex[v]);
                     polNumerati.push_back(v);
                }
            }
            rangeI=rangeF+1;
            cuttedPolygons.push_back(pol);
            newPolygonsVertex.push_back(polNumerati);
            j++;
            for (int e=l+2; e<_numVertex; e++) //un lato lo salto perché l'ho già considerato nel poligono precedente
            {
                if (edgeWithInters[e]==true)
                {
                    l=e;
                    e=_numVertex+1;
                }
            }
        }
    }
}
}
