#include "Intersector.hpp"
#include <math.h>

namespace PolygonCutLibrary {

Intersector::Intersector()
{

}

Intersector::~Intersector()
{

}

void Intersector::SetStraight(const Vector2d &origin1, const Vector2d &end1)
{
    _origin1 = origin1;
    matrixTangentVector.col(0) = end1 - origin1; //t1
}

void Intersector::SetEdge(const Vector2d &origin2, const Vector2d &end2)
{
    _origin2 = origin2;
    matrixTangentVector.col(1) = origin2 - end2; //t2
}

bool Intersector::Intersection()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;

    rightHandSide = _origin2 - _origin1;
    double det = matrixTangentVector.determinant(); //mi serve per calcolare la matrice inversa
    bool intersection = false;

    double check = toleranceParallelism * toleranceParallelism * matrixTangentVector.col(0).squaredNorm() *  matrixTangentVector.col(1).squaredNorm();
    if(det * det >= check)
    {
        Matrix2d solverMatrix;
        solverMatrix << matrixTangentVector(1,1), -matrixTangentVector(0,1), -matrixTangentVector(1,0), matrixTangentVector(0,0); //se la divido per il determinante, è l'inversa di matrixTangentVector
        resultParametricCoordinates = (solverMatrix * rightHandSide)/det; // vedi appunti di D'Auria

        if (resultParametricCoordinates(1)-1.0 < toleranceIntersection  && resultParametricCoordinates(1) > -toleranceIntersection ) //controllo che l'intersezione sia interna al lato
            intersection = true;
        else
            intersection = false;
    }
    return intersection;
}

const Vector2d& Intersector::IntersectionCoordinates()
{
    intersectionCoordinates = _origin1 + matrixTangentVector.col(0)*resultParametricCoordinates(0); // vedi appunti di D'Auria

    return intersectionCoordinates;
}

}
